package ro.vadim.gunrun.communication.messages;

import java.util.Map;

public interface MessageHandler {
	
	public void manageMessage(Map<String, Object> jsonData);
	
}
