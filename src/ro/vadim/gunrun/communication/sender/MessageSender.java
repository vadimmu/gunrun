package ro.vadim.gunrun.communication.sender;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import ro.vadim.gunrun.communication.CommunicationUtils;
import ro.vadim.gunrun.communication.messages.Messages;
import ro.vadim.gunrun.communication.messages.Messages.Admin;
import ro.vadim.gunrun.communication.messages.Messages.InGame;
import ro.vadim.gunrun.communication.messages.Messages.Lobby;
import ro.vadim.gunrun.communication.messages.Messages.InGame.ToServer;
import ro.vadim.gunrun.game.gameelements.Player;
import ro.vadim.gunrun.game.storage_and_data.StaticData_Players;

import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class MessageSender{

	private static Socket socket = null;
	
	private static OutputStream toServer_Stream = null;
	private static BufferedWriter toServer_Writer = null;	
	
	//ADMIN MESSAGES
	/////////////////////////////////////////////////////////
	public static void send_HELLO(UUID currentPlayerUUID){
		
		try{
			
			LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
			
			if(currentPlayerUUID == null)
				jsonStructure.put("player", "null");
			
			else
				jsonStructure.put("player", currentPlayerUUID.toString());
			
			
			Log.i("send_HELLO", jsonStructure.toString());
			sendMessage(composeMessage(Messages.Admin.ToServer.HELLO, jsonStructure));
		}
		catch(IOException e){
			Log.println(Log.ERROR, "send_CHANGE_POSITION", e.toString());			
		}
	}
	
	public static void send_GAMES_LIST(){
		try {
			sendMessage(composeEmptyMessage(Messages.Admin.ToServer.GAMES_LIST));
		} 
		
		catch (IOException e) {
			Log.println(Log.ERROR, "send_GAMES_LIST", e.toString());
		}		
	}
	
	public static void send_CREATE_GAME(String name, int gameType){
		//public static int CREATE_GAME = 25;				//{name: GameServerName, gameType: gameType}
				
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		jsonStructure.put("name", name);
		jsonStructure.put("gameType", String.valueOf(gameType));
		
		try {
			sendMessage(composeMessage(Messages.Admin.ToServer.CREATE_GAME, jsonStructure));
		} 
		
		catch (JsonProcessingException e) {
			Log.println(Log.ERROR, "send_CREATE_GAME", e.toString());
		} 
		
		catch (IOException e) {
			Log.println(Log.ERROR, "send_CREATE_GAME", e.toString());
		}
	}
	
	
	
	//IN-GAME MESSAGES
	/////////////////////////////////////////////////////////

	int CHANGE_POSITION = 0;	// {latitude: newLatitude, longitude: newLongitude}
	int SHOOT = 1; 				// {target: targetUUID, weapon: weapon_name, damage: weapon_damage, (optional)timestamp: timeStamp}
	int MESSAGE_TEAM = 2; 		// {message : messageString}	
	
	public static void send_CHANGE_POSITION(double latitude, double longitude){
		
		try{
			LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
			
			jsonStructure.put("latitude", String.valueOf(latitude));
			jsonStructure.put("longitude", String.valueOf(longitude));
			
			sendMessage(composeMessage(Messages.InGame.ToServer.CHANGE_POSITION, jsonStructure));
		}
		catch(IOException e){
			Log.println(Log.ERROR, "send_CHANGE_POSITION", e.toString());			
		}
	}
	
	public static void send_SHOOT(UUID targetUUID, String weaponName, int damage){
		
		try{
			
			Player targetPlayer = StaticData_Players.getPlayerById(targetUUID);
						
			LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
			jsonStructure.put("targetUUID", targetUUID.toString());
			jsonStructure.put("weapon", weaponName);
			jsonStructure.put("damage", String.valueOf(damage));
			
			sendMessage(composeMessage(Messages.InGame.ToServer.SHOOT, jsonStructure));
		}
		catch(IOException e){
			Log.println(Log.ERROR, "send_SHOOT", e.toString());			
		}
	}
	
	public static void send_MESSAGE_TEAM(String message) throws IOException{
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		jsonStructure.put("message", message);
		
		sendMessage(composeMessage(Messages.InGame.ToServer.MESSAGE_TEAM, jsonStructure));
	}
	
	
	//LOBBY MESSAGES
	/////////////////////////////////////////////////////////
	
	
	public static String composeEmptyMessage(int messageCode){
		
		return "{ \"messageType\" : \"" + String.valueOf(messageCode)+ "\", \"data\" : {} }";
	}
	
	public static String composeMessage(int messageCode, Map<String, Object> innerStructure) throws JsonProcessingException{
		
		String messageAux = CommunicationUtils.mapper.writeValueAsString(innerStructure);
		return "{ \"messageType\" : \"" + String.valueOf(messageCode)+ "\", \"data\" : " + messageAux + "}";
	}
	
	
	public static void send_MESSAGE_ALL(String message) throws IOException{
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		jsonStructure.put("message", message);
				
		sendMessage(composeMessage(Messages.Lobby.ToServer.MESSAGE_ALL, jsonStructure));
	}
	
	public static void send_CHANGE_TEAM() throws IOException{
		
		sendMessage(composeEmptyMessage(Messages.Lobby.ToServer.CHANGE_TEAM));		
	}
	
	public static void send_CHANGE_NAME(String newName) throws IOException{
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		jsonStructure.put("name", newName);
		
		sendMessage(composeMessage(Messages.Lobby.ToServer.CHANGE_NAME, jsonStructure));
	}
	
	public static void send_CHANGE_PROFESSION(String profession) throws IOException{
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		jsonStructure.put("profession", profession);
		
		sendMessage(composeMessage(Messages.Lobby.ToServer.CHANGE_PROFESSION, jsonStructure));		
	}
	
	public static void send_CHOOSE_WEAPONS(){
		//TODO implement this later on, when there will be a larger number of weapons available. 
		//Now each profession has a given number of weapons and that's that.
	}
	
	public static void send_READY(boolean ready){
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		jsonStructure.put("ready", String.valueOf(ready));		
		
		try {
			sendMessage(composeMessage(Messages.Lobby.ToServer.READY, jsonStructure));
		} 
		
		catch (JsonProcessingException e) {
			Log.println(Log.ERROR, "send_READY", e.toString());
		} 
		
		catch (IOException e) {
			Log.println(Log.ERROR, "send_READY", e.toString());
		}
	}
	
	
	

	
	
	
	
	
	public static void init(Socket socket){
		
		try {
			CommunicationUtils.mapper = new ObjectMapper();
			MessageSender.socket = socket;
			toServer_Stream = socket.getOutputStream();
			toServer_Writer = new BufferedWriter(new OutputStreamWriter(toServer_Stream));
		}
		
		catch (IOException e) {			
			Log.println(Log.ERROR, "MessageSender", "Sender could not initialize !");
			Log.println(Log.ERROR, "MessageSender", e.toString());
			e.printStackTrace();
		}
	}

	
	private static void sendMessage(String message) throws IOException{
		if(message!=null)
			toServer_Writer.write(message+'\n');		
			toServer_Writer.flush();
	}
	
	
	private static void sendMessage(Map<String, Object> jsonMessage) throws IOException{
		String encodedMessage = CommunicationUtils.mapper.writeValueAsString(jsonMessage);
		sendMessage(encodedMessage);
	}
	
		
	public static void stop(){		
		try {			
			MessageSender.toServer_Writer.flush();
			MessageSender.socket.shutdownInput();
			MessageSender.socket.shutdownOutput();
			MessageSender.socket.close();
		} 
		
		catch (IOException e) {			
			Log.e("MessageSender", "Could not close MessageSender ! Reason: "+e.toString());
		}
		
		
		
		
	}

}
