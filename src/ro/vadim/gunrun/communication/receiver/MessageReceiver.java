package ro.vadim.gunrun.communication.receiver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import ro.vadim.gunrun.communication.CommunicationUtils;
import ro.vadim.gunrun.communication.messages.MessageHandler;
import ro.vadim.gunrun.communication.messages.Messages;
import ro.vadim.gunrun.communication.messages.Messages.Admin;
import ro.vadim.gunrun.communication.messages.Messages.InGame;
import ro.vadim.gunrun.communication.messages.Messages.Lobby;
import ro.vadim.gunrun.communication.messages.Messages.InGame.FromServer;
import ro.vadim.gunrun.config.Message;
import ro.vadim.gunrun.game.fragments.LobbyFragment;
import ro.vadim.gunrun.game.fragments.MainMenuFragment;
import ro.vadim.gunrun.game.fragments.MainMenuSettingsFragment;
import ro.vadim.gunrun.game.gameelements.Player;
import ro.vadim.gunrun.game.storage_and_data.StaticData_Players;
import ro.vadim.gunrun.game.storage_and_data.StaticData_ProfessionsAndWeapons;
import ro.vadim.gunrun.game.utils.AdminUtils;
import ro.vadim.gunrun.game.utils.FragmentUtils;
import ro.vadim.gunrun.game.utils.GameFragmentMapUtils;

import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsonschema.JsonSchema;




public class MessageReceiver implements Runnable{

	private Socket socket = null;
		
	private InputStream fromServer_Stream = null;
	private BufferedReader fromServer_Reader = null;
	
	
	public static boolean helloReceived = false;
	
	public static boolean configReceived = false;
   
	public static boolean stop = false;
	
	
	public static void stop(){
		stop = true;
	}
	
	
	public MessageReceiver(Socket socket) {
		this.socket = socket;
		MessageReceiver.configReceived = false;
		MessageReceiver.helloReceived = false;
	}
		
	public Map<String, Object> decodeMessage(String message) throws IOException{
				
		if(CommunicationUtils.mapper == null)
			CommunicationUtils.mapper = new ObjectMapper();
		
		Log.println(Log.INFO, "DECODING MESSAGE", message);
		
		try{
			
			Map<String, Object> jsonObject = CommunicationUtils.mapper.readValue(message, HashMap.class); 
											//GsonJson.jsonParser.fromJson(message, Message.class);
			
			if(jsonObject == null)
				Log.println(Log.INFO, "MESSAGE AFTER DECODING : ", "IT'S NULL !!!");
			
			return jsonObject;
		}		
		
		catch (JsonParseException e) {
			Log.println(Log.WARN, "JSON ERROR (JsonParseException) (original message) : ", message);
			Log.println(Log.WARN, "JSON ERROR (JsonParseException)", e.toString());			
		} 
		
		catch (JsonMappingException e) {
			Log.println(Log.WARN, "JSON ERROR (JsonMappingException) (original message) : ", message);
			Log.println(Log.WARN, "JSON ERROR (JsonMappingException)", e.toString());
			
		}
				
			
		return null;							
		
		
	}
	
	
	
	
	public void on(int messageType, Map<String, Object> jsonObject, MessageHandler messageManager){
							
		//Log.println(Log.INFO, "MANAGING MESSAGE", "...");
		
		if(jsonObject == null){
			//Log.println(Log.INFO, "MANAGING MESSAGE (MESSAGE NULL)", "MESSAGE NULL");
			return;
		}
		
		if(!jsonObject.containsKey("messageType")){
			System.out.println("the Json object passed as an argument does not have a \"messageType\" field.");			
			return;	
		}
		
		int receivedMessageType = Integer.valueOf(String.valueOf(jsonObject.get("messageType")));
		//Log.println(Log.INFO, "MESSAGE TYPE", "["+String.valueOf(jsonObject.get("messageType"))+"]");
		
		
		if(receivedMessageType == messageType){
			Log.println(Log.INFO, "FOUND MESSAGE TYPE", "getting into it. messageType : "+String.valueOf(jsonObject.get("messageType")));
			
			if(jsonObject.containsKey("data")){
				Log.println(Log.INFO, "GETTING DATA FROM MESSAGE", "...");
				messageManager.manageMessage((Map<String, Object>) jsonObject.get("data"));
			}
			else{
				System.out.println("correct message type, but no data...");
			}
		}
	}
	
	private String getNextMessage(){
		try {
			return fromServer_Reader.readLine();
		} 
		
		catch (IOException e) {
			Log.println(Log.ERROR, "getNextMessage: Error : ", e.toString());
			return null;
		}		
	}
	
	public void handleMessages(){
		
			
		Log.println(Log.INFO, "RECEIVED MESSAGE HANDLER", "handleMessages() started");
		
		
		try{
			fromServer_Stream =  this.socket.getInputStream();
			fromServer_Reader = new BufferedReader(new InputStreamReader(fromServer_Stream));
		}
		catch(IOException e){
			Log.println(Log.ERROR, "handleMessages : stream could not be opened! :", e.toString());
			return;
		}
		
		
		
		String receivedMessage = "";
		while(((receivedMessage = getNextMessage()) != null) && (stop==false)){
			
			Log.println(Log.INFO, "RECEIVED MESSAGE", receivedMessage);
			
			
			Map<String, Object> messageObject;
			
			try {
				
				messageObject = decodeMessage(receivedMessage);
				if(messageObject == null)
					throw new IOException("MessageObject is null. ");
				
				Log.println(Log.INFO, "MESSAGE after decoding : ", messageObject.toString());				
				
				for(String key : messageObject.keySet())			
					Log.println(Log.INFO, "MESSAGE HAS FIELD :", key );
				
			} 
			
			catch (IOException e) {				
				Log.println(Log.WARN, "handleMessages (error during message decoding)", e.toString());
				messageObject = null;
			}
			
			
			//MESSAGES FROM SERVER 
			
			//IN-GAME MESSAGES
			///////////////////////////////////////////////////////////////////////////////
			
			
			
			int CHANGE_POSITION = 3;	// {player: playerUUID, latitude: newLatitude, longitude: newLongitude}
			int SHOOT = 4;				// {player: playerUUID, target: targetUUID, weapon: weapon_name, damage: damageAmount, (optional)timestamp: timeStamp}
			int MESSAGE_TEAM = 5;		// {player: playerUUID, message: newMessage}
			int GAME_START = 6;			// {} //broadcast message that is sent from the server to let all the players know 
										//    //that they are sufficiently far from each other and their weapons are now considered active
			
			
			on(Messages.InGame.FromServer.GAME_START, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					
					MessageReceiverUtils.manage_GAME_START(jsonData);					
				}
			});
						
			on(Messages.InGame.FromServer.MESSAGE_TEAM, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					
					MessageReceiverUtils.manage_MESSAGE_TEAM(jsonData);					
				}
			});
						
			on(Messages.InGame.FromServer.CHANGE_POSITION, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					
					MessageReceiverUtils.manage_CHANGE_POSITION(jsonData);					
				}
			});
						
			on(Messages.InGame.FromServer.SHOOT, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					
					MessageReceiverUtils.manage_SHOOT(jsonData);					
					
				}
			});
			
			
			
			
			
			on(Messages.Lobby.FromServer.ENTER_GAME, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					MessageReceiverUtils.manage_ENTER_GAME(jsonData);						
				}
			});
		
			on(Messages.Lobby.FromServer.COUNTDOWN, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					MessageReceiverUtils.manage_COUNTDOWN(jsonData);						
				}
			});
			
			on(Messages.Lobby.FromServer.CHANGE_NAME, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					MessageReceiverUtils.manage_CHANGE_NAME(jsonData);
				}
			});
			
			on(Messages.Lobby.FromServer.CONFIGURATION, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {						
					MessageReceiverUtils.manage_CONFIGURATION(jsonData);
				}
			});	
									
			on(Messages.Lobby.FromServer.CHANGE_PROFESSION, messageObject, new MessageHandler() {
				
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					MessageReceiverUtils.manage_CHANGE_PROFESSION(jsonData);
				}
			});
			
			on(Messages.Lobby.FromServer.CHANGE_TEAM, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					MessageReceiverUtils.manage_CHANGE_TEAM(jsonData);
				}
			});
						
			on(Messages.Lobby.FromServer.MESSAGE_ALL, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					MessageReceiverUtils.manage_MESSAGE_ALL(jsonData);				
				}
			});
			
			on(Messages.Lobby.FromServer.PLAYER_JOINED, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					MessageReceiverUtils.manage_PLAYER_JOINED(jsonData);
				}
			});
						
			on(Messages.Lobby.FromServer.PLAYER_LEFT, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					MessageReceiverUtils.manage_PLAYER_LEFT(jsonData);					
				}
			});
						
			on(Messages.Lobby.FromServer.READY, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					MessageReceiverUtils.manage_READY(jsonData);
				}
			});

			
			
			//ADMIN MESSAGES
			////////////////////////////////////////////////////////////////////////////////
			
					
			on(Messages.Admin.FromServer.HELLO, messageObject, new MessageHandler() {

				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					
					MessageReceiverUtils.manage_HELLO(jsonData);
				}
			});
		
			on(Messages.Admin.FromServer.GAMES_LIST, messageObject, new MessageHandler() {

				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					
					MessageReceiverUtils.manage_GAMES_LIST(jsonData);
				}
			});
			
			on(Messages.Admin.FromServer.GAME_CREATED, messageObject, new MessageHandler() {
				
				@Override
				public void manageMessage(Map<String, Object> jsonData) {
					
					MessageReceiverUtils.manage_GAME_CREATED(jsonData);					
				}
			});
			
		}
		
		if(receivedMessage == null){
			if(stop != true){
				Log.println(Log.WARN, "RECEIVER STOPPED !!!", "received message = null");
				AdminUtils.buildConnectionLostAlertMessage();
				FragmentUtils.loadMainMenuFragment(FragmentUtils.currentActivity);				
			}
		}
		
		if(stop == true){
			Log.println(Log.WARN, "RECEIVER STOPPED !!!", "stop = true");
			stop = false;
		}
	
		
	}
		
	@Override
	public void run() {
		
		Log.println(Log.INFO, "STARTING RECEIVER THREAD", "...");
		handleMessages();
		
	}
	
}
