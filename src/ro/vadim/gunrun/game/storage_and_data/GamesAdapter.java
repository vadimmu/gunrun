package ro.vadim.gunrun.game.storage_and_data;

import ro.vadim.gunrun.R;
import ro.vadim.gunrun.game.misc.Game;
import ro.vadim.gunrun.game.utils.FragmentUtils;
import ro.vadim.gunrun.game.utils.GameElementUtils;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GamesAdapter extends ArrayAdapter<Game>{

    Context context; 
    int layoutResourceId = R.layout.listview_item_row;  
    Game data[] = null;
    
    public GamesAdapter(Context context, int layoutResourceId, Game[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }
    
    
    private String getGameString(Game game){
    	    	
		String type = "Game type: ";
		if(game.getGameType().intValue() == Game.GAME_TYPE_NORMAL)
			type += "Regular game";
		
		else if(game.getGameType().intValue() == Game.GAME_TYPE_DUEL)
			type += "Duel";
		
		else if(game.getGameType().intValue() == Game.GAME_TYPE_DAVID_VS_GOLIATH)
			type += "David vs. Goliath";
		
		else 
			type += "UNKNOWN GAME TYPE";
		
		String gameString = game.getName() + " ( "+type+" )";
		
		gameString += "\n( No. of Players : "+String.valueOf(game.getCountAwayTeam() + game.getCountHomeTeam())+" ) ";
		
		if(game.getInProgress().booleanValue() == true)
			gameString+= " [ in progress ] ";
				
    	return gameString;
    }
    
        
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        GameHolder holder = null;
        
        if(row == null)
        {        	
            LayoutInflater inflater = FragmentUtils.currentActivity.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            
            holder = new GameHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
            
            row.setTag(holder);
        }
        else
        {
            holder = (GameHolder)row.getTag();
        }
        
        
        Game game = data[position];
        if(game != null){        	
	        holder.txtTitle.setText(getGameString(game));
	        holder.imgIcon.setImageResource(GameElementUtils.getGameIcon(game));
        }
        
        return row;
    }
    
    static class GameHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
    }
}