package ro.vadim.gunrun.game.storage_and_data;

import java.util.ArrayList;

import ro.vadim.gunrun.R;
import ro.vadim.gunrun.game.gameelements.Player;
import ro.vadim.gunrun.game.misc.Game;
import ro.vadim.gunrun.game.storage_and_data.GamesAdapter.GameHolder;
import ro.vadim.gunrun.game.utils.FragmentUtils;
import ro.vadim.gunrun.game.utils.GameElementUtils;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TeamAdapter extends ArrayAdapter<Player>{

    Context context; 
    int layoutResourceId = R.layout.listview_item_row;  
    ArrayList<Player> data = null;
    
    public TeamAdapter(Context context, int layoutResourceId, ArrayList<Player> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }    
    
    private String getPlayerString(Player player){
    	    	
		if(player != null){
			
			String playerString = player.getNickname()+'\n'+"("+player.getProfessionTitle()+")";
			
			if(player.isReady)
				playerString+=" [ READY ]";
			
			return playerString;
		}
		else
			return "PLAYER NULL";
    	
    }
    
    
    
    
    
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        TeamHolder holder = null;
        
        if(row == null)
        {        	
            LayoutInflater inflater = FragmentUtils.currentActivity.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            
            holder = new TeamHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
            
            row.setTag(holder);
        }
        else
        {
            holder = (TeamHolder)row.getTag();
        }        
        
        Player player = data.get(position);
        if(player != null){        	
	        holder.txtTitle.setText(getPlayerString(player));
	        holder.imgIcon.setImageResource(GameElementUtils.getPlayerIcon(player));
        }
        
        return row;
    }
    
    static class TeamHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
    }
}
