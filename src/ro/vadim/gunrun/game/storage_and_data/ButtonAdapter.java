package ro.vadim.gunrun.game.storage_and_data;

import java.util.ArrayList;

import ro.vadim.gunrun.game.gameelements.Weapon;
import ro.vadim.gunrun.game.utils.GameElementUtils;
import ro.vadim.gunrun.game.utils.GameFragmentButtonsUtils;
import ro.vadim.gunrun.game.utils.GameFragmentDrawingUtils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;

public class ButtonAdapter extends BaseAdapter {  
	private Context mContext;
	private ArrayList<Weapon> weapons = null;
	  
	// Gets the context so it can be used later  
	public ButtonAdapter(Context c) {  
		mContext = c;
		weapons = new ArrayList<Weapon>(StaticData_Players.currentPlayer.getWeapons());
	}
	
	// Total number of things contained within the adapter  
	public int getCount() {  
		return weapons.size();
	}  
	  
	// Require for structure, not really used in my code.  
	public Object getItem(int position) {  
		return null;  
	}  
	  
	// Require for structure, not really used in my code. Can  
	// be used to get the id of an item in the adapter for  
	// manual control.  
	public long getItemId(int position) {  
		return position;  
	}  
	  
	public View getView(int position, View convertView, ViewGroup parent) {  
		Button weaponButton = null;
		
		if (convertView == null) {
			
			weaponButton = new Button(mContext);
			
			
			LayoutParams layoutParams = new LayoutParams(100, 55);
			layoutParams.height = LayoutParams.WRAP_CONTENT;
			layoutParams.width = LayoutParams.WRAP_CONTENT;
			
			
			weaponButton.setBackgroundResource(GameElementUtils.getWeaponIcon(weapons.get(position)));						
			weaponButton.setId(GameFragmentDrawingUtils.generateViewId());
			weaponButton.setEnabled(false);
			weaponButton.setId(position);
			weaponButton.setPadding(8, 8, 8, 8);
			
			final Weapon weaponAUX = weapons.get(position);
			final Button weaponButtonAUX = weaponButton;
			
			weaponButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
										
					if(!weaponButtonAUX.isClickable())
						return;
					
					boolean shotWasPerformed = GameFragmentButtonsUtils.shoot(v, weaponAUX, StaticData_Players.selectedPlayerUUID);
					
					
				}
			});
			
			weaponButton.setOnLongClickListener(new OnLongClickListener() {
				
				@Override
				public boolean onLongClick(View v) {
					
					GameFragmentDrawingUtils.drawWeaponRange(weaponAUX);					
					return true;
				}
			});
			
			weaponButton.setLayoutParams(layoutParams);
		}
		
		else {
			weaponButton = (Button) convertView;  
		}		
		
		return weaponButton;  
	}  
}  
