package ro.vadim.gunrun.game.storage_and_data;

import java.util.ArrayList;

import ro.vadim.gunrun.R;
import ro.vadim.gunrun.game.gameelements.Weapon;
import ro.vadim.gunrun.game.storage_and_data.WeaponsAdapter.WeaponHolder;
import ro.vadim.gunrun.game.utils.FragmentUtils;
import ro.vadim.gunrun.game.utils.GameElementUtils;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfessionsAdapter extends ArrayAdapter<String>{

    Context context; 
    int layoutResourceId = R.layout.listview_item_row;  
    ArrayList<String> data = null;
    
    public ProfessionsAdapter(Context context, int layoutResourceId, ArrayList<String> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }
    
    
    
    
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ProfessionHolder holder = null;
        
        if(row == null)
        {        	
            LayoutInflater inflater = FragmentUtils.currentActivity.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            
            holder = new ProfessionHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
            
            row.setTag(holder);
        }
        else
        {
            holder = (ProfessionHolder)row.getTag();
        }        
        
        String professionTitle = data.get(position);
        if(professionTitle != null){        	
	        holder.txtTitle.setText(professionTitle);
	        holder.imgIcon.setImageResource(GameElementUtils.getProfessionIcon(professionTitle));
        }
        
        return row;
    }
    
    static class ProfessionHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
    }
}
