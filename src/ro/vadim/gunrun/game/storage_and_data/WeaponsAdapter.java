package ro.vadim.gunrun.game.storage_and_data;

import java.util.ArrayList;

import ro.vadim.gunrun.R;
import ro.vadim.gunrun.game.gameelements.Player;
import ro.vadim.gunrun.game.gameelements.Weapon;
import ro.vadim.gunrun.game.storage_and_data.TeamAdapter.TeamHolder;
import ro.vadim.gunrun.game.utils.FragmentUtils;
import ro.vadim.gunrun.game.utils.GameElementUtils;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class WeaponsAdapter extends ArrayAdapter<Weapon>{

    Context context; 
    int layoutResourceId = R.layout.listview_item_row;  
    ArrayList<Weapon> data = null;
    
    public WeaponsAdapter(Context context, int layoutResourceId, ArrayList<Weapon> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }    
    
    private String getWeaponString(Weapon weapon){
    	    	
		if(weapon != null){
			
			String weaponString = weapon.getName().toUpperCase()+
					" [ Damage: "+String.valueOf(weapon.getDamage())+" Range: "+String.valueOf(weapon.getRange())+
					" Cooldown: "+String.valueOf(weapon.getCooldown())+" Duration: "+String.valueOf(weapon.getDuration())+" ]";
			
			return weaponString;
		}
		
		else
			return "N/A";
		
    }
    
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        WeaponHolder holder = null;
        
        if(row == null)
        {        	
            LayoutInflater inflater = FragmentUtils.currentActivity.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            
            holder = new WeaponHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);
            
            row.setTag(holder);
        }
        else
        {
            holder = (WeaponHolder)row.getTag();
        }        
        
        Weapon weapon = data.get(position);
        if(weapon != null){        	
	        holder.txtTitle.setText(getWeaponString(weapon));
	        holder.imgIcon.setImageResource(GameElementUtils.getWeaponIcon(weapon));
        }
        
        return row;
    }
    
    static class WeaponHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
    }
}

