package ro.vadim.gunrun.game.storage_and_data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.UUID;

import ro.vadim.gunrun.game.gameelements.Player;
import ro.vadim.gunrun.game.gameelements.Profession;
import ro.vadim.gunrun.game.gameelements.Weapon;
import ro.vadim.gunrun.game.utils.FragmentUtils;
import ro.vadim.gunrun.game.utils.GameFragmentMapUtils;
import ro.vadim.gunrun.R;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.util.Log;



public class StaticData_Players {
	
	
	public static final int HOME_TEAM = 0;
	public static final int AWAY_TEAM = 1;
	
	
	public static final int VISIBILITY_RANGE = 250;
	
	
	public static Drawable marker_selectedPlayer = null;
	public static Drawable marker_currentPlayer = null; 
	public static Drawable marker_friend = null;
	public static Drawable marker_enemy = null;
		
	public static Integer maxPlayersPerTeam = 20;
		
	public static LinkedHashMap<UUID, Player> friends = null;
	public static LinkedHashMap<UUID, Player> enemies = null;
	
	public static UUID currentPlayerUUID = null;
	public static Player currentPlayer = null;	
	public static int currentPlayerTeam = -1;
	
	public static UUID selectedPlayerUUID = null;
	
	
		
	
	public static int countPlayers(){
		
		int currentPlayer = 0;
		if(StaticData_Players.currentPlayer != null) currentPlayer = 1;
		return friends.size()+enemies.size()+currentPlayer;
	}
	
	public static int countFriends(){
		
		return friends.size();
	}

	public static int countEnemies(){
		
		return enemies.size();
	}
	
	
	
	
	
	public static Player getFriendById(UUID uuid){
		if(uuid == null) return null;
		
		Player friend = null;
		friend = friends.get(uuid);
		
		return friend;	
	}
	
	public static Player getEnemyById(UUID uuid){
		if(uuid == null) return null;
		
		Player enemy = null;
		enemy = enemies.get(uuid);
		return enemy;
	}
	
	public static Player getPlayerById(UUID uuid){
		if(uuid == null) return null;
		
		Player player = null;
		if(uuid.equals(currentPlayerUUID)) 
			return currentPlayer;
		
		if(friends.containsKey(uuid)){
			player = friends.get(uuid);
			if(player != null) 
				return player;
		}
		if(enemies.containsKey(uuid)){
			player = enemies.get(uuid);		
			return player;
		}
		
		Log.println(Log.ERROR, "getPlayerById : PLAYER NOT FOUND", uuid.toString());			
		return null;		
	}
	

	
	
	
	public static void initCurrentPlayer(){
		
		Log.println(Log.INFO, "INIT CURRENT PLAYER", "...");
		
		HashMap<String, String> playerData = Storage.loadCurrentPlayerData(FragmentUtils.currentActivity);
				
		currentPlayer = new Player(playerData.get("currentPlayerName"), playerData.get("currentPlayerProfessionTitle"), 0, 0);		
		currentPlayerUUID = StaticData_Players.currentPlayerUUID;
	}
	
	
	
	
	
	public static synchronized void addPlayer(Player player, UUID uuid, boolean friend){
		
		if(player.getPosition() == null)
			player.setPosition(0, 0);
		
		if(friend) friends.put(uuid, player);
		else enemies.put(uuid, player);
	}
	
	public static synchronized void removePlayer(UUID uuid){
		friends.remove(uuid);
		enemies.remove(uuid);
	}
	
	
	
	public static boolean playerIsMe(UUID uuid){
		
		if(uuid == null) return false;
		
		if(uuid.compareTo(currentPlayerUUID) == 0)
			return true;
		
		return false;
	}
	
	public static boolean playerIsFriend(UUID uuid){
		
		if(uuid == null) return false;
		
		return friends.containsKey(uuid);
	}
	
	public static boolean playerIsEnemy(UUID uuid){
		
		if(uuid == null) return false;
		
		return enemies.containsKey(uuid);
	}
	
	public static boolean playerIsSelected(UUID uuid){
		if(uuid.equals(selectedPlayerUUID))
			return true;
		
		return false;
	}
	
	
	
	public static synchronized void currentPlayer_changeTeam(){
		
		if(StaticData_Players.currentPlayerTeam == HOME_TEAM)
			StaticData_Players.currentPlayerTeam = AWAY_TEAM;
		else if(StaticData_Players.currentPlayerTeam == AWAY_TEAM)
			StaticData_Players.currentPlayerTeam = HOME_TEAM;
		
		LinkedHashMap<UUID, Player> copyFriends = new LinkedHashMap<UUID, Player>(StaticData_Players.friends);
		LinkedHashMap<UUID, Player> copyEnemies = new LinkedHashMap<UUID, Player>(StaticData_Players.enemies);
		
		StaticData_Players.friends = new LinkedHashMap<UUID, Player> (copyEnemies);
		StaticData_Players.enemies = new LinkedHashMap<UUID, Player> (copyFriends);
		
	} 
	
	public static synchronized void currentPlayer_changeProfession(String professionTitle){
		StaticData_Players.currentPlayer.changeProfession(professionTitle);
		
		ArrayList<Weapon> weapons = new ArrayList<Weapon>();
		Profession p = StaticData_ProfessionsAndWeapons.professions.get(professionTitle);
		for(String weapon : p.weapons){
			Weapon w = StaticData_ProfessionsAndWeapons.weapons.get(weapon);
			weapons.add(w);
		}					
		StaticData_Players.currentPlayer.setWeapons(weapons);
	}
	
	
}
