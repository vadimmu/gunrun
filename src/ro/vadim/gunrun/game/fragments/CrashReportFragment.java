package ro.vadim.gunrun.game.fragments;

import ro.vadim.gunrun.R;
import ro.vadim.gunrun.game.storage_and_data.Storage;
import ro.vadim.gunrun.game.utils.FragmentUtils;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class CrashReportFragment extends Fragment{

	TextView crashReportTextView = null;
	
	
	private void setupCrashReportText(View view){
				
		crashReportTextView = (TextView) view.findViewById(R.id.text_crash_report);
		
		String crashReport = Storage.loadCrashReport(getActivity());
		
		if(crashReport == null)
			crashReportTextView.setText("N/A");
		
		else
			crashReportTextView.setText(crashReport);
	}
	
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {    
	    
	    View view = inflater.inflate(R.layout.crash_report, container, false);
	    FragmentUtils.currentFragment = this;
	    setupCrashReportText(view);
	    
	    return view;
    }
	
	
}
