package ro.vadim.gunrun.game.fragments;

import ro.vadim.gunrun.game.utils.FragmentUtils;
import ro.vadim.gunrun.R;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

public class LoadingFragment extends Fragment{
	
	Activity parentActivity = null;
	ProgressBar progressBar = null;
	Thread loadingThread = null;
	
	
	public static int LOADING_GAME_CLIENT = 0;
	public static int LOADING_GAME_MANAGEMENT_CLIENT = 1;
	
	public int whatsLoading = -1;
	
	
	public void setParentActivity(Activity parentActivity){		
		this.parentActivity = parentActivity;		
	}
	
	/**set this up before commiting the FragmentTransaction**/
	public void setupLoadingWorker(final LoadingWorker loadingWorker){
		loadingThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				loadingWorker.doWork();
				loadingWorker.doAfterWork();
			}
		});
	}
	
	
	public LoadingFragment(){
		super();
	}
		
	
	private void setupLoadingElement(View view){
		progressBar = (ProgressBar) view.findViewById(R.id.loading);
	}
	
	
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {    
    	
	    // Inflate the layout for this fragment
	    View view = inflater.inflate(R.layout.fragment_loading, container, false);	    
	    
	    FragmentUtils.setCurrentFragment(this);
	    FragmentUtils.setCurrentContext(view.getContext());
	    
	    
	    
	    
	    setupLoadingElement(view);
	    
	    loadingThread.start();
	    
	    return view;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Log.println(Log.INFO, "context ?", String.valueOf(parentActivity));
        
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
    
    
    
    
    public interface LoadingWorker{    	
    	public void doWork();    	
    	public void doAfterWork();    	
    }
    
    
    
    
    
    
}
