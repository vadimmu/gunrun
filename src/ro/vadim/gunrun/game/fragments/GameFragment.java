package ro.vadim.gunrun.game.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import ro.vadim.gunrun.communication.sender.MessageSender;
import ro.vadim.gunrun.game.gameelements.Player;
import ro.vadim.gunrun.game.gameelements.Profession;
import ro.vadim.gunrun.game.gameelements.Weapon;
import ro.vadim.gunrun.game.misc.WeaponReportingTask;
import ro.vadim.gunrun.game.storage_and_data.StaticData_Players;
import ro.vadim.gunrun.game.storage_and_data.StaticData_ProfessionsAndWeapons;
import ro.vadim.gunrun.game.utils.AdminUtils;
import ro.vadim.gunrun.game.utils.FragmentUtils;
import ro.vadim.gunrun.game.utils.GameFragmentButtonsUtils;
import ro.vadim.gunrun.game.utils.GameFragmentDrawingUtils;
import ro.vadim.gunrun.game.utils.GameFragmentMapUtils;
import ro.vadim.gunrun.game.utils.GameFragmentPeriodicRefreshRunnable;
import ro.vadim.gunrun.R;



import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.Overlay;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.app.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.GridLayout;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class GameFragment extends SupportMapFragment{

	private static Activity parentActivity = null;	
	private static Thread refreshThread = null;
	
		
	public static View mapFragmentView = null;	
	public static RelativeLayout mapContainer = null;
	public static GoogleMap googleMap = null;	
	
		
	public static LinearLayout toggleButtonsLayout = null;
	public static LinearLayout weaponButtonsLayout = null;
	public static LinearLayout powerupsLayout = null;
		
		
	public static ImageButton previousTargetButton = null;
	public static ImageButton nextTargetButton = null;
	public static ToggleButton friendsToggleButton = null;
	public static Button selectMeButton = null;	
	
	
	public static ImageView myHealthIcon = null;
	public static TextView myHealthTextView = null;
	public static TextView selectedPlayerInfoTextView = null;
	
	public static Spinner messagesSpinner = null;
	
		
	
	private static final BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>(10);
    public static ThreadPoolExecutor executor = new ThreadPoolExecutor(10, 15, 3, TimeUnit.SECONDS, workQueue);
    
		
    public static Circle weaponRangeCircle = null;
    
    
    
    
	public GameFragment(){
		super();		
	}
	
	
	
	
	public void setParentActivity(Activity parentActivity) {
		this.parentActivity = parentActivity;
	}
	

	
		
	
	
	public void setupGameFragment(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
		container.removeAllViews();
		
		mapFragmentView = super.onCreateView(inflater, container, savedInstanceState);		
		googleMap = this.getMap();
		
		mapContainer = new RelativeLayout(getActivity());
		mapContainer.addView(mapFragmentView, new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		
		FragmentUtils.currentFragment = this;
		FragmentUtils.currentContext = mapContainer.getContext();
		
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
		if(!AdminUtils.areGooglePlayServicesAvailable(container.getContext())){
		    AdminUtils.buildAlertMessageNoGooglePlayServices(container.getContext());
			return container;
		}
		
		setupGameFragment(inflater, container, savedInstanceState);
		
	    GameFragmentButtonsUtils.setupTargetButtons(mapContainer);	    
	    GameFragmentButtonsUtils.setupHealthTextView(mapContainer);
	    GameFragmentButtonsUtils.setupPowerupsLayout(mapContainer);	    
	    GameFragmentButtonsUtils.setupMessagesSpinner(mapContainer);	    	    
		GameFragmentDrawingUtils.setupGoogleMap(googleMap);
		GameFragmentButtonsUtils.setupWeaponButtons(mapContainer);
		
		
		GameFragmentDrawingUtils.redrawAll();
		
		return mapContainer;
    }
	
	
	
	
	
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        
        Log.println(Log.INFO, "context ?", String.valueOf(parentActivity));
        
    }
	
    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
    } 
    
    
    @Override
    public void onHiddenChanged(boolean hidden) {
    	super.onHiddenChanged(hidden);
    }
    
}
