package ro.vadim.gunrun.game.fragments;

import java.io.IOException;
import java.util.ArrayList;

import ro.vadim.gunrun.communication.TcpGameClient;
import ro.vadim.gunrun.communication.TcpGameManagementClient;
import ro.vadim.gunrun.communication.sender.MessageSender;
import ro.vadim.gunrun.game.gameelements.Profession;
import ro.vadim.gunrun.game.misc.Game;
import ro.vadim.gunrun.game.misc.Games;
import ro.vadim.gunrun.game.storage_and_data.GamesAdapter;
import ro.vadim.gunrun.game.storage_and_data.StaticData_Players;
import ro.vadim.gunrun.game.storage_and_data.StaticData_ProfessionsAndWeapons;
import ro.vadim.gunrun.game.utils.AdminUtils;
import ro.vadim.gunrun.game.utils.FragmentUtils;
import ro.vadim.gunrun.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class GameSelectionFragment extends Fragment{
	
	
	public ImageButton createGameButton = null;
	public ImageButton refreshGamesButton = null;
	public ImageButton joinGameButton = null;
	public ImageButton backToMainMenuButton = null;
	public ListView gamesListView = null;
		
	
	private LayoutInflater inflater = null;
	
	Game[] gamesList = null;
	
	private GamesAdapter gamesAdapter = null;
	
	
	//private ArrayList<String> games = null;
	
	Integer selectedGame = null;
	
	
	private void refreshGamesList_unsafe(){
						
		if(Games.gamesList == null){
			gamesList = new Game[0];			
			return;
		}
		
		gamesList = new Game[Games.gamesList.size()];
		Games.gamesList.toArray(gamesList);
		
		gamesAdapter = new GamesAdapter(getActivity(), R.layout.listview_item_row, gamesList);
		gamesListView.setAdapter(gamesAdapter);
		gamesListView.postInvalidate();
	}
	
	
	public void refreshGamesList(){
		if(!FragmentUtils.isRunningOnUiThread()){
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					refreshGamesList_unsafe();
				}
			});
		}
		
		else{
			refreshGamesList_unsafe();
		}
	}
	
	
	public void createGameCreationDialog(View view){
		
		
		View extraComponents = inflater.inflate(R.layout.dialog_create_game, null);
		
		final EditText gameNameText = (EditText) extraComponents.findViewById(R.id.text_game_name);
		final Spinner gameTypeSpinner = (Spinner) extraComponents.findViewById(R.id.spinner_game_type);		
		
		ArrayList<String> gameTypes = new ArrayList<String>(3);
		gameTypes.add("Regular game");
		gameTypes.add("Duel");
		gameTypes.add("David vs. Goliath");
		
		
		final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_dropdown_item, gameTypes);
				
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		gameTypeSpinner.setAdapter(dataAdapter);
		
		
		selectedGame = 0;		
				
		gameTypeSpinner.setSelection(selectedGame);		
		gameTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {				
					
				selectedGame = arg2;
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {				
				
			}
		});		
		
		
		
		
		final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
		
		builder.setView(extraComponents);
		
	    builder.setCancelable(false)
	    		.setPositiveButton("Create", new DialogInterface.OnClickListener() {
	               public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
	            	   MessageSender.send_CREATE_GAME(gameNameText.getText().toString(), selectedGame);
	               }	               
	    		})
	    		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();						
					}
				});
	    		
	    final AlertDialog alert = builder.create();
	    alert.show();
	}
			
	public void setupGamesListView(View view){
				
		gamesListView = (ListView) view.findViewById(R.id.listView_games_list);
		
		//refreshGamesList();
		
		
		
		gamesListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				if(selectedGame != null)
					arg0.getChildAt(selectedGame.intValue()).setBackgroundResource(R.color.transparentWhite);
				
				selectedGame = new Integer(arg2);
				arg0.getChildAt(selectedGame.intValue()).setBackgroundResource(R.color.transparentYellow);
			}
			
		});
	
	}
	
	
	
	public void setupCreateGamesButton(View view){
		createGameButton = (ImageButton) view.findViewById(R.id.button_create_game);
		
		final View auxView = view;
		
		createGameButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				createGameCreationDialog(auxView);
			}
		});
		
	}
		
	public void setupRefreshGameButton(View view){
		refreshGamesButton = (ImageButton) view.findViewById(R.id.button_refresh_games);
		refreshGamesButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Games.refreshInProgress = true;
				Games.resetGamesList();
				refreshGamesList();
				MessageSender.send_GAMES_LIST();			
			}
		});
		
	}
	
	
	public void setupBackToMainMenuButton(View view){
		backToMainMenuButton = (ImageButton) view.findViewById(R.id.button_return_to_main_menu);
		backToMainMenuButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				TcpGameManagementClient.stopServer();
				FragmentUtils.loadMainMenuFragment(getActivity());
			}
		});
		
		
	}
	
	
	
	
	public void setupJoinGameButton(View view){
		joinGameButton = (ImageButton) view.findViewById(R.id.button_join_game);
		joinGameButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				Thread newThread = new Thread(new Runnable() {
					
					@Override
					public void run() {
						
						
						if(Games.gamesList == null)
							return;
						
						if(selectedGame == null)
							return;
						
						if(selectedGame > Games.gamesList.size() - 1)
							return;
						
						
						Game game = Games.getGameByPosition(selectedGame.intValue());
						
						if(game == null)
							return;
						
						AdminUtils.joinGame(game);
					}
				});
				
				newThread.start();
				
			}
		});
		
	}
		
	public void setupButtons(View view){
		setupCreateGamesButton(view);
		setupRefreshGameButton(view);
		setupJoinGameButton(view);
		setupBackToMainMenuButton(view);
	}
		
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {    
		    
		View view = inflater.inflate(R.layout.fragment_game_selection, container, false);		
		this.inflater = inflater; 
		
		FragmentUtils.setCurrentFragment(this);
		
		Games.resetGamesList();
		MessageSender.send_GAMES_LIST();	
		
		setupGamesListView(view);
		setupButtons(view);
		
		
		
		return view;
	}


	
	

	    

}
