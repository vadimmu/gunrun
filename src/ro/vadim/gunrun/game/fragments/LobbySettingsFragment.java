package ro.vadim.gunrun.game.fragments;



import java.io.IOException;
import java.util.ArrayList;

import ro.vadim.gunrun.communication.sender.MessageSender;
import ro.vadim.gunrun.game.gameelements.Player;
import ro.vadim.gunrun.game.gameelements.Profession;
import ro.vadim.gunrun.game.gameelements.Weapon;
import ro.vadim.gunrun.game.storage_and_data.StaticData_Players;
import ro.vadim.gunrun.game.storage_and_data.StaticData_ProfessionsAndWeapons;
import ro.vadim.gunrun.game.storage_and_data.TeamAdapter;
import ro.vadim.gunrun.game.storage_and_data.WeaponsAdapter;
import ro.vadim.gunrun.game.utils.FragmentUtils;
import ro.vadim.gunrun.R;


import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

public class LobbySettingsFragment extends Fragment {
    
	private Activity parentActivity = null;
	
	private ImageButton okButton = null;
	private Spinner professionSpinner = null;
	private EditText nameText = null;
	
	
	
	private ImageView professionIcon = null;	
	private TextView healthAmount = null;
	private ListView weaponsList = null;
	private TextView professionDescription = null;
	
	
	public ArrayList<String> professions = null;
	
	ArrayList<Weapon> weapons = null;
	
	
	
	private void setupPlayerNameText(View view){
		
		nameText = (EditText)view.findViewById(R.id.text_name);
		nameText.setText(StaticData_Players.currentPlayer.getNickname());		
	}
		
	private void setupSpinner(View view){
		
		professions = new ArrayList<String>(StaticData_ProfessionsAndWeapons.professions.size());
		
		
		for(Profession p : StaticData_ProfessionsAndWeapons.professions.values()){			
			professions.add(p.title);
		}		
		
		professionSpinner = (Spinner) view.findViewById(R.id.spinner_professions);		
		
		final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_dropdown_item, professions);
				
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		professionSpinner.setAdapter(dataAdapter);
		
		
		int position = 0;
		String currentPlayerProfession = StaticData_Players.currentPlayer.getProfessionTitle();
		if(professions.contains(currentPlayerProfession))
			position = professions.indexOf(currentPlayerProfession);
		
				
		professionSpinner.setSelection(position);		
		professionSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if(professionDescription!=null){
					
					String professionTitle = professions.get(arg2);
					
					Profession profession = StaticData_ProfessionsAndWeapons.professions.get(professionTitle);
															
					professionDescription.setText(profession.description);
				}
				
				if(weaponsList != null)
					refreshWeaponsList();
				
				
				refreshProfessionIcon();
				
				
				if(healthAmount != null)
					healthAmount.setText(
							String.valueOf(
									StaticData_ProfessionsAndWeapons.professions.get(
											professions.get(arg2)).health));
				
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	
	
    private int getProfessionIcon(String professionName){
    	    	
    	if(professionName.equals("Marine"))
    		return R.drawable.icon_profession_marine;
    	
    	if(professionName.equals("Medic"))
    		return R.drawable.icon_profession_medic;
    	
    	if(professionName.equals("Scout"))
    		return R.drawable.icon_profession_scout;
    	
    	if(professionName.equals("Sniper"))
    		return R.drawable.icon_profession_sniper;
    	
    	if(professionName.equals("All Rounder"))
    		return R.drawable.icon_profession_all_rounder;
    	
    	if(professionName.equals("Duelist"))
    		return R.drawable.icon_profession_duelist;
    	
    	if(professionName.equals("Goliath"))
    		return R.drawable.icon_profession_goliath;
    	    	
    	return R.drawable.icon_profession_marine;
    }
	
	private void refreshProfessionIcon(){
		
		String professionName = "";
		
		if(professionSpinner.getSelectedItem()!=null)
			professionName = professions.get(professionSpinner.getSelectedItemPosition());			
		
		else
			professionName = StaticData_Players.currentPlayer.getProfessionTitle();				
		
		if(professionIcon != null)
			professionIcon.setImageResource(getProfessionIcon(professionName));
			
	}
	
	
	private void setupProfessionIcon(View view){
		
		professionIcon = (ImageView) view.findViewById(R.id.image_professionIcon);
		refreshProfessionIcon();		
	}
	
	
	private void setupHealthAmount(View view){
		
		healthAmount = (TextView) view.findViewById(R.id.text_healthAmount);		
	}
		
	private void setupProfessionDescription(View view){
		
		professionDescription = (TextView) view.findViewById(R.id.text_professionDescription);		
	}
	
	
	
	private void getWeaponsList(){
		
		String selectedProfession = professions.get(professionSpinner.getSelectedItemPosition());
		ArrayList<String> weaponNames = StaticData_ProfessionsAndWeapons.professions.get(selectedProfession).weapons;
		
		if(selectedProfession == null)
			return;
				
		weapons = new ArrayList<Weapon>(weaponNames.size());		
		
		for(String weaponName: weaponNames){
			weapons.add(StaticData_ProfessionsAndWeapons.weapons.get(weaponName));			
		}		
	}
	
	
	
	private void refreshWeaponsList(){
		
		getWeaponsList();
		
		weaponsList.setAdapter(
				new WeaponsAdapter(getActivity(), R.layout.listview_item_row, weapons)
		);
	}
	
	private void setupWeaponsList(View view){
		
		weaponsList = (ListView) view.findViewById(R.id.list_weaponDetails);
		weaponsList.setClickable(false);		
		refreshWeaponsList();
	}
	
	
	private void addProfessionDetails(){
		
	}
	
	
	private void setupOkButton(View view){
		
		final View myView = view;
		
		okButton = (ImageButton) view.findViewById(R.id.button_ok);
		
		okButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				String newNickname = nameText.getText().toString();
				String newProfession = professionSpinner.getSelectedItem().toString();
				
				Log.println(Log.INFO, "CHOICES", "Nickname : "+newNickname);
				Log.println(Log.INFO, "CHOICES", "Profession : "+newProfession);
				
				StaticData_Players.currentPlayer.setNickname(newNickname);
				StaticData_Players.currentPlayer.changeProfession(newProfession);
				
				
				
				try {
					MessageSender.send_CHANGE_NAME(nameText.getText().toString());					
					MessageSender.send_CHANGE_PROFESSION(newProfession);
				} 
				
				catch (IOException e) {
					Toast toast = Toast.makeText(myView.getContext(), "Could not set personal data", Toast.LENGTH_SHORT);
					toast.show();
				}

				
				FragmentUtils.loadLobbyFragment(parentActivity);
								
			}
		});
		
		
	}
	
	
	private void setupInformationControls(View view){
		setupProfessionDescription(view);
		setupSpinner(view);
    	setupPlayerNameText(view);
    	setupOkButton(view);  
    	setupSpinner(view);    	
    	setupPlayerNameText(view);
    	setupProfessionIcon(view);
    	setupHealthAmount(view);
    	setupWeaponsList(view);
	}
	
	
	
	public LobbySettingsFragment(){
		super();		
	}
	
	public void setParentActivity(Activity parentActivity){
		this.parentActivity = parentActivity;
	}
	
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
    	View view = inflater.inflate(R.layout.fragment_lobby_config, container, false);
    	    	
    	view.setBackgroundColor(Color.WHITE);
    	
    	view.setBackgroundResource(R.drawable.background_lobby_settings_720);
    	
    	FragmentUtils.setCurrentFragment(this);
    	FragmentUtils.setCurrentContext(view.getContext());
    	
    	setupInformationControls(view);

    	
    	return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);        
    }
}