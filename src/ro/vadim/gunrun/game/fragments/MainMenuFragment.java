package ro.vadim.gunrun.game.fragments;



import java.io.IOException;

import ro.vadim.gunrun.communication.CommunicationUtils;
import ro.vadim.gunrun.communication.TcpGameClient;
import ro.vadim.gunrun.communication.receiver.MessageReceiver;
import ro.vadim.gunrun.game.storage_and_data.StaticData_Players;
import ro.vadim.gunrun.game.storage_and_data.StaticData_ProfessionsAndWeapons;
import ro.vadim.gunrun.game.storage_and_data.Storage;
import ro.vadim.gunrun.game.utils.AdminUtils;
import ro.vadim.gunrun.game.utils.FragmentUtils;
import ro.vadim.gunrun.game.utils.GameFragmentMapUtils;
import ro.vadim.gunrun.R;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.opengl.Visibility;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

public class MainMenuFragment extends Fragment {
	
	private Activity parentActivity = null;
	
	private ImageButton startButton = null;
	private ImageButton aboutButton = null;
	private ImageButton exitButton = null;
	private ImageButton settingsButton = null;
	
	private FrameLayout logoFrame = null;
	
	
	private boolean ready = false;
	
	private String previousText = "";
			
	private void setupSettingsButton(View view){
		
		settingsButton = (ImageButton) view.findViewById(R.id.button_settings);
		
		final View myView = view;
		
		settingsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				FragmentUtils.loadMainMenuSettingsFragment(parentActivity);
			}
		});
	}
		
	private void setupStartButton(final View view){
		startButton = (ImageButton) view.findViewById(R.id.button_start_game);
		
		final View myView = view;
		
		final TextView loadingInfo = (TextView) myView.findViewById(R.id.loading_info);
		
		
		startButton.setOnClickListener(new OnClickListener() {
			
			
			@Override
			public void onClick(View v) {			
				
				LoadingFragment newFragment = FragmentUtils.setupLoadingToGameSelection();
				FragmentUtils.loadLoadingFragment(parentActivity, newFragment);
			    
			}
		});
	}
		
	private void setupAboutButton(View view){
		aboutButton = (ImageButton) view.findViewById(R.id.button_about);
		final View myView = view;
		
		aboutButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {			
				FragmentUtils.loadTutorialsFragment(FragmentUtils.currentActivity);
			}
		});
	}
	
	private void setupExitButton(View view){
		exitButton = (ImageButton) view.findViewById(R.id.button_exit);
		final View myView = view;
		
		exitButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				AdminUtils.exitApp();
			}
		});
	}
		
	private void setupButtons(View view){
		setupSettingsButton(view);
		setupAboutButton(view);
		setupExitButton(view);
		setupStartButton(view);
	}
	
	
	
	public void setParentActivity(Activity parentActivity){		
		this.parentActivity = parentActivity;
		
	}
		
	public MainMenuFragment(){
		super();
	}
	
	
	@Override
	public void onResume() {
		super.onResume();		
		GameFragmentMapUtils.setGpsOn();		
	}
	
	
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {    
    		    
	    View view = inflater.inflate(R.layout.fragment_menu, container, false);
	    	    
	    view.setBackgroundColor(Color.TRANSPARENT);
	    
	    Storage.loadPlayerIdData(FragmentUtils.currentActivity);
	    
	    if(!AdminUtils.areGooglePlayServicesAvailable(container.getContext())){
		    	    	
	    	//MapUtils.buildAlertMessageNoGooglePlayServices(container.getContext());
	    	
	    	AdminUtils.showDownloadDialog(FragmentUtils.currentActivity, "No map access", "You do not have Google Play Services installed. Please do this now...", "Install", "Exit");
	    	
	    	
		}	    
	    
	    FragmentUtils.setCurrentFragment(this);
	    FragmentUtils.setCurrentContext(view.getContext());
	    
	    parentActivity = this.getActivity();
	    
	    Storage.loadServerData(FragmentUtils.currentActivity);
	    
	    setupButtons(view);
	    
	    return view;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Log.println(Log.INFO, "context ?", String.valueOf(parentActivity));
        
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
    
    @Override
    public void onPause() {
    	super.onPause();
    	Storage.savePlayerIdData(FragmentUtils.currentActivity);
    }
    
	@Override
	public void onDetach() {
		super.onDetach();
		Storage.savePlayerIdData(FragmentUtils.currentActivity);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Storage.savePlayerIdData(FragmentUtils.currentActivity);
	}
    
    
    
}