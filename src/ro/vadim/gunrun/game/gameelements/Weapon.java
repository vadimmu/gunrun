package ro.vadim.gunrun.game.gameelements;

import java.util.Map;

import android.os.SystemClock;
import android.text.format.Time;

public class Weapon {
	
	public static final int POLICY_SELF = 0;
	
	public static final int POLICY_FRIENDS = 1;
	
	public static final int POLICY_FRIENDS_AND_SELF = 2;
	
	public static final int POLICY_ENEMIES = 3;	
	
	public static final int POLICY_FRIENDS_AND_ENEMIES = 4;
	
	public static final int POLICY_FRIENDS_AND_ENEMIES_AND_SELF = 5;
	
	
	
	private String name = "";
	private int range = 0;
	private int damage = 0;
	private int cooldown = 0; //in seconds 
	private int duration = 0; //in seconds
	private int policy = POLICY_ENEMIES;
	private String description = "";
	
	
	
	private Long lastUsed = (long) 0; 
	
	
	
	public Weapon(Weapon newWeapon){
		setName(newWeapon.getName());
		setRange(newWeapon.getRange());
		setDamage(newWeapon.getDamage());
		setCooldown(newWeapon.getCooldown());
		setDuration(newWeapon.getDuration());
		setPolicy(newWeapon.getPolicy());
		setDescription(newWeapon.getDescription());
		
	}
	
	public Weapon(String newName, int newRadius, int newDamage, int newCooldownTime, int newDuration, int newPlayerSelectionPolicy, String newDescription){
		setName(newName);
		setRange(newRadius);
		setDamage(newDamage);
		setCooldown(newCooldownTime);
		setDuration(newDuration);
		setPolicy(newPlayerSelectionPolicy);
		setDescription(newDescription);
	}

	public Long getLastUsed() {
		return lastUsed;
	}

	public void setLastUsed(Long lastUsed) {
		Time time = new Time();
		time.setToNow();				
		this.lastUsed = time.toMillis(false);
	}
	
	
	
	
	
	
	
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getCooldown() {
		return cooldown;
	}

	public void setCooldown(int cooldown) {
		this.cooldown = cooldown;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getPolicy() {
		return policy;
	}

	public void setPolicy(int policy) {
		this.policy = policy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
