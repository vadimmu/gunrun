package ro.vadim.gunrun.game.gameelements;

import java.util.ArrayList;

import ro.vadim.gunrun.game.storage_and_data.StaticData_Players;
import ro.vadim.gunrun.game.storage_and_data.StaticData_ProfessionsAndWeapons;
import ro.vadim.gunrun.game.utils.GameFragmentMapUtils;

import android.graphics.drawable.Drawable;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class Player {
		
	public static final String DEFAULT_NICKNAME = "Player";
	public static final String DEFAULT_PROFESSION_TITLE = "Marine";
	public static final int DEFAULT_HEALTH = 100;
		
	public boolean isReady = false;	
	public boolean isInvisible = false;
	public boolean isShielded = false;
	
	private String nickname = null;
	private String professionTitle = null;	
	private Integer health = null;	
	private ArrayList<Weapon> weapons = null;
	
	private LatLng position = null;
	
	private int selectedWeaponIndex = 0;
	
	public Player(String newPlayerNickname){		
		this.setNickname(newPlayerNickname);
	}
	
	public Player(String newPlayerNickname, String newProfessionTitle, double latitude, double longitude){
		
		if(newProfessionTitle == null){
			/*
			if(StaticData_ProfessionsAndWeapons.professions != null){
				if(StaticData_ProfessionsAndWeapons.professions.size() != 0){				
					
					this.professionTitle = StaticData_ProfessionsAndWeapons.professions.keySet().iterator().next(); 
					
				}
			}
			*/			
			this.professionTitle = "N/A";
		}
		else
			changeProfession(newProfessionTitle);
		
		this.setNickname(newPlayerNickname);
		this.setPosition(latitude, longitude);
	}
	
	
	public void changeProfession(String newProfessionTitle){
		if(StaticData_ProfessionsAndWeapons.professions.containsKey(newProfessionTitle)){
			
			Profession profession = StaticData_ProfessionsAndWeapons.professions.get(newProfessionTitle);
			
			this.professionTitle = newProfessionTitle;
			this.health = profession.health;
			this.weapons = new ArrayList<Weapon>(profession.weapons.size());
			for(String weaponName : profession.weapons){
				if(StaticData_ProfessionsAndWeapons.weapons.containsKey(weaponName))
					this.weapons.add(StaticData_ProfessionsAndWeapons.weapons.get(weaponName));
			}
		}
		
		else{
			this.professionTitle = "N/A";			
		}
		
	}
	
	
	public boolean isSelected(){
		
		if(StaticData_Players.getPlayerById(StaticData_Players.selectedPlayerUUID).equals(this)){
			return true;
		}
		
		return false;
	}
	
	public boolean isDead(){		
		return (health<=0);		
	}
	
	
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		if(nickname != null)
			this.nickname = nickname;
	}

	public String getProfessionTitle() {
		return professionTitle;
	}

	public void setProfessionTitle(String newProfessionTitle) {
		if(newProfessionTitle != null)
			this.professionTitle = newProfessionTitle;
	}

	public ArrayList<Weapon> getWeapons() {
		return weapons;
	}

	public void setWeapons(Weapon[] newWeapons) {
		if(newWeapons != null){
			
			this.weapons = new ArrayList<Weapon>(newWeapons.length);
			
			for(Weapon w : newWeapons) 
				this.weapons.add(w);
			
		}
	}
	
	public void setWeapons(ArrayList<Weapon> newWeapons) {
		if(newWeapons != null)		
			this.weapons = newWeapons;
	}

	public Integer getHealth() {
		return health;
	}

	public synchronized void setHealth(Integer newHealth) {
		if(newHealth != null)
			this.health = newHealth;
	}

	public synchronized void modifyHealth(int amount){
		this.health -= amount;
	}	
	
	public int getSelectedWeaponIndex() {
				
		return selectedWeaponIndex;
	}

	public void setSelectedWeaponIndex(int selectedWeapon) {
		this.selectedWeaponIndex = selectedWeapon;
	}
	
	public void setPosition(double latitude, double longitude){
		position = new LatLng(latitude, longitude);
	}
	
	public LatLng getPosition(){
		return new LatLng(position.latitude, position.longitude);
	}
	
	
	
	
	@Override
	public String toString() {
		return "Name: "+getNickname()+" Profession : "+getProfessionTitle()+" Health : "+getHealth()+" Position : "+getPosition().toString();
	}
}
