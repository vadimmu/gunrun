package ro.vadim.gunrun.game.utils;

import java.io.IOException;


import ro.vadim.gunrun.communication.CommunicationUtils;
import ro.vadim.gunrun.communication.TcpGameClient;
import ro.vadim.gunrun.communication.TcpGameManagementClient;
import ro.vadim.gunrun.communication.receiver.MessageReceiver;
import ro.vadim.gunrun.game.fragments.CrashReportFragment;
import ro.vadim.gunrun.game.fragments.GameFragment;
import ro.vadim.gunrun.game.fragments.GameSelectionFragment;
import ro.vadim.gunrun.game.fragments.LoadingFragment;
import ro.vadim.gunrun.game.fragments.LobbyFragment;
import ro.vadim.gunrun.game.fragments.LobbySettingsFragment;
import ro.vadim.gunrun.game.fragments.MainMenuFragment;
import ro.vadim.gunrun.game.fragments.MainMenuSettingsFragment;
import ro.vadim.gunrun.game.fragments.TutorialsFragment;
import ro.vadim.gunrun.game.fragments.LoadingFragment.LoadingWorker;
import ro.vadim.gunrun.game.storage_and_data.StaticData_Players;
import ro.vadim.gunrun.R;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class FragmentUtils {

	public static boolean isGameFragmentOn = false;	
		
	public static Activity currentActivity = null;
	
	public static Context currentContext = null;
	
	public static Fragment currentFragment = null;
	
	public static Toast toast = null; //initialized in setContext
	
	
	
	public static boolean isRunningOnUiThread(){
		return (Looper.getMainLooper().getThread() == Thread.currentThread());
	}
	
	
	
	public static void loadLobbyFragment(Activity parentActivity){
		
		LobbyFragment newFragment = new LobbyFragment();
		newFragment.setParentActivity(parentActivity);				
		
		FragmentTransaction transaction = currentFragment.getFragmentManager().beginTransaction();
		transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
	    transaction.replace(R.id.fragment_container, newFragment);
	    transaction.addToBackStack(null);
	    transaction.commit();		
	}
	
	public static void loadLobbySettingsFragment(Activity parentActivity){
		LobbySettingsFragment newFragment = new LobbySettingsFragment();
		newFragment.setParentActivity(parentActivity);
						
		FragmentTransaction transaction = currentFragment.getFragmentManager().beginTransaction();
		transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
	    transaction.replace(R.id.fragment_container, newFragment);
	    transaction.addToBackStack(null);
	    transaction.commit();
	}
	
	public static void loadMainMenuFragment(Activity parentActivity){
		MainMenuFragment newFragment = new MainMenuFragment();
		newFragment.setParentActivity(parentActivity);				
		
		FragmentTransaction transaction = currentFragment.getFragmentManager().beginTransaction();
		transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
	    transaction.replace(R.id.fragment_container, newFragment);
	    transaction.addToBackStack(null);
	    transaction.commit();
	}
	
	public static void loadMainMenuSettingsFragment(Activity parentActivity){
		
		MainMenuSettingsFragment newFragment = new MainMenuSettingsFragment();
		newFragment.setParentActivity(parentActivity);				
		
		FragmentTransaction transaction = currentFragment.getFragmentManager().beginTransaction();
		transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
	    transaction.replace(R.id.fragment_container, newFragment);
	    transaction.addToBackStack(null);
	    transaction.commit();
	}
	
	public static void loadCrashReportFragment(Activity parentActivity){
		
		CrashReportFragment newFragment = new CrashReportFragment();		
		FragmentTransaction transaction = currentFragment.getFragmentManager().beginTransaction();
		transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
	    transaction.replace(R.id.fragment_container, newFragment);
	    transaction.addToBackStack(null);
	    transaction.commit();
	}
	
	public static void loadLoadingFragment(Activity parentActivity, LoadingFragment newFragment){
						
		FragmentTransaction transaction = currentFragment.getFragmentManager().beginTransaction();
		transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
	    transaction.replace(R.id.fragment_container, newFragment);
	    transaction.addToBackStack(null);
	    transaction.commit();
	}
	
	public static void loadTutorialsFragment(Activity parentActivity){
		TutorialsFragment newFragment = new TutorialsFragment();
		
		FragmentTransaction transaction = currentFragment.getFragmentManager().beginTransaction();
		transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
	    transaction.replace(R.id.fragment_container, newFragment);
	    transaction.addToBackStack(null);
	    transaction.commit();
	}
	
	public static void loadGameSelectionFragment(){
		GameSelectionFragment newFragment = new GameSelectionFragment();
		
		FragmentTransaction transaction = currentFragment.getFragmentManager().beginTransaction();
		transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
	    transaction.replace(R.id.fragment_container, newFragment);
	    transaction.addToBackStack(null);
	    transaction.commit();
	}
	
	
	
	
	public static void loadGameFragment(){
		startGame();
	}
	
	public static void removeGameFragment(Activity parentActivity){
		
		if(FragmentUtils.currentContext.getClass().equals(GameFragment.class)){
			Log.println(Log.INFO, "REMOVING GAME FRAGMENT", "...");
						
			FragmentUtils.isGameFragmentOn = false;
			
			loadMainMenuFragment(currentActivity);
		}
	}
	
	
	
	
	
	public static void setCurrentContext(Context context){
		
		FragmentUtils.currentContext = context;
	}
	
	public static void setCurrentFragment(Fragment currentFragment){
		
		FragmentUtils.currentFragment = currentFragment;				
	}	
	
	public static Fragment getCurrentFragment(){
		return currentFragment;
	}
	
	public static Context getCurrentContext(){
		return currentContext;
	}
	
	
	
	
	
	public static void showText_unsafe(final Context context, final String text){
		if(toast != null)	
			toast.cancel();
	
		toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
		toast.show();
	}
	public static void showText(final Context context, final String text){
		
		if(!FragmentUtils.isRunningOnUiThread()){
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					showText_unsafe(context, text);
				}
			});
		}
		
		else{
			showText_unsafe(context, text);
		}
	}
	
	public static LoadingFragment setupLoadingToLobby(final Activity parentActivity, final String ipAddress, final int port){
		
		LoadingFragment newFragment = new LoadingFragment();
		newFragment.setParentActivity(parentActivity);
		newFragment.whatsLoading = LoadingFragment.LOADING_GAME_CLIENT;
		newFragment.setupLoadingWorker(new LoadingWorker() {
								
			
			boolean workDone = false;
			
			@Override
			public void doWork() {					
				if(!GameFragmentMapUtils.isGpsOn()){
					FragmentUtils.showText(FragmentUtils.getCurrentContext(), "The GPS is off. Please turn it on !");
				}
				
				else{
				
					try {
						
						TcpGameClient.startServer(ipAddress, port);
						
						Log.println(Log.INFO, "STARTED SERVER", "started server");
						
						if(TcpGameClient.clientSocket != null)
							workDone = true;
						
						else 
							workDone = false;
						
											
						
						while(MessageReceiver.configReceived == false){
							Thread.sleep(100);								
						}
					} 
					
					catch (IOException e) {						
						Log.println(Log.ERROR, "CONNECTING TO SERVER", e.toString());
						workDone = false;
					} 
					
					catch (InterruptedException e) {
						Log.println(Log.ERROR, "LOADING FRAGMENT ERROR", e.toString());
						workDone = false;						
					}
				}
				
			}
			
			@Override
			public void doAfterWork() {
				
				if(workDone == true){
					LobbyFragment newFragment = new LobbyFragment();
					newFragment.setParentActivity(parentActivity);
					
					FragmentTransaction transaction = FragmentUtils.getCurrentFragment().getFragmentManager().beginTransaction();
					transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
				    transaction.replace(R.id.fragment_container, newFragment);
				    transaction.addToBackStack(null);
				    transaction.commit();
				}
				
				else{
					
					MainMenuFragment newFragment = new MainMenuFragment();
					newFragment.setParentActivity(parentActivity);
					
					FragmentTransaction transaction = FragmentUtils.getCurrentFragment().getFragmentManager().beginTransaction();
					transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
				    transaction.replace(R.id.fragment_container, newFragment);
				    transaction.addToBackStack(null);
				    transaction.commit();
					
				    
				}
				
			}
		});
		
		return newFragment;
		
	}
	
	public static LoadingFragment setupLoadingToGameSelection(){
		LoadingFragment newFragment = new LoadingFragment();
		newFragment.whatsLoading = LoadingFragment.LOADING_GAME_MANAGEMENT_CLIENT;		
		newFragment.setParentActivity(currentActivity);
		newFragment.setupLoadingWorker(new LoadingWorker() {
			
			
			boolean workDone = false;
			
			@Override
			public void doWork() {
				try {					
					
					TcpGameManagementClient.startServer(CommunicationUtils.defaultServerAddress, CommunicationUtils.defaultServerPort);
					
					Log.println(Log.INFO, "TcpGameManagementClient", "CONNECTING TO GAME MANAGEMENT SERVER");
					
					if(TcpGameClient.clientSocket != null)
						workDone = true;
					
					else 
						workDone = false;										
					
					while(MessageReceiver.helloReceived == false){
						Thread.sleep(100);						
					}
					workDone = true;
				} 
				
				catch (IOException e) {						
					Log.println(Log.ERROR, "CONNECTING TO SERVER", e.toString());
					workDone = false;
				} 
				
				catch (InterruptedException e) {
					Log.println(Log.ERROR, "LOADING FRAGMENT ERROR", e.toString());
					workDone = false;						
				}
				
			}
			
			@Override
			public void doAfterWork() {
				if(workDone == true){
					
					Log.i("setupLoadingToGameSelection", "workDone = true. Going to Game Selection");
					loadGameSelectionFragment();
				}
				
				else{
					Log.i("setupLoadingToGameSelection", "workDone = false. Going to Main Menu");
					loadMainMenuFragment(currentActivity);
				}
				
			}
		});
		
		
		return newFragment;
	}
	
	
	
	public static void updateCurrentFragmentUI(){
		View fragmentView = FragmentUtils.getCurrentFragment().getView();		
		
		FragmentTransaction transaction = ((LobbyFragment)FragmentUtils.getCurrentFragment()).getFragmentManager().beginTransaction(); 
				
		transaction.hide(FragmentUtils.getCurrentFragment());
		transaction.show(FragmentUtils.getCurrentFragment());
		transaction.commit();
	}
	
	
	public static void updateLobby(){
		
		Log.println(Log.INFO, "updateLobby CALLED", "...");
					
		FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					Log.println(Log.INFO, "updateLobby on UI thread", "...");
					
					
					if(FragmentUtils.currentFragment.getClass().equals(LobbyFragment.class))					
						((LobbyFragment)FragmentUtils.currentFragment).setupLists(((LobbyFragment)FragmentUtils.currentFragment).getView());
				}
		});
	}
		
	
	public static void startGame(){
		Log.println(Log.INFO, "UPDATING GameFragment", "...");
		
		if(FragmentUtils.getCurrentFragment().getClass().equals(LobbyFragment.class)){
			Log.println(Log.INFO, "Identified the LobbyFragment", "ok");
			
			GameFragment newFragment = new GameFragment();
			newFragment.setParentActivity(((LobbyFragment)FragmentUtils.getCurrentFragment()).getActivity());
			FragmentTransaction transaction = ((LobbyFragment)FragmentUtils.getCurrentFragment()).getFragmentManager().beginTransaction();
						
			//transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
		    transaction.replace(R.id.fragment_container, newFragment);
		    transaction.addToBackStack(null);
		    transaction.commit();
			
		}
	}		
	
	
	public static void lobbyFragment_setReadyButtonEnabled(boolean enabled){
		
		if(FragmentUtils.getCurrentFragment().getClass().equals(LobbyFragment.class))		
			((LobbyFragment)FragmentUtils.getCurrentFragment()).setReadyButtonEnabled(enabled);		
	}
	
	public static synchronized void updateGameFragment(){
		Log.println(Log.INFO, "UPDATING GameFragment", "...");
		
		String currentFragmentClass = FragmentUtils.getCurrentFragment().getClass().toString();
		Log.println(Log.INFO, "current fragment : ", currentFragmentClass);
		Log.println(Log.INFO, "GameFragment class : ", GameFragment.class.toString());
		
		
		if(FragmentUtils.getCurrentFragment().getClass().equals(GameFragment.class)){
			Log.println(Log.INFO, "Identified the GameFragment", "ok");
			
			View fragmentView = ((GameFragment)FragmentUtils.getCurrentFragment()).getView();
					
			FragmentTransaction transaction = ((GameFragment)FragmentUtils.getCurrentFragment()).getFragmentManager().beginTransaction(); 
			
			transaction.hide(FragmentUtils.getCurrentFragment());
			transaction.show(FragmentUtils.getCurrentFragment());
			transaction.commit();
		}
		
		//MapUtils.mapView.getController().animateTo(StaticData_Players.currentPlayer.getPosition());
		
	}
	
	
	
	
	
	
	
	
	
}
