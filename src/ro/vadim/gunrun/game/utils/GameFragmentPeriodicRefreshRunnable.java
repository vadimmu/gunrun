package ro.vadim.gunrun.game.utils;

import ro.vadim.gunrun.game.fragments.GameFragment;
import ro.vadim.gunrun.game.storage_and_data.StaticData_Players;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public class GameFragmentPeriodicRefreshRunnable implements Runnable{
		
	@Override
	public void run() {		
		
		while(FragmentUtils.currentFragment.getClass().equals(GameFragment.class)){
			
			try {
				Thread.sleep(500);
				GameFragmentDrawingUtils.redrawAll();
			}
			
			catch (InterruptedException e) {
				Log.println(Log.ERROR, "RefreshRunnable ERROR ! ", e.toString());
			}
		}	
	}      	  
}
