package ro.vadim.gunrun.game.utils;

import ro.vadim.gunrun.R;
import ro.vadim.gunrun.game.gameelements.Player;
import ro.vadim.gunrun.game.gameelements.Weapon;
import ro.vadim.gunrun.game.misc.Game;

public class GameElementUtils {

	
    public static int getWeaponIcon(Weapon weapon){
    	if(weapon.getName().equals("Pistol"))
    		return R.drawable.button_weapon_pistol;
    	
    	if(weapon.getName().equals("Rifle"))
    		return R.drawable.button_weapon_rifle;
    		
    	if(weapon.getName().equals("Sniper Rifle"))
    		return R.drawable.button_weapon_sniper_rifle;
    		
    	if(weapon.getName().equals("Painkillers"))
    		return R.drawable.button_weapon_painkillers;
    		
    	if(weapon.getName().equals("Shield"))
    		return R.drawable.button_weapon_shield;
    		
    	if(weapon.getName().equals("Invisibility Cloak"))
    		return R.drawable.button_weapon_invisibility_cloak;
    		
    	if(weapon.getName().equals("Knife"))
    		return R.drawable.button_weapon_knife;    	 		
    	    	
    	return R.drawable.button_weapon_empty_weapon;
    }
    
    public static int getWeaponLargeIcon(Weapon weapon){
    	if(weapon.getName().equals("Pistol"))
    		return R.drawable.button_weapon_pistol_large;
    	
    	if(weapon.getName().equals("Rifle"))
    		return R.drawable.button_weapon_rifle_large;
    		
    	if(weapon.getName().equals("Sniper Rifle"))
    		return R.drawable.button_weapon_sniper_rifle_large;
    		
    	if(weapon.getName().equals("Painkillers"))
    		return R.drawable.button_weapon_painkillers_large;
    		
    	if(weapon.getName().equals("Shield"))
    		return R.drawable.button_weapon_shield_large;
    		
    	if(weapon.getName().equals("Invisibility Cloak"))
    		return R.drawable.button_weapon_invisibility_cloak_large;
    		
    	if(weapon.getName().equals("Knife"))
    		return R.drawable.button_weapon_knife_large;
    	    	
    	return R.drawable.button_weapon_empty_weapon_large;
    }
    
    
    public static int getWeaponMediumIcon(Weapon weapon){
    	if(weapon.getName().equals("Pistol"))
    		return R.drawable.button_weapon_pistol_medium;
    	
    	if(weapon.getName().equals("Rifle"))
    		return R.drawable.button_weapon_rifle_medium;
    		
    	if(weapon.getName().equals("Sniper Rifle"))
    		return R.drawable.button_weapon_sniper_rifle_medium;
    		
    	if(weapon.getName().equals("Painkillers"))
    		return R.drawable.button_weapon_painkillers_medium;
    		
    	if(weapon.getName().equals("Shield"))
    		return R.drawable.button_weapon_shield_medium;
    		
    	if(weapon.getName().equals("Invisibility Cloak"))
    		return R.drawable.button_weapon_invisibility_cloak_medium;
    		
    	if(weapon.getName().equals("Knife"))
    		return R.drawable.button_weapon_knife_medium;
    	    	
    	return R.drawable.button_weapon_empty_weapon_medium;
    }
    
    
    public static int getWeaponIconAccordingToPolicy(Weapon weapon, int buttonSizePolicy){
    	
    	if(buttonSizePolicy == GameFragmentButtonsUtils.BUTTON_SIZE_SMALL){
    		return getWeaponIcon(weapon);
    	}
    	
    	if(buttonSizePolicy == GameFragmentButtonsUtils.BUTTON_SIZE_MEDIUM){
    		return getWeaponMediumIcon(weapon);
    	}
    	
    	if(buttonSizePolicy == GameFragmentButtonsUtils.BUTTON_SIZE_LARGE){
    		return getWeaponLargeIcon(weapon);	
    	}
    	
    	return getWeaponIcon(weapon);
    }
    
    
    
	public static boolean isWeaponAPowerup(Weapon weapon){
		
	    
    	if(weapon.getName().equals("Pistol"))
    		return false;
    	
    	if(weapon.getName().equals("Rifle"))
    		return false;
    		
    	if(weapon.getName().equals("Sniper Rifle"))
    		return false;
    		
    	if(weapon.getName().equals("Painkillers"))
    		return true;
    		
    	if(weapon.getName().equals("Shield"))
    		return true;
    		
    	if(weapon.getName().equals("Invisibility Cloak"))
    		return true;
    		
    	if(weapon.getName().equals("Knife"))
    		return false;    	 		
    	    	
    	return false;
	}
	
	
	
    public static int getProfessionIcon(String professionName){
    	
    	if(professionName.equals("Marine"))
    		return R.drawable.icon_profession_marine;
    	
    	if(professionName.equals("Medic"))
    		return R.drawable.icon_profession_medic;
    	
    	if(professionName.equals("Scout"))
    		return R.drawable.icon_profession_scout;
    	
    	if(professionName.equals("Sniper"))
    		return R.drawable.icon_profession_sniper;
    	
    	if(professionName.equals("All Rounder"))
    		return R.drawable.icon_profession_all_rounder;
    	
    	if(professionName.equals("Duelist"))
    		return R.drawable.icon_profession_duelist;
    	
    	if(professionName.equals("Goliath"))
    		return R.drawable.icon_profession_goliath;
    	    	
    	return R.drawable.icon_profession_marine;
    }
    
    public static int getPlayerIcon(Player player){
    	
    	String professionName = player.getProfessionTitle();
    	return getProfessionIcon(professionName);
    }
    
    
    public static int getGameIcon(Game game){
    	
    	if(game.getGameType().intValue() == Game.GAME_TYPE_NORMAL)
			return R.drawable.icon_gametype_regular_game2;
		
		else if(game.getGameType().intValue() == Game.GAME_TYPE_DUEL)
			return R.drawable.icon_gametype_duel2;
		
		else if(game.getGameType().intValue() == Game.GAME_TYPE_DAVID_VS_GOLIATH)
			return R.drawable.icon_gametype_david_vs_goliath2;		
		
    	return R.drawable.icon_gametype_regular_game2;
    }
    
    
    
    public static int getWeaponDisabledIcon(Weapon weapon){
    	if(weapon.getName().equals("Pistol"))
    		return R.drawable.button_weapon_empty_weapon;
    	
    	if(weapon.getName().equals("Rifle"))
    		return R.drawable.button_weapon_empty_weapon;
    		
    	if(weapon.getName().equals("Sniper Rifle"))
    		return R.drawable.button_weapon_empty_weapon;
    		
    	if(weapon.getName().equals("Painkillers"))
    		return R.drawable.button_weapon_empty_powerup;
    		
    	if(weapon.getName().equals("Shield"))
    		return R.drawable.button_weapon_empty_powerup;
    		
    	if(weapon.getName().equals("Invisibility Cloak"))
    		return R.drawable.button_weapon_empty_powerup;
    		
    	if(weapon.getName().equals("Knife"))
    		return R.drawable.button_weapon_empty_weapon;		
    	    
    	return R.drawable.button_weapon_empty_weapon;
    }
    
    
    
	
	
}
