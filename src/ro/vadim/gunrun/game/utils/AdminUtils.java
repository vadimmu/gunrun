package ro.vadim.gunrun.game.utils;

import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;

import ro.vadim.gunrun.communication.TcpGameClient;
import ro.vadim.gunrun.communication.TcpGameManagementClient;
import ro.vadim.gunrun.game.fragments.GameFragment;
import ro.vadim.gunrun.game.misc.Game;
import ro.vadim.gunrun.game.misc.Games;
import ro.vadim.gunrun.game.storage_and_data.StaticData_Players;
import ro.vadim.gunrun.game.storage_and_data.Storage;
import ro.vadim.gunrun.logging.Logger;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;

public class AdminUtils {

	private static WakeLock wakeLock = null; //keeps the cpu running
    private static WifiLock wifiLock = null; //keeps the wifi running
	public static Logger errorLogger = new Logger();	
	
	
	
	private static void startLoggingThenExit(final boolean exit){
		
		String logString = errorLogger.getLogString();
		Storage.saveCrashReport(FragmentUtils.currentActivity, logString);
		
		if(logString == null)
			Log.i("startLoggingThenExt", "Checking logger string : IT'S NULL !");
		
		else{
			Log.i("startLoggingThenExt", "Checking logger string : OK");
			Log.i("startLoggingThenExt", "Checking logger string : "+logString);
		}		
		
		if(exit == true)
			AdminUtils.exitApp();
				
	}	
	
	public static void startLoggingAndExit(){
		startLoggingThenExit(true);		
	}
	
	public static void startLogging(){
		startLoggingThenExit(false);
	}
	
	public static void setDefaultUncaughtExceptionHandler(){
		
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			
			@Override
			public void uncaughtException(Thread thread, Throwable ex) {
				
				Log.e("UNCAUGHT_EXCEPTION", ex.toString(), ex);
				startLoggingAndExit();
				
			}
		});
	}
    
    
    
    
	
    public static void setWifiLock(Context context){
    	
    	WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL , "MyWifiLock");
        if(!wifiLock.isHeld()){ wifiLock.acquire(); }
    }
    
    public static void removeWifiLock(){
    	if(wifiLock != null){
    		if(wifiLock.isHeld()){
    			wifiLock.release();
    		}
    	}
    }
	
    public static void setScreenLock(Activity parentActivity){
    	parentActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);    	
    }
    
    public static void setWakeLock(Context context){
    	PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
    	wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock");
        if(!wakeLock.isHeld()){ wakeLock.acquire(); }
    }
    
    public static void removeWakeLock(){
    	if(wakeLock != null){
    		if(wakeLock.isHeld()){
    			wakeLock.release();
    		}
    	}
    }
    
    
    
    
    
    public static void exitApp(){
    	
    	startLogging();
    	
    	android.os.Process.killProcess(android.os.Process.myPid());
		AdminUtils.removeWifiLock();
		
		Log.e("Extracting log and data usage before exit", "...");    	
    	
		
    }
        
    public static void trimCache(Context context) {
        try {
           File dir = context.getCacheDir();
           if (dir != null && dir.isDirectory()) {
              deleteDir(dir);
           }
        } catch (Exception e) {
           // TODO: handle exception
        }
     }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
           String[] children = dir.list();
           for (int i = 0; i < children.length; i++) {
              boolean success = deleteDir(new File(dir, children[i]));
              if (!success) {
                 return false;
              }
           }
        }

        // The directory is now empty so delete it
        return dir.delete();
     }
    
 	public static boolean areGooglePlayServicesAvailable(Context context){
		
    	int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
    	if(status == ConnectionResult.SUCCESS) {
    	    return true;
    	}
    	return false;
	}
         
 	public static AlertDialog showDownloadDialog(final Activity activity,
			CharSequence stringTitle, CharSequence stringMessage,
			CharSequence stringButtonYes, CharSequence stringButtonNo) {
		AlertDialog.Builder downloadDialog = new AlertDialog.Builder(activity);
		downloadDialog.setCancelable(false);
		downloadDialog.setTitle(stringTitle);
		downloadDialog.setMessage(stringMessage);
		downloadDialog.setPositiveButton(stringButtonYes,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialogInterface, int i) {
						Uri uri = Uri
								.parse("https://play.google.com/store/apps/details?id=com.google.android.gms");
						Intent intent = new Intent(Intent.ACTION_VIEW, uri);
						activity.startActivity(intent);
					}
				});
		downloadDialog.setNegativeButton(stringButtonNo,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialogInterface, int i) {
						AdminUtils.exitApp();						
					}
				});
		return downloadDialog.show();
	}
 	


 	
 	
	public static void buildAlertMessage_unsafe(final Context context, final String message){
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    builder.setMessage(message)
	    		.setCancelable(false)
	    		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	               public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
	            	   dialog.cancel();
	               }
	    		});
	    final AlertDialog alert = builder.create();
	    alert.show();		
	}
	
	public static void buildAlertMessage(final Context context, final String message){
		
		if(!FragmentUtils.isRunningOnUiThread()){
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					buildAlertMessage_unsafe(context, message);
				}
			});
		}
		
		else{
			buildAlertMessage_unsafe(context, message);
		}	
		
	}
	
	public static void buildConnectionLostAlertMessage(){
		buildAlertMessage(FragmentUtils.currentActivity, "The connection to the server is down. Please reconnect.");				
	}
	
	
	
	
	
	public static void buildGameOverAlertMessage_unsafe(final Context context, final Activity parentActivity){
		
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    builder.setMessage("GAME OVER !")
	           .setCancelable(false)
	           .setNegativeButton("SPECTATE", new DialogInterface.OnClickListener() {
				
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						StaticData_Players.currentPlayer.setHealth(1);					
						dialog.cancel();
					}
				})
	           
	           .setPositiveButton("EXIT", new DialogInterface.OnClickListener() {
	               public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
	            	   
	            	   Thread newThread = new Thread(new Runnable() {
						
	            		   @Override
	            		   public void run() {
	            			   TcpGameClient.stopServer();
	            			   StaticData_Players.enemies.clear();
	            			   StaticData_Players.friends.clear();
							
	            		   }
	            	   });
	            	   
	            	   newThread.start();
	            	   
	            	   FragmentUtils.isGameFragmentOn = false;
	            	   FragmentUtils.currentContext = parentActivity;	            	   
	            	   FragmentUtils.loadMainMenuFragment(parentActivity);
	               }
	           });
	           
	           
	    final AlertDialog alert = builder.create();
	    alert.show();
		
	}
	
	public static void buildGameOverAlertMessage(final Context context, final Activity parentActivity){
		if(!FragmentUtils.isRunningOnUiThread()){
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					buildGameOverAlertMessage_unsafe(context, parentActivity);
				}
			});
		}
		
		else{
			buildGameOverAlertMessage_unsafe(context, parentActivity);
		}
	}
	
	public static void buildExitGameAlertMessage_unsafe(final Context context, final Activity parentActivity){
		
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    builder.setMessage("QUIT THE GAME ?")
	           .setCancelable(false)
	           .setPositiveButton("QUIT", new DialogInterface.OnClickListener() {
	               public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
	            	   TcpGameClient.stopServer();
	            	   FragmentUtils.loadMainMenuFragment(parentActivity);
	               }
	           })
	           .setNegativeButton("KEEP PLAYING", new DialogInterface.OnClickListener() {
	               public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
	            	   
	            	   
	            	   
	            	   if(FragmentUtils.isGameFragmentOn){
	            		   
	            		   GameFragment gameFragment = (GameFragment)FragmentUtils.currentFragment;	            		   
	            		   gameFragment.googleMap.setMyLocationEnabled(false);
	            		   
	            		   GameFragmentUpdaterUtils.disableWeapons();
	            	   }
	            	   
	            	   dialog.cancel();	                   
	               }
	           });
	           
	    final AlertDialog alert = builder.create();
	    alert.show();
		
	}
	
	public static void buildExitGameAlertMessage(final Context context, final Activity parentActivity){
		if(!FragmentUtils.isRunningOnUiThread()){
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					buildExitGameAlertMessage_unsafe(context, parentActivity);
				}
			});
		}
		
		else{
			buildExitGameAlertMessage_unsafe(context, parentActivity);
		}
		
		
	}
	
	
	
	
	
	public static void buildAlertMessageNoGps_unsafe(final Context context) {
	    final AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    builder.setMessage("Your GPS seems to be disabled.")
	           .setCancelable(false)
	           .setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
	               public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
	                   context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));	                   
	               }
	           });
	           
	    final AlertDialog alert = builder.create();
	    alert.show();
	}
	
	public static void buildAlertMessageNoGps(final Context context) {
		if(!FragmentUtils.isRunningOnUiThread()){
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					buildAlertMessageNoGps_unsafe(context);
				}
			});
		}
		
		else{
			buildAlertMessageNoGps_unsafe(context);
		}
	}
		
	public static void buildAlertMessageNoGooglePlayServices_unsafe(final Context context){
	    
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    builder.setMessage("Google Play Services were not found on this device. Please install it from the App Market / Google Play")
	           .setCancelable(false)
	           .setPositiveButton("Exit Game", new DialogInterface.OnClickListener() {
	               public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
	            	   AdminUtils.exitApp();
	               }
	           });
	           
	    final AlertDialog alert = builder.create();
	    alert.show();
		
	}
	
	public static void buildAlertMessageNoGooglePlayServices(final Context context){
		if(!FragmentUtils.isRunningOnUiThread()){
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					buildAlertMessageNoGooglePlayServices_unsafe(context);
				}
			});
		}
		
		else{
			buildAlertMessageNoGooglePlayServices_unsafe(context);
		}
		
		
	}
	
	

	
	
	
	
	public static void joinGame(Game game){
		
		if(game.getIp().equals("0.0.0.0")){
			
			Log.e("GameSelectionFragment", "IP is 0.0.0.0. Can't use it !");
			return;
		}
		
		if(game.getInProgress().booleanValue() == true){
			AdminUtils.buildAlertMessage(FragmentUtils.currentActivity, "Unfortunately, this game is already in progress. You cannot join it.");
			return;
		}
								
		TcpGameManagementClient.stopServer();
		
		Log.i("GameSelectionFragment", "CONNECTING TO GAME ! GameType: "+String.valueOf(game.getGameType()));
		
		Games.setCurrentGame(game);
		
		FragmentUtils.loadLoadingFragment(FragmentUtils.currentActivity, FragmentUtils.setupLoadingToLobby(FragmentUtils.currentActivity, game.getIp(), game.getPort()));		
	}
 	
 	
 	
}
