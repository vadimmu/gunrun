package ro.vadim.gunrun.game.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.location.Location;
import android.opengl.Visibility;
import android.os.Build;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.Space;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.RelativeLayout.LayoutParams;


import ro.vadim.gunrun.communication.sender.MessageSender;
import ro.vadim.gunrun.game.fragments.GameFragment;
import ro.vadim.gunrun.game.gameelements.Player;
import ro.vadim.gunrun.game.gameelements.Weapon;
import ro.vadim.gunrun.game.misc.AbilityCountdown;
import ro.vadim.gunrun.game.misc.DurationReportingTask;
import ro.vadim.gunrun.game.misc.WeaponReportingTask;
import ro.vadim.gunrun.game.misc.AbilityCountdown.CountdownAction;
import ro.vadim.gunrun.game.storage_and_data.ButtonAdapter;
import ro.vadim.gunrun.game.storage_and_data.StaticData_Players;
import ro.vadim.gunrun.game.storage_and_data.StaticData_ProfessionsAndWeapons;
import ro.vadim.gunrun.game.storage_and_data.WeaponButtonAdapter;
import ro.vadim.gunrun.R;

public class GameFragmentButtonsUtils {
	
	
	private static boolean selectFriends = true;	
	public static ArrayList<Button> buttons = null;	
	public static int EMS_DEFAULT = 4;
	public static int EMS_ALTERNATIVE = 5;	
	
	public static int BUTTON_SIZE_SMALL = 0;
	public static int BUTTON_SIZE_MEDIUM = 1;
	public static int BUTTON_SIZE_LARGE = 2;
	
	public static int weaponButtonSizePolicy = BUTTON_SIZE_SMALL;
	
	public static void setupPowerupsLayout(RelativeLayout relativeLayout){
		
		Log.println(Log.INFO, "Setting up powerups layout", "...");
		
		GameFragment.powerupsLayout = new LinearLayout(relativeLayout.getContext());
		GameFragment.powerupsLayout.setOrientation(LinearLayout.HORIZONTAL);
		GameFragment.powerupsLayout.setId(GameFragmentDrawingUtils.generateViewId());
		
		GameFragment.powerupsLayout.setBackgroundColor(Color.WHITE);
		GameFragment.powerupsLayout.setMinimumWidth(GameFragment.myHealthTextView.getWidth());
		GameFragment.powerupsLayout.setMinimumHeight(GameFragment.myHealthTextView.getHeight());
		
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		
		GameFragment.powerupsLayout.setLayoutParams(layoutParams);
		relativeLayout.addView(GameFragment.powerupsLayout);
		
	}
	
	public static void addPowerupCounter(final Weapon weapon){
		Log.println(Log.INFO, "Preparing powerup counter", "...");
				
		FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				if(GameFragment.powerupsLayout == null)
					return;
				
				Log.println(Log.INFO, "Adding powerup counter", "Weapon : " + weapon.getName());
				
				TextView textView = new TextView(GameFragment.powerupsLayout.getContext());
				textView.setTextColor(Color.BLACK);
				textView.setBackgroundColor(Color.WHITE);
				textView.setText(weapon.getName());
				
				LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
				
				layoutParams.gravity = Gravity.CENTER_VERTICAL;
				textView.setLayoutParams(layoutParams);
							
				startWeaponDuration(textView, weapon);				
			}
		});
	}
		
	public static void addTargetButtons(RelativeLayout relativeLayout){
		
		GameFragment.toggleButtonsLayout = new LinearLayout(relativeLayout.getContext());
		GameFragment.toggleButtonsLayout.setOrientation(LinearLayout.VERTICAL);
		GameFragment.toggleButtonsLayout.setId(GameFragmentDrawingUtils.generateViewId());
		
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		GameFragment.toggleButtonsLayout.setLayoutParams(layoutParams);
		
		GameFragment.previousTargetButton = new ImageButton(relativeLayout.getContext());		
		GameFragment.previousTargetButton.setId(GameFragmentDrawingUtils.generateViewId());		
		GameFragment.previousTargetButton.setBackgroundResource(R.drawable.button_previous_friend);
		layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);		
		GameFragment.previousTargetButton.setLayoutParams(layoutParams);
		
		
		GameFragment.friendsToggleButton = new ToggleButton(relativeLayout.getContext());	
		GameFragment.friendsToggleButton.setId(GameFragmentDrawingUtils.generateViewId());
		GameFragment.friendsToggleButton.setTextOn("Sel.\nFriend");
		GameFragment.friendsToggleButton.setTextOff("Sel.\nEnemy");
		GameFragment.friendsToggleButton.setBackgroundResource(R.drawable.button_empty_green);					
		GameFragment.friendsToggleButton.setText("Friends");
		GameFragment.friendsToggleButton.setChecked(true);		
		GameFragment.friendsToggleButton.setGravity(Gravity.CENTER);
		layoutParams = new LayoutParams(getWeaponButtonWidthAccordingToPolicy(BUTTON_SIZE_MEDIUM), getWeaponButtonWidthAccordingToPolicy(BUTTON_SIZE_MEDIUM));		
		GameFragment.friendsToggleButton.setLayoutParams(layoutParams);
		
		
		GameFragment.nextTargetButton = new ImageButton(relativeLayout.getContext());		
		GameFragment.nextTargetButton.setId(GameFragmentDrawingUtils.generateViewId());		
		GameFragment.nextTargetButton.setBackgroundResource(R.drawable.button_next_friend);
		layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);		
		GameFragment.nextTargetButton.setLayoutParams(layoutParams);
				
		GameFragment.selectMeButton = new Button(relativeLayout.getContext());
		GameFragment.selectMeButton.setText("Sel.\nMyself");
		GameFragment.selectMeButton.setId(GameFragmentDrawingUtils.generateViewId());
		GameFragment.selectMeButton.setEms(EMS_DEFAULT);
		GameFragment.selectMeButton.setLines(2);
		GameFragment.selectMeButton.setBackgroundResource(R.drawable.button_select_myself);
		
		layoutParams = new LayoutParams(getWeaponButtonWidthAccordingToPolicy(BUTTON_SIZE_MEDIUM), getWeaponButtonWidthAccordingToPolicy(BUTTON_SIZE_MEDIUM));		
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		layoutParams.topMargin = getWeaponButtonWidthAccordingToPolicy(BUTTON_SIZE_MEDIUM);
		GameFragment.selectMeButton.setLayoutParams(layoutParams);
		relativeLayout.addView(GameFragment.selectMeButton);
		
		
		
		GameFragment.toggleButtonsLayout.addView(GameFragment.nextTargetButton);
		GameFragment.toggleButtonsLayout.addView(GameFragment.friendsToggleButton);
		GameFragment.toggleButtonsLayout.addView(GameFragment.previousTargetButton);
		
		
		relativeLayout.addView(GameFragment.toggleButtonsLayout);		
	}
	

    
    
    public static int getWeaponButtonWidth(){
    	
    	int densityCategory = getDpiCategory();
    	if(densityCategory == DisplayMetrics.DENSITY_LOW)
    		return 36;
    	
    	if(densityCategory == DisplayMetrics.DENSITY_MEDIUM)
    		return 48;
    	
    	if(densityCategory == DisplayMetrics.DENSITY_HIGH)
    		return 72;
    	
    	if(densityCategory == DisplayMetrics.DENSITY_XHIGH)
    		return 96;
    	
    	if(densityCategory == DisplayMetrics.DENSITY_XXHIGH)
    		return 96;
    	
    	return 48; 
    }
    
    public static int getWeaponMediumButtonWidth(){
    	
    	int densityCategory = getDpiCategory();
    	if(densityCategory == DisplayMetrics.DENSITY_LOW)
    		return 45;
    	
    	if(densityCategory == DisplayMetrics.DENSITY_MEDIUM)
    		return 60;
    	
    	if(densityCategory == DisplayMetrics.DENSITY_HIGH)
    		return 90;
    	
    	if(densityCategory == DisplayMetrics.DENSITY_XHIGH)
    		return 120;
    	
    	if(densityCategory == DisplayMetrics.DENSITY_XXHIGH)
    		return 120;
    	
    	return 60;    	
    }
    
    public static int getWeaponLargeButtonWidth(){
    	
    	int densityCategory = getDpiCategory();
    	if(densityCategory == DisplayMetrics.DENSITY_LOW)
    		return 54;
    	
    	if(densityCategory == DisplayMetrics.DENSITY_MEDIUM)
    		return 72;
    	
    	if(densityCategory == DisplayMetrics.DENSITY_HIGH)
    		return 108;
    	
    	if(densityCategory == DisplayMetrics.DENSITY_XHIGH)
    		return 144;
    	
    	if(densityCategory == DisplayMetrics.DENSITY_XXHIGH)
    		return 144;
    	
    	return 72;
    }
    
    public static int getWeaponButtonWidthAccordingToPolicy(int buttonSizePolicy){
    	
    	if(buttonSizePolicy == BUTTON_SIZE_SMALL)
    		return getWeaponButtonWidth();
    	
    	if(buttonSizePolicy == BUTTON_SIZE_MEDIUM)
    		return getWeaponMediumButtonWidth();
    	
    	if(buttonSizePolicy == BUTTON_SIZE_LARGE)
    		return getWeaponLargeButtonWidth();
    	
    	return getWeaponButtonWidth();
    }
     
    
    
    public static int getScreenWidth(){
    	WindowManager wm = (WindowManager) FragmentUtils.currentActivity.getSystemService(Context.WINDOW_SERVICE);
    	Display display = wm.getDefaultDisplay();
    	return display.getWidth();
    }
    
    public static int getDpiCategory(){
    	DisplayMetrics metrics = FragmentUtils.currentActivity.getResources().getDisplayMetrics();
    	return metrics.densityDpi;
    }
    
    
    
    public static int getOneButtonWidth(int buttonSizePolicy){
    	
    	int oneButtonWidth = 0;
    	
    	
    	if(buttonSizePolicy == BUTTON_SIZE_SMALL)
    		oneButtonWidth = getWeaponButtonWidth();
    		
    	else if(buttonSizePolicy == BUTTON_SIZE_MEDIUM)    	
    		oneButtonWidth = getWeaponMediumButtonWidth();
    	
    	else if(buttonSizePolicy == BUTTON_SIZE_LARGE)    	
    		oneButtonWidth = getWeaponLargeButtonWidth();
    	
    	else
    		oneButtonWidth = 0;
    	
    	return oneButtonWidth;
    }
    
    public static int getWeaponButtonsSpace(int buttonSizePolicy){
    	
    	int unavailableSpace = GameFragment.toggleButtonsLayout.getWidth();
    	
    	int oneButtonWidth = getOneButtonWidth(buttonSizePolicy);
    	
    	
    	int countButtons = StaticData_Players.currentPlayer.getWeapons().size();
    	int screenWidth = getScreenWidth();
    	
    	//Log.i("getWeaponButtonsSpace", "Screen Width : "+String.valueOf(screenWidth));
    	//Log.i("getWeaponButtonsSpace", "NextTargetButton Width : "+String.valueOf(unavailableSpace));
    	//Log.i("getWeaponButtonsSpace", "WeaponButton Width : "+String.valueOf(oneButtonWidth));
    	//Log.i("getWeaponButtonsSpace", "WeaponButton count : "+String.valueOf(countButtons));
    	
    	int interButtonSpace = ((screenWidth * 5/6) - (countButtons * oneButtonWidth)) / (countButtons + 1);
    	
    	//Log.i("getWeaponButtonsSpace", "inter-button space : "+String.valueOf(interButtonSpace));
    	
    	
    	return interButtonSpace;
    }
    
    public static int getButtonSizePolicy(){
    	
    	int buttonSizePolicy = BUTTON_SIZE_LARGE;
    	
    	int interButtonSpace = getWeaponButtonsSpace(buttonSizePolicy);
		
    	if(interButtonSpace < 0){
    		buttonSizePolicy = BUTTON_SIZE_MEDIUM;
			interButtonSpace = getWeaponButtonsSpace(buttonSizePolicy);
		}
    	
    	if(interButtonSpace < 0){
    		buttonSizePolicy = BUTTON_SIZE_SMALL;
			interButtonSpace = getWeaponButtonsSpace(buttonSizePolicy);
		}
    	
    	return buttonSizePolicy;	
    }
    
    
    
    
    
	public static void addWeaponButtons(LinearLayout weaponsLayout){
		
		int numberOfWeapons = StaticData_Players.currentPlayer.getWeapons().size();		
		buttons = new ArrayList<Button>(numberOfWeapons);		
		
		weaponButtonSizePolicy = getButtonSizePolicy();
		
		int interButtonSpace = getWeaponButtonsSpace(weaponButtonSizePolicy);
		//Log.i("addWeaponButtons", "final inter-button space : "+String.valueOf(interButtonSpace));
		
		for(Weapon w: StaticData_Players.currentPlayer.getWeapons()){
			
			Button weaponButton = new Button(weaponsLayout.getContext());			
			weaponButton.setId(GameFragmentDrawingUtils.generateViewId());
			weaponButton.setEnabled(false);
			
						
			int weaponWidth = getWeaponButtonWidthAccordingToPolicy(weaponButtonSizePolicy);
			int weaponIcon = GameElementUtils.getWeaponIconAccordingToPolicy(w, weaponButtonSizePolicy);
			weaponButton.setTextSize((int)(weaponWidth / 4.8));
			weaponButton.setBackgroundResource(GameElementUtils.getWeaponIconAccordingToPolicy(w, weaponButtonSizePolicy));
			
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(weaponWidth, weaponWidth);
			layoutParams.leftMargin = interButtonSpace;
			layoutParams.gravity = Gravity.CENTER_VERTICAL;
			
			//layoutParams.gravity = Gravity.CENTER;			
			
			
							
			
			
			
			
			final Weapon weaponAUX = w;
			final Button weaponButtonAUX = weaponButton;
			
			weaponButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
										
					if(!weaponButtonAUX.isClickable())
						return;
					
					boolean shotWasPerformed = shoot(v, weaponAUX, StaticData_Players.selectedPlayerUUID);
					
					
				}
			});
			
			weaponButton.setOnLongClickListener(new OnLongClickListener() {
				
				@Override
				public boolean onLongClick(View v) {
					
					GameFragmentDrawingUtils.drawWeaponRange(weaponAUX);					
					return true;
				}
			});
			
			weaponButton.setLayoutParams(layoutParams);
			weaponsLayout.addView(weaponButton);
			
			buttons.add(weaponButton);
			
		}
	}
		
	public static void setupWeaponButtons(RelativeLayout relativeLayout){
		
		DisplayMetrics displayMetrics = new DisplayMetrics();
		
		GameFragment.weaponButtonsLayout = new LinearLayout(relativeLayout.getContext());
		GameFragment.weaponButtonsLayout.setOrientation(LinearLayout.HORIZONTAL);
		GameFragment.weaponButtonsLayout.setId(GameFragmentDrawingUtils.generateViewId());
		
		
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		//layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		layoutParams.addRule(RelativeLayout.LEFT_OF, GameFragment.previousTargetButton.getId());		
		layoutParams.addRule(RelativeLayout.ALIGN_TOP, GameFragment.previousTargetButton.getId());
			
		//layoutParams.width = (int) (displayMetrics.widthPixels * 0.8);
		//layoutParams.height = (int) GameFragment.previousTargetButton.getHeight();
		
		GameFragment.weaponButtonsLayout.setLayoutParams(layoutParams);
		
		addWeaponButtons(GameFragment.weaponButtonsLayout);		
				
		relativeLayout.addView(GameFragment.weaponButtonsLayout);
		
	}

	public static void setupTargetButtons(RelativeLayout relativeLayout){
		addTargetButtons(relativeLayout);
		
		setupFriendToggleButton();
		setupPreviousTargetButton();
		setupNextTargetButton();
		setupSelectMeButton();
	}
	
	public static void setupHealthTextView(RelativeLayout relativeLayout){
				
		GameFragment.myHealthIcon = new ImageView(relativeLayout.getContext());
		GameFragment.myHealthIcon.setId(GameFragmentDrawingUtils.generateViewId());
		GameFragment.myHealthIcon.setBackgroundColor(FragmentUtils.currentActivity.getResources().getColor(R.color.muchLessTransparentWhite));
		GameFragment.myHealthIcon.setImageResource(R.drawable.icon_health_small);
		
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		
		GameFragment.myHealthIcon.setLayoutParams(layoutParams);
		relativeLayout.addView(GameFragment.myHealthIcon);
		
		
		
		GameFragment.myHealthTextView = new TextView(relativeLayout.getContext());
		GameFragment.myHealthTextView.setId(GameFragmentDrawingUtils.generateViewId());
		GameFragment.myHealthTextView.setText(String.valueOf(StaticData_Players.currentPlayer.getHealth()));	
		GameFragment.myHealthTextView.setTextColor(Color.BLACK);
		GameFragment.myHealthTextView.setBackgroundColor(FragmentUtils.currentActivity.getResources().getColor(R.color.muchLessTransparentWhite));
		GameFragment.myHealthTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
		GameFragment.myHealthTextView.setGravity(Gravity.CENTER);
		GameFragment.myHealthTextView.setWidth(2 * getWeaponButtonWidth());
		
		layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.RIGHT_OF, GameFragment.myHealthIcon.getId());
		layoutParams.addRule(RelativeLayout.ALIGN_BOTTOM, GameFragment.myHealthIcon.getId());
		layoutParams.addRule(RelativeLayout.ALIGN_TOP, GameFragment.myHealthIcon.getId());
		
		GameFragment.myHealthTextView.setLayoutParams(layoutParams);
		
		relativeLayout.addView(GameFragment.myHealthTextView);
		
	}
		
	
	
	public static void addItemsOnMessagesSpinner(RelativeLayout relativeLayout) {  
		  ArrayList<String> list = new ArrayList<String>();
		  		  
		  list.add("Team Messages >>>");
		  list.add("Go Go Go !");
		  list.add("Cover me !");
		  list.add("Heal me !");
		  list.add("Take the flank !");
		  list.add("Group up !");
		  list.add("Defence !");
		  list.add("Offence !");
		  list.add("Split up !");  
		  
		  
		  
		  ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(relativeLayout.getContext(), android.R.layout.simple_spinner_item, list){
			  
			@Override
			public boolean isEnabled(int position) {
				if(position == 0)
					return false;
				
				return super.isEnabled(position);
			}
			 
			  
		  };
		  
		  dataAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line); 
		  GameFragment.messagesSpinner.setAdapter(dataAdapter);
		  
		  GameFragment.messagesSpinner.getAdapter().getView(0, null, null).setEnabled(false);
	}
	
	public static void setupMessagesSpinner(final RelativeLayout relativeLayout){
		
		GameFragment.messagesSpinner = new Spinner(relativeLayout.getContext());
		GameFragment.messagesSpinner.setId(GameFragmentDrawingUtils.generateViewId());
		GameFragment.messagesSpinner.setBackgroundColor(FragmentUtils.currentActivity.getResources().getColor(R.color.muchLessTransparentWhite));
				
		GameFragment.messagesSpinner.setBackgroundResource(R.drawable.icon_message_2);
		
		addItemsOnMessagesSpinner(relativeLayout);		
		
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.BELOW, GameFragment.myHealthIcon.getId());
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		layoutParams.addRule(RelativeLayout.ALIGN_RIGHT, GameFragment.myHealthIcon.getId());
		
		
		GameFragment.messagesSpinner.setLayoutParams(layoutParams);
		GameFragment.messagesSpinner.setContentDescription("TEAM MESSAGES");
		
		
		GameFragment.messagesSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				
				if(arg2<=0)
					return;
				
				String message = arg0.getItemAtPosition(arg2).toString();
				
				try {
					MessageSender.send_MESSAGE_TEAM(message);
					FragmentUtils.showText(FragmentUtils.currentContext, " ME : \""+message+"\"");					
				} 
				catch (IOException e) {
					Log.println(Log.ERROR, "Could not send in-game message to team !", e.toString());
				}
				
				GameFragment.messagesSpinner.setSelection(0);
												
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
			
			
		});
		
		GameFragment.messagesSpinner.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				
				return false;
			}
		});
		
		
		relativeLayout.addView(GameFragment.messagesSpinner);		
		
	}
	
	
	
	
	public static void setupPreviousTargetButton(){
		GameFragment.previousTargetButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				manageSelectPreviousTarget();				
			}	
		});
	}
		
	public static void setupNextTargetButton(){
		GameFragment.nextTargetButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				manageSelectNextTarget();		
			}
		});		
	}
		
	public static void setupFriendToggleButton(){
		GameFragment.friendsToggleButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				toggleFriends();
			}			
		});
	}	
	
	public static void setupSelectMeButton(){
		
		GameFragment.selectMeButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				GameFragmentDrawingUtils.selectCurrentPlayer();
			}
		});
		
	}
	
	
	
	
	
	public static void manageSelectNextTarget(){
		
		if(GameFragment.friendsToggleButton.isChecked())
			GameFragmentDrawingUtils.selectNextClosestFriend();
		else
			GameFragmentDrawingUtils.selectNextClosestEnemy();
	}
	
	public static void manageSelectPreviousTarget(){
		
		if(GameFragment.friendsToggleButton.isChecked())
			GameFragmentDrawingUtils.selectPreviousClosestFriend();
		else
			GameFragmentDrawingUtils.selectPreviousClosestEnemy();
	}
	
	public static void toggleFriends(){
		
		if(GameFragment.friendsToggleButton.isChecked()){
			selectFriends = true;
			GameFragment.nextTargetButton.setBackgroundResource(R.drawable.button_next_friend);
			GameFragment.previousTargetButton.setBackgroundResource(R.drawable.button_previous_friend);
			GameFragment.friendsToggleButton.setBackgroundResource(R.drawable.button_empty_green);
			
		}
		else{
			selectFriends = false;
			GameFragment.nextTargetButton.setBackgroundResource(R.drawable.button_next_enemy);
			GameFragment.previousTargetButton.setBackgroundResource(R.drawable.button_previous_enemy);
			GameFragment.friendsToggleButton.setBackgroundResource(R.drawable.button_weapon_empty_weapon_medium);
		}
	}
	
	
	
	
	
	public static void startAbilityCountdown(AbilityCountdown abilityCountdown){
		Thread countdownThread = new Thread(abilityCountdown);
		countdownThread.start();
	}
	
	public static void startWeaponCooldown(View pressedButtonView, Weapon currentWeapon){
		
		WeaponReportingTask cooldownTimer = new WeaponReportingTask((Button)pressedButtonView);									
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			
			Log.println(Log.INFO, "Build version > HONEYCOMB", "running on custom executor");
			cooldownTimer.executeOnExecutor(GameFragment.executor, currentWeapon);
		}
		else {
			Log.println(Log.INFO, "Build version < HONEYCOMB", "running on default executor");
			cooldownTimer.execute(currentWeapon);
		}		
	}
		
	public static void startWeaponDuration(View textView, final Weapon currentWeapon){
		
		final DurationReportingTask cooldownTimer = new DurationReportingTask((TextView)textView);
				
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			
			Log.println(Log.INFO, "Build version > HONEYCOMB", "running on custom executor");
			cooldownTimer.executeOnExecutor(GameFragment.executor, currentWeapon);
		}
		else {
			Log.println(Log.INFO, "Build version < HONEYCOMB", "running on default executor");
			cooldownTimer.execute(currentWeapon);
		}
			
	}
	
	
	
	
	
	
	
	
	
	
	private static boolean cooldownHasExpired(Weapon currentWeapon){
		
		if(GameFragmentMapUtils.getTimeDelta(currentWeapon.getLastUsed(), true) <= currentWeapon.getCooldown() ){
			FragmentUtils.showText(GameFragment.mapFragmentView.getContext(), "can't shoot this weapon yet !");
			return false;	
		}		
		
		return true;
	}
		
	private static boolean targetCanBeSeen(Player targetPlayer){
		if(targetPlayer.isInvisible){
			FragmentUtils.showText(GameFragment.mapFragmentView.getContext(), "you can't see'em, you can't shoot'em !");
			return false;
		}
		
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	private static synchronized boolean shoot_generic_weapon(UUID targetID, Weapon currentWeapon, View pressedButtonView){
		
		Player targetPlayer = StaticData_Players.getPlayerById(targetID);
		String currentWeaponName = currentWeapon.getName();
		
		if(targetPlayer == null){
			FragmentUtils.showText(FragmentUtils.currentActivity, "target = null !");
			return false;
		}
		
		int damage = currentWeapon.getDamage();
		
		//doesn't shoot if the cooldown hasn't passed
		if(!cooldownHasExpired(currentWeapon))
			return false;
		
		//can't shoot an invisible target
		if(!targetCanBeSeen(targetPlayer))
			return false;
		
		
		//shield reduces damage to half
		if(targetPlayer.isShielded)
			damage /= 2;
		
				
		//mark the last time the weapon was used. with or without the countdown on the button, this restricts use.
		long shotTime = GameFragmentMapUtils.getCurrentTimeMillis();
		currentWeapon.setLastUsed(shotTime);
				
		MessageSender.send_SHOOT(targetID, currentWeaponName, damage);
		
		targetPlayer.modifyHealth(damage);
		
		GameFragmentDrawingUtils.redrawPlayer(targetID);
		
		//disable the button and show a countdown until it will be enabled again		
		startWeaponCooldown(pressedButtonView, currentWeapon);
		
		return true;
	}
		
	private static boolean shoot_invisibility_cloak(View pressedButtonView, Weapon currentWeapon){
		
		if(!cooldownHasExpired(currentWeapon))
			return false;
		
		
		UUID targetID = StaticData_Players.currentPlayerUUID;
		
		Player target = StaticData_Players.getPlayerById(targetID);
		target.isInvisible = true;
		startAbilityCountdown(StaticData_ProfessionsAndWeapons.setInvisiblityCountdown(targetID));
		
		long shotTime = GameFragmentMapUtils.getCurrentTimeMillis();
		currentWeapon.setLastUsed(shotTime);
		
		MessageSender.send_SHOOT(targetID, currentWeapon.getName(), currentWeapon.getDamage());		
		
		GameFragmentUpdaterUtils.showText("You have used Invisibility Cloak on yourself !");		
		addPowerupCounter(currentWeapon);
		startWeaponCooldown(pressedButtonView, currentWeapon);
		
		return true;
	}
	
	private static boolean shoot_shield(View pressedButtonView, Weapon currentWeapon, UUID targetID){
				
		if(!cooldownHasExpired(currentWeapon))
			return false;
		
		Player targetPlayer = StaticData_Players.getPlayerById(targetID);
			
		if(targetPlayer == null){			
			return false;
		}		
		
		if(!targetPlayer.isShielded){
			
			targetPlayer.isShielded = true;
			startAbilityCountdown(StaticData_ProfessionsAndWeapons.setShieldCountdown(targetID));
						
			long shotTime = GameFragmentMapUtils.getCurrentTimeMillis();		
			currentWeapon.setLastUsed(shotTime);			
			MessageSender.send_SHOOT(targetID, currentWeapon.getName(), currentWeapon.getDamage());
			
			if(targetID.equals(StaticData_Players.currentPlayerUUID)){
				GameFragmentUpdaterUtils.showText("You have used Shield on yourself !");
				addPowerupCounter(currentWeapon);
			}
			
			else
				GameFragmentUpdaterUtils.showText("You have used Shield on " + StaticData_Players.getPlayerById(targetID).getNickname());
		}
		
		else{			
			GameFragmentUpdaterUtils.showText("This player is already shielded !");			
		}		
		
		startWeaponCooldown(pressedButtonView, currentWeapon);
		
		return true;
	}
		
	private static boolean shoot_painkillers(View pressedButtonView, Weapon currentWeapon, UUID targetID){
		
		Player target = StaticData_Players.getPlayerById(targetID);
		
		if(target == null){			
			return false;
		}
			
		
		String profession = target.getProfessionTitle();
		int maxProfessionHealth = StaticData_ProfessionsAndWeapons.professions.get(profession).health;
		
		int healthDifference = maxProfessionHealth - target.getHealth();		
		int healAmount = (healthDifference >= (-1)*currentWeapon.getDamage()) ? currentWeapon.getDamage() : (-1) * healthDifference; 
		
		if(!cooldownHasExpired(currentWeapon))
			return false;
		
		if(healAmount == 0){
			GameFragmentUpdaterUtils.showText("Nothing to heal...");
			return false;
		}
		
		
		
		
		target.modifyHealth(healAmount);
		GameFragmentUpdaterUtils.updateHealthTextView();
		long shotTime = GameFragmentMapUtils.getCurrentTimeMillis();
		currentWeapon.setLastUsed(shotTime);
		
		
		
		
		MessageSender.send_SHOOT(targetID, currentWeapon.getName(), healAmount);
		startWeaponCooldown(pressedButtonView, currentWeapon);		
		
		if(targetID.equals(StaticData_Players.currentPlayerUUID))
			GameFragmentUpdaterUtils.showText("You have healed yourself ( "+ (-1)*healAmount+" health)");
		
		else
			GameFragmentUpdaterUtils.showText("You have healed" + StaticData_Players.getPlayerById(targetID).getNickname() + " ( "+ (-1)*healAmount+" health)");
		
		
		return true;
		
	}
	
	
	
	
	
	
	
	
	
	
	private static boolean shootRequirementsSatisfied(Weapon weapon, UUID targetID){
		
		
		if(StaticData_Players.playerIsEnemy(targetID)){
			if(weapon.getPolicy() == Weapon.POLICY_ENEMIES)
				return true;
			
			if(weapon.getPolicy() == Weapon.POLICY_FRIENDS)
				return false;
			
			if(weapon.getPolicy() == Weapon.POLICY_FRIENDS_AND_ENEMIES)
				return true;
			
			if(weapon.getPolicy() == Weapon.POLICY_FRIENDS_AND_ENEMIES_AND_SELF)
				return true;
			
			if(weapon.getPolicy() == Weapon.POLICY_FRIENDS_AND_SELF)
				return false;
			
			if(weapon.getPolicy() == Weapon.POLICY_SELF)
				return false;			
		}
		
		if(StaticData_Players.playerIsFriend(targetID)){
			if(weapon.getPolicy() == Weapon.POLICY_ENEMIES)
				return false;
			
			if(weapon.getPolicy() == Weapon.POLICY_FRIENDS)
				return true;
			
			if(weapon.getPolicy() == Weapon.POLICY_FRIENDS_AND_ENEMIES)
				return true;
			
			if(weapon.getPolicy() == Weapon.POLICY_FRIENDS_AND_ENEMIES_AND_SELF)
				return true;
			
			if(weapon.getPolicy() == Weapon.POLICY_FRIENDS_AND_SELF)
				return true;
			
			if(weapon.getPolicy() == Weapon.POLICY_SELF)
				return false;			
		}
		
		
		if(StaticData_Players.playerIsMe(targetID)){
			if(weapon.getPolicy() == Weapon.POLICY_ENEMIES)
				return false;
			
			if(weapon.getPolicy() == Weapon.POLICY_FRIENDS)
				return false;
			
			if(weapon.getPolicy() == Weapon.POLICY_FRIENDS_AND_ENEMIES)
				return false;
			
			if(weapon.getPolicy() == Weapon.POLICY_FRIENDS_AND_ENEMIES_AND_SELF)
				return true;
			
			if(weapon.getPolicy() == Weapon.POLICY_FRIENDS_AND_SELF)
				return true;
			
			if(weapon.getPolicy() == Weapon.POLICY_SELF)
				return true;
		}
		
		return false;
		
	}
	
	private static UUID approveShotOnTarget(Weapon weapon, UUID targetID){
		
		if(weapon.getName().equals("Invisibility Cloak")){			
			return StaticData_Players.currentPlayerUUID;
		}
		
		
		if((weapon.getPolicy() == Weapon.POLICY_SELF) || 
			(weapon.getPolicy() == Weapon.POLICY_FRIENDS_AND_SELF) || 
			(weapon.getPolicy() == Weapon.POLICY_FRIENDS_AND_ENEMIES_AND_SELF)){
			
			if(StaticData_Players.selectedPlayerUUID == null)
				targetID = StaticData_Players.currentPlayerUUID;
		}
		
		if(targetID == null)
			Log.i("GameFragmentButtonsUtils", "shoot (approveShot): targetID = null");
		else
			Log.i("GameFragmentButtonsUtils", "shoot (approveShot): targetID = "+targetID.toString());
		
		
		if(targetID == null){
			return null;
		}
		
		if(!shootRequirementsSatisfied(weapon, targetID))
			return null;
		
		double distance = GameFragmentMapUtils.getDistanceBwtweenMeAndPlayer(targetID);
		
		int range = weapon.getRange();
		
		if(distance > range){
			FragmentUtils.showText(GameFragment.mapFragmentView.getContext(), "distance "+String.valueOf(distance) + ". TOO FAR ! Get closer !");
			return null;
		}
		
		return targetID;
		
		
	}
	
	public static boolean shoot(View pressedButtonView, Weapon weapon, UUID targetID){
		
		
		if((pressedButtonView == null) || (weapon == null))
			return false;
		
		
		if(targetID == null)
			Log.i("GameFragmentButtonsUtils", "shoot (before): targetID = null");
		else
			Log.i("GameFragmentButtonsUtils", "shoot (before): targetID = "+targetID.toString());
		
		
		
		targetID = approveShotOnTarget(weapon, targetID); 
		
		if(targetID == null)
			return false;
		
		if(targetID == null)
			Log.i("GameFragmentButtonsUtils", "shoot (after): targetID = null");
		else
			Log.i("GameFragmentButtonsUtils", "shoot (after): targetID = "+targetID.toString());
		
		
		
		if(weapon.getName().equals("Invisibility Cloak")){			
			return shoot_invisibility_cloak(pressedButtonView, weapon);
		}
		
		if(weapon.getName().equals("Shield")){			
			return shoot_shield(pressedButtonView, weapon, targetID);
		}
		
		if(weapon.getName().equals("Painkillers")){			
			return shoot_painkillers(pressedButtonView, weapon, targetID);
		}
		
		GameFragmentUpdaterUtils.showText("You have used " + weapon.getName() + " on " + StaticData_Players.getPlayerById(targetID).getNickname());
		return shoot_generic_weapon(targetID, weapon, pressedButtonView);
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
