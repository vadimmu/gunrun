package ro.vadim.gunrun.game.misc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Games {

	public static ArrayList<Game> gamesList = new ArrayList<Game>(10);
	
	public static boolean refreshInProgress = false;
		
	private static Game currentGame = null;
	
	public synchronized static Game getGameByPosition(int position){		
		return gamesList.get(position);
	}
	
	
	
	public synchronized static Game getGameByID(UUID gameID){
		
		for(Game g: gamesList){
			if(g.getGameID().equals(gameID))
				return g;			
		}
		
		return null;
	}
	
	
	
	
	
	
	
	public synchronized static void resetGamesList(){		
		gamesList.clear();
		currentGame = null;
	}
	
	
	
	
	public synchronized static void addGame(Game newGame){
		gamesList.add(newGame);
	}
	
	public synchronized static void removeGame(UUID gameID){
		gamesList.remove(getGameByID(gameID));
	}

	

	
	
	
	
	public static Game getCurrentGame() {
		return currentGame;
	}



	public static void setCurrentGame(Game game) {
		Games.currentGame = game;
	}
	
	
	
	
	
	
	
}
