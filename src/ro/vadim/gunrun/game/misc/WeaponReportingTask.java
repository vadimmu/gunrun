package ro.vadim.gunrun.game.misc;

import java.util.ArrayList;
import java.util.concurrent.Executor;

import ro.vadim.gunrun.R;
import ro.vadim.gunrun.game.gameelements.Weapon;
import ro.vadim.gunrun.game.utils.GameElementUtils;
import ro.vadim.gunrun.game.utils.GameFragmentButtonsUtils;
import ro.vadim.gunrun.game.utils.GameFragmentMapUtils;

import com.google.android.maps.MapView;

import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.text.format.Time;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class WeaponReportingTask extends AsyncTask<Weapon, Integer, Integer> {	
  
	
	private boolean hasFinished = true;
	
	Button pressedButton = null;
	String initialWeaponName = "";
	Weapon currentWeapon = null;
	Drawable currentWeaponIcon = null; 
	
	
	public WeaponReportingTask(Button newPressedButton){
		
		super();
		
		hasFinished = true;
		
		pressedButton = newPressedButton;
		
		initialWeaponName = pressedButton.getText().toString();
	}


	
	

	
	
	
	@Override
 	protected Integer doInBackground(Weapon... params) {
		
		
		hasFinished = false;
		
		Weapon weapon = params[0];
		currentWeapon = weapon;
		currentWeaponIcon = pressedButton.getBackground();
 		if(weapon.getLastUsed() == null) 
 			return 0;
 		
 		
 		int secondsElapsed = (int) GameFragmentMapUtils.getTimeDelta(weapon.getLastUsed(), true);
 		
 		while(secondsElapsed <= weapon.getCooldown()){
 			
 			publishProgress(weapon.getCooldown() - secondsElapsed);
 			
 			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
 			
 			secondsElapsed = (int) GameFragmentMapUtils.getTimeDelta(weapon.getLastUsed(), true);
 		}
 		
 		return 1;
 	}
     
    
	private int getEmptyButtonIconAccordingToPolicy(int buttonSizePolicy){
		
		if(buttonSizePolicy == GameFragmentButtonsUtils.BUTTON_SIZE_SMALL){
			if(GameElementUtils.isWeaponAPowerup(currentWeapon))
	    		return R.drawable.button_weapon_empty_powerup;
	    	else
	    		return R.drawable.button_weapon_empty_weapon;
		}
		
		if(buttonSizePolicy == GameFragmentButtonsUtils.BUTTON_SIZE_MEDIUM){
			if(GameElementUtils.isWeaponAPowerup(currentWeapon))
	    		return R.drawable.button_weapon_empty_powerup_medium;
	    	else
	    		return R.drawable.button_weapon_empty_weapon_medium;
		}
		
		if(buttonSizePolicy == GameFragmentButtonsUtils.BUTTON_SIZE_LARGE){
			if(GameElementUtils.isWeaponAPowerup(currentWeapon))
	    		return R.drawable.button_weapon_empty_powerup_large;
	    	else
	    		return R.drawable.button_weapon_empty_weapon_large;
		}
		
		if(GameElementUtils.isWeaponAPowerup(currentWeapon))
    		return R.drawable.button_weapon_empty_powerup;
    	else
    		return R.drawable.button_weapon_empty_weapon;				

	}
	
	
    protected synchronized void onProgressUpdate(Integer... progress) {
    	
    	int interButtonSpace = GameFragmentButtonsUtils.getWeaponButtonsSpace(GameFragmentButtonsUtils.weaponButtonSizePolicy);
    	
		pressedButton.setBackgroundResource(getEmptyButtonIconAccordingToPolicy(GameFragmentButtonsUtils.weaponButtonSizePolicy));
		
    	    	
    	if(pressedButton.isClickable())
    		pressedButton.setClickable(false);
    	
    	if(progress != null)
    		if(progress[0] < 10)    		
    			pressedButton.setText("0"+progress[0].toString());
    	
    		else
    			pressedButton.setText(progress[0].toString());
    	
    	else 
    		pressedButton.setText(initialWeaponName);
    }

    
    @Override
    protected void onPostExecute(Integer result) {
    	
    	pressedButton.setBackgroundDrawable(currentWeaponIcon);
    	pressedButton.setText(initialWeaponName);
    	pressedButton.setClickable(true);
    	super.onPostExecute(result);
    	
    	hasFinished = true;
    	
    }
    
    @Override
    protected void onCancelled() {
    	
    	
    	pressedButton.setText(initialWeaponName);    	
    	super.onCancelled();
    	
    	hasFinished = true;
    }



	public boolean hasFinished() {
		return hasFinished;		
	}    
}
