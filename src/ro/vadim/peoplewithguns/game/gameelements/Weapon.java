package ro.vadim.peoplewithguns.game.gameelements;

import java.util.Map;

import android.os.SystemClock;
import android.text.format.Time;

public class Weapon {
	
	public static final int POLICY_SELF = 0;
	
	public static final int POLICY_FRIENDS = 1;
	
	public static final int POLICY_FRIENDS_AND_SELF = 2;
	
	public static final int POLICY_ENEMIES = 3;	
	
	public static final int POLICY_FRIENDS_AND_ENEMIES = 4;
	
	public static final int POLICY_FRIENDS_AND_ENEMIES_AND_SELF = 5;
	
	
	
	public String name = "";
	public int range = 0;
	public int damage = 0;
	public int cooldown = 0; //in seconds 
	public int duration = 0;//0 means instant. anything above 0 is for the SHIELD and RADAR
	public int policy = POLICY_ENEMIES;
	public String description = "";
	
	
	
	
	private Long lastUsed = (long) 0; 
	
	
	
	public static void configureDefaultWeapons(Map<String, Object> weapons){
		//TODO implementeaza asta cand ai un server functional si comunicarea aranjata
		
		
		
	}
	
	public Weapon(Weapon newWeapon){
		name = newWeapon.name;
		range = newWeapon.range;
		damage = newWeapon.damage;
		cooldown = newWeapon.cooldown;
		duration = newWeapon.duration;
		policy = newWeapon.policy;
		description = newWeapon.description;
		
	}
	
	public Weapon(String newName, int newRadius, int newDamage, int newCooldownTime, int newDuration, int newPlayerSelectionPolicy, String newDescription){
		name = newName;
		range = newRadius;
		damage = newDamage;
		cooldown = newCooldownTime;
		duration = newDuration;
		policy = newPlayerSelectionPolicy;
		description = newDescription;
	}

	public Long getLastUsed() {
		return lastUsed;
	}

	public void setLastUsed(Long lastUsed) {
		Time time = new Time();
		time.setToNow();				
		this.lastUsed = time.toMillis(false);
	}
	
}
