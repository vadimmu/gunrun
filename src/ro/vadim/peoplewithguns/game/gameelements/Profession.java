package ro.vadim.peoplewithguns.game.gameelements;

import java.util.ArrayList;

public class Profession {
		
	public static int DEFAULT_NUMBER_OF_WEAPONS = 3;
	
	
	public String title = "";	
	public int health = 0;
	public ArrayList<String> weapons = null;
	public String description = "";
	
	
	public Profession(ArrayList<String> newWeapons, int newHealth, String newTitle, String newDescription){
				
		this.weapons = newWeapons;
		this.health = newHealth;
		this.title = newTitle;
		this.description = newDescription;
		
	}
	
}
