package ro.vadim.peoplewithguns.game.fragments;



import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.UUID;

import ro.vadim.peoplewithguns.R;
import ro.vadim.peoplewithguns.communication.TcpGameClient;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.game.gameelements.Player;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;
import ro.vadim.peoplewithguns.game.storage_and_data.Storage;
import ro.vadim.peoplewithguns.game.utils.FragmentUtils;
import ro.vadim.peoplewithguns.game.utils.GameFragmentMapUtils;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface.OnShowListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.ColorStateList;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

public class LobbyFragment extends Fragment{
	
	private Activity parentActivity = null;
	public View currentView = null;
	
	
	private Button readyButton = null;
	private Button changeInfoButton = null;
	private Button changeTeamButton = null;
	private Button backButton = null;
	
	private ListView list_HomeTeam = null;
	private ListView list_AwayTeam = null;
	
	private ArrayList<String> homeTeam = null;
	private ArrayList<String> awayTeam = null;
		
	private boolean ready = false;
	private boolean isCurrentPlayerInHomeTeam = false;
	
	
	public LobbyFragment(){
		super();
	}
			
	public void setReadyButtonEnabled(boolean enabled){
		if(readyButton != null){
			//readyButton.setEnabled(enabled);
		}
	}
	
	
	private void setupBackButton(View view){
		backButton = (Button) view.findViewById(R.id.button_back);
		final View myView = view;
		
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				TcpGameClient.stopServer();								
				FragmentUtils.loadMainMenuFragment(parentActivity);				
			}
		});
		
	}
	
	private void setupChangeTeamButton(View view){
		changeTeamButton = (Button) view.findViewById(R.id.button_change_team);
		final View myView = view;
		
		changeTeamButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Toast toast = Toast.makeText(myView.getContext(), "Friends : "+String.valueOf(StaticData_Players.friends.size())+" Enemies : "+String.valueOf(StaticData_Players.enemies.size()), Toast.LENGTH_SHORT);
				toast.show();
				
				try {
					StaticData_Players.currentPlayer_changeTeam();					
					
					setupLists(myView);					
					
					Log.println(Log.INFO, "SENDING MESSAGE", "CHANGE_TEAM");
					MessageSender.send_CHANGE_TEAM();
				} 
				catch (IOException e) {
					Log.println(Log.ERROR, "SEND ERROR","Could not send CHANGE TEAM");
					Log.println(Log.ERROR, "SEND ERROR", e.toString());
					e.printStackTrace();
				}
				
			}
		});
	}
	
	private void setupReadyButton(View view){
		readyButton = (Button) view.findViewById(R.id.button_ready);
		final View myView = view;
		
		readyButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				FragmentUtils.showText(myView.getContext(), "Ready !");
				
				
				if(ready == false){
					changeInfoButton.setEnabled(false);
					changeTeamButton.setEnabled(false);
					ready = true;
					
					StaticData_Players.currentPlayer.isReady = true;
					MessageSender.send_READY(true);
				}
				
				else{
					changeInfoButton.setEnabled(true);
					changeTeamButton.setEnabled(true);				
					ready = false;
					
					StaticData_Players.currentPlayer.isReady = false;
					MessageSender.send_READY(false);
				}
				
				setupLists(myView);
			}
		});
		
		
	}
	
	private void setupChangeInfoButton(View view){
		changeInfoButton = (Button) view.findViewById(R.id.button_change_info);
		final View myView = view;
		
		changeInfoButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
								
				FragmentUtils.loadLobbySettingsFragment(parentActivity);
								
			}
		});
		
	}
		
	
	
	
	private void setupButtons(View view){
		setupBackButton(view);
		setupChangeInfoButton(view);
		setupChangeTeamButton(view);
		setupReadyButton(view);
	}
	
	
	public void manageListClick(int arg2){
		if((changeInfoButton.isEnabled())&&(isCurrentPlayerInHomeTeam == true ) && (arg2 == 0)){
			FragmentUtils.loadLobbySettingsFragment(parentActivity);
		}		
	}
	
	public void setupLists(View view){
		
		
		addPlayers(StaticData_Players.currentPlayerTeam);
				
		
		list_HomeTeam = (ListView) view.findViewById(R.id.list_home_team);
		list_AwayTeam = (ListView) view.findViewById(R.id.list_away_team);
		
		
		list_HomeTeam.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				manageListClick(arg2);
			}
		});
		
		list_AwayTeam.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				manageListClick(arg2);
			}
		});	
		
		
		list_HomeTeam.setAdapter(
				
				new ArrayAdapter<String>(
						view.getContext(),
						android.R.layout.simple_list_item_1,
						android.R.id.text1,
						homeTeam
				)				
		);
		
		
		list_AwayTeam.setAdapter(
				new ArrayAdapter<String>(
						view.getContext(),
						android.R.layout.simple_list_item_1,
						android.R.id.text1,
						awayTeam
				)
		);
		
		
		((ArrayAdapter<String>)list_HomeTeam.getAdapter()).notifyDataSetChanged();
		((ArrayAdapter<String>)list_AwayTeam.getAdapter()).notifyDataSetChanged();
				
	}
	
	
	
	
	public void setParentActivity(Activity parentActivity){
		this.parentActivity = parentActivity;
	}
	
	public String constructPlayerInfo(Player player){
		if(player != null){
			
			String playerString = player.getNickname()+'\n'+"("+player.getProfessionTitle()+")";
			
			if(player.isReady)
				playerString+=" [ READY ]";
			
			return playerString;
		}
		else
			return "PLAYER NULL";
	}
	
	
	
	public void addPlayersAccordingToMyTeam(ArrayList<String> myTeam, ArrayList<String> enemyTeam){
		
		//add current player
		myTeam.add("[ ME ]" + constructPlayerInfo(StaticData_Players.currentPlayer));
				
		//add the other players
		if(StaticData_Players.friends.size() >= 0){
			for(Player p : StaticData_Players.friends.values()){
				myTeam.add(constructPlayerInfo(p));
			}
		}
		if(StaticData_Players.enemies.size() >= 0){
			for(Player p : StaticData_Players.enemies.values()){
				enemyTeam.add(constructPlayerInfo(p));
			}
		}
		
		
	}
	
	public void addPlayers(int myTeam){
		
		if(StaticData_Players.friends == null)
			StaticData_Players.friends = new LinkedHashMap<UUID, Player>(StaticData_Players.maxPlayersPerTeam);
		
		if(StaticData_Players.enemies == null)
			StaticData_Players.enemies = new LinkedHashMap<UUID, Player>(StaticData_Players.maxPlayersPerTeam);
		
		
		//if(homeTeam == null) 
		homeTeam = new ArrayList<String>(StaticData_Players.maxPlayersPerTeam);
				
		//if(awayTeam == null) 
		awayTeam = new ArrayList<String>(StaticData_Players.maxPlayersPerTeam);
		
		
		if(myTeam == StaticData_Players.HOME_TEAM){
			isCurrentPlayerInHomeTeam = true;
			addPlayersAccordingToMyTeam(homeTeam, awayTeam);
		}
		else{
			isCurrentPlayerInHomeTeam = false;
			addPlayersAccordingToMyTeam(awayTeam, homeTeam);
		}
	}
	
	

	
	
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {    
    		    
	    View view = inflater.inflate(R.layout.fragment_lobby_main, container, false);
	    	    
	    
	    view.setBackgroundResource(R.drawable.background_lobby);
	    
	    FragmentUtils.setCurrentFragment(this);	    
	    FragmentUtils.setCurrentContext(view.getContext());	    	    
	    
	    setupLists(view);
	    setupButtons(view);
	    
	    currentView = view;
	    
	    return view;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Log.println(Log.INFO, "context ?", String.valueOf(parentActivity));
        
    }

    @Override
    public void onStart() {
        super.onStart();
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
    
    @Override
    public void onHiddenChanged(boolean hidden) {
    	super.onHiddenChanged(hidden);
    	
    }
    

    @Override
    public void onPause() {
    	super.onPause();
    	
    	Log.i("onPause called !", "...");
    	Storage.saveCurrentPlayerData(FragmentUtils.currentActivity);
    }
    
	@Override
	public void onDetach() {
		super.onDetach();
		Storage.saveCurrentPlayerData(FragmentUtils.currentActivity);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Storage.saveCurrentPlayerData(FragmentUtils.currentActivity);
	}
    
    

    
    
}