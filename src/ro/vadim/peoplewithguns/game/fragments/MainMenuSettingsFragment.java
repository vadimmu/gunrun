package ro.vadim.peoplewithguns.game.fragments;



import java.net.InetAddress;
import java.net.UnknownHostException;

import ro.vadim.peoplewithguns.R;
import ro.vadim.peoplewithguns.communication.CommunicationUtils;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;
import ro.vadim.peoplewithguns.game.storage_and_data.Storage;
import ro.vadim.peoplewithguns.game.utils.AdminUtils;
import ro.vadim.peoplewithguns.game.utils.FragmentUtils;
import ro.vadim.peoplewithguns.game.utils.GameFragmentMapUtils;


import android.app.Activity;
import android.content.Context;
import android.opengl.Visibility;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.view.View.OnClickListener;

public class MainMenuSettingsFragment extends Fragment {
	
	private Activity parentActivity = null;
	
	private Button okButton = null;
	
	private EditText playerIdText = null;
	private EditText serverIpAddressText = null;
	private EditText serverPortText = null;
	
	
	
	private boolean ready = false;
	
	private String previousText = "";
	

	private void setupPlayerIdText(View view){
		playerIdText = (EditText) view.findViewById(R.id.player_id_text);
		playerIdText.setKeyListener(null);
		
		if(StaticData_Players.currentPlayerUUID == null)
			playerIdText.setText("null");
		
		else
			playerIdText.setText(StaticData_Players.currentPlayerUUID.toString());
		
	}
	
	private void setupServerIpAddressText(View view){
		serverIpAddressText = (EditText) view.findViewById(R.id.server_address_text);
		serverIpAddressText.setText(CommunicationUtils.defaultServerAddress);
	}
	
	private void setupServerPortText(View view){
		serverPortText = (EditText) view.findViewById(R.id.server_port_text);
		serverPortText.setText(String.valueOf(CommunicationUtils.defaultServerPort));
	}
	
	
	
	
	private void setupOkButton(View view){
		okButton = (Button) view.findViewById(R.id.button_settings_ok);
		final View myView = view;
		
		okButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {			
				
				try{
					
					String ip = serverIpAddressText.getText().toString();
					int port = Integer.valueOf(serverPortText.getText().toString());
					
					if(CommunicationUtils.validateIPAddress(ip) == false){
						throw new UnknownHostException("validateIpAddress : the IP Address is not valid !");						
					}				
					
					CommunicationUtils.defaultServerAddress = ip;
					CommunicationUtils.defaultServerPort = port;
					
					
					if((CommunicationUtils.defaultServerAddress == "")||(CommunicationUtils.defaultServerAddress == null)){
						
						Toast toast = Toast.makeText(myView.getContext(), "You have not entered a valid IP address.", Toast.LENGTH_SHORT);
						toast.show();
						return;
					}
					
					else{
						
						FragmentUtils.loadMainMenuFragment(parentActivity);			    
					}
					
				}
				catch(NumberFormatException e){
					Log.println(Log.ERROR, "MainMenuSettings : you have not entered a valid port number", e.toString());
					AdminUtils.buildAlertMessage(FragmentUtils.currentContext, "The Port that you have entered is invalid...");
				}				
				catch(UnknownHostException e1) {
					Log.println(Log.ERROR, "MainMenuSettings : you have not entered a valid server address", e1.toString());
					AdminUtils.buildAlertMessage(FragmentUtils.currentContext, "The Server Address is invalid...");					
				}
				
			
			}
		});
	}
		
	
	private void setupButtons(View view){
		
		setupPlayerIdText(view);
		setupServerIpAddressText(view);
		setupServerPortText(view);		
		setupOkButton(view);
	}
	
	
	
	public void setParentActivity(Activity parentActivity){		
		this.parentActivity = parentActivity;
		
	}
	
	
	public MainMenuSettingsFragment(){
		super();
	}
	
	
	
	
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {    
    		    
	    View view = inflater.inflate(R.layout.fragment_menu_settings, container, false);
	    	    
	    FragmentUtils.setCurrentFragment(this);
	    FragmentUtils.setCurrentContext(view.getContext());
	    
	    Storage.loadServerData(FragmentUtils.currentActivity);
	    
	    setupButtons(view);
	    
	    
	    return view;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Log.println(Log.INFO, "context ?", String.valueOf(parentActivity));
        
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
    
    @Override
    public void onPause() {
    	super.onPause();
    	Storage.saveServerData(FragmentUtils.currentActivity);
    }
    
	@Override
	public void onDetach() {
		super.onDetach();
		Storage.saveServerData(FragmentUtils.currentActivity);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		Storage.saveServerData(FragmentUtils.currentActivity);
	}
    
    
    
}