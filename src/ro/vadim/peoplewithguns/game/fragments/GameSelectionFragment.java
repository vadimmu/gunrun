package ro.vadim.peoplewithguns.game.fragments;

import java.io.IOException;
import java.util.ArrayList;

import ro.vadim.peoplewithguns.R;
import ro.vadim.peoplewithguns.communication.TcpGameClient;
import ro.vadim.peoplewithguns.communication.TcpGameManagementClient;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.game.gameelements.Profession;
import ro.vadim.peoplewithguns.game.misc.Game;
import ro.vadim.peoplewithguns.game.misc.Games;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_ProfessionsAndWeapons;
import ro.vadim.peoplewithguns.game.utils.FragmentUtils;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class GameSelectionFragment extends Fragment{
	
	
	public Button createGameButton = null;
	public Button refreshGamesButton = null;
	public Button joinGameButton = null;
	public Spinner gamesSpinner = null;	
	
	private LayoutInflater inflater = null;	
	private ArrayList<String> games = null;
	
	Integer selectedGame = null;
	
	
	private void refreshGamesList_unsafe(){
		
		Context context = FragmentUtils.getCurrentFragment().getView().getContext();		
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, games);		
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		gamesSpinner.setAdapter(dataAdapter);
		
		gamesSpinner.postInvalidate();		
	}
	
	public void refreshGamesList(){
		if(!FragmentUtils.isRunningOnUiThread()){
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					refreshGamesList_unsafe();
				}
			});
		}
		
		else{
			refreshGamesList_unsafe();
		}
	}
	
	
	
	
	public void createGameCreationDialog(View view){
		
		
		View extraComponents = inflater.inflate(R.layout.dialog_create_game, null);
		
		final EditText gameNameText = (EditText) extraComponents.findViewById(R.id.text_game_name);
				
		final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
		
		builder.setView(extraComponents);
		
	    builder.setCancelable(false)
	    		.setPositiveButton("Create", new DialogInterface.OnClickListener() {
	               public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
	            	   MessageSender.send_CREATE_GAME(gameNameText.getText().toString());
	               }	               
	    		})
	    		.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();						
					}
				});
	    		
	    final AlertDialog alert = builder.create();
	    alert.show();
	}
	
	
	
	
	public void setupGamesSpinner(View view){
		
		setGames(new ArrayList<String>(10));
		
		gamesSpinner = (Spinner) view.findViewById(R.id.spinner_games_list);
			
		
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_dropdown_item, games);
				
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		gamesSpinner.setAdapter(dataAdapter);
				
				
		gamesSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {				
				selectedGame = arg2;
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
								
			}
		});

		
		
		
	}
	
	public void setupCreateGamesButton(View view){
		createGameButton = (Button) view.findViewById(R.id.button_create_game);
		
		final View auxView = view;
		
		createGameButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				createGameCreationDialog(auxView);
			}
		});
		
	}
		
	public void setupRefreshGameButton(View view){
		refreshGamesButton = (Button) view.findViewById(R.id.button_refresh_games);
		refreshGamesButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Games.refreshInProgress = true;
				MessageSender.send_GAMES_LIST();				
			}
		});
		
	}
	
	public void setupJoinGameButton(View view){
		joinGameButton = (Button) view.findViewById(R.id.button_join_game);
		joinGameButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				Thread newThread = new Thread(new Runnable() {
					
					@Override
					public void run() {
						
						
						if(Games.gamesList == null)
							return;
						
						if(selectedGame == null)
							return;
						
						if(selectedGame > Games.gamesList.size() - 1)
							return;
						
						
						TcpGameManagementClient.stopServer();
						
						Game game = Games.getGameByPosition(selectedGame.intValue());
						
						if(game == null)
							return;
						
						
						if(game.getIp().equals("0.0.0.0")){
							
							Log.e("GameSelectionFragment", "IP is 0.0.0.0. Can't use it !");
							return;
						}
												
						
						Log.i("GameSelectionFragment", "CONNECTING TO GAME");
						FragmentUtils.loadLoadingFragment(getActivity(), FragmentUtils.setupLoadingToLobby(getActivity(), game.getIp(), game.getPort()));
						
					}
				});
				
				newThread.start();
				
			}
		});
		
	}
		
	public void setupButtons(View view){
		setupCreateGamesButton(view);
		setupRefreshGameButton(view);
		setupJoinGameButton(view);
	}
		
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {    
		    
		View view = inflater.inflate(R.layout.fragment_game_selection, container, false);
		view.setBackgroundResource(R.drawable.background_lobby);
		this.inflater = inflater; 
		
		FragmentUtils.setCurrentFragment(this);
		
		setupGamesSpinner(view);
		setupButtons(view);
		MessageSender.send_GAMES_LIST();
		
		return view;
	}


	
	
	
	public synchronized ArrayList<String> getGames() {
		return games;
	}


	public synchronized void setGames(ArrayList<String> games) {
		this.games = new ArrayList<String>(games);		
	}
	    

}
