package ro.vadim.peoplewithguns.game.fragments;



import java.io.IOException;
import java.util.ArrayList;

import ro.vadim.peoplewithguns.R;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.game.gameelements.Profession;
import ro.vadim.peoplewithguns.game.gameelements.Weapon;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_ProfessionsAndWeapons;
import ro.vadim.peoplewithguns.game.utils.FragmentUtils;


import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class LobbySettingsFragment extends Fragment {
    
	private Activity parentActivity = null;
	
	private Button okButton = null;
	private Spinner professionSpinner = null;
	private EditText nameText = null;
	private EditText professionDetails = null;
	
	public ArrayList<String> professions = null;
	
	
	private void setupPlayerNameText(View view){
		
		nameText = (EditText)view.findViewById(R.id.text_name);
		nameText.setText(StaticData_Players.currentPlayer.getNickname());		
	}
	
	private void setupEditText(View view){
		
		professionDetails = (EditText) view.findViewById(R.id.text_profession_details);		
		
	}
	
	private void setupSpinner(View view){
		
		professions = new ArrayList<String>(StaticData_ProfessionsAndWeapons.professions.size());
		
		
		for(Profession p : StaticData_ProfessionsAndWeapons.professions.values()){			
			professions.add(p.title);
		}		
		
		professionSpinner = (Spinner) view.findViewById(R.id.spinner_professions);		
		
		final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_dropdown_item, professions);
				
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		professionSpinner.setAdapter(dataAdapter);
		
		
		int position = 0;
		String currentPlayerProfession = StaticData_Players.currentPlayer.getProfessionTitle();
		if(professions.contains(currentPlayerProfession))
			position = professions.indexOf(currentPlayerProfession);
		
				
		professionSpinner.setSelection(position);		
		professionSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if(professionDetails!=null){
					
					String professionTitle = professions.get(arg2);
					
					Profession profession = StaticData_ProfessionsAndWeapons.professions.get(professionTitle);
					
										
					professionDetails.setText(
							profession.description+'\n'+
							"Weapons : "+
							StaticData_ProfessionsAndWeapons.professions.get(professionTitle).weapons.toString()
					);
					
					
				}
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	
	
	
	
	
	
	private void setupOkButton(View view){
		
		final View myView = view;
		
		okButton = (Button) view.findViewById(R.id.button_ok);
		
		okButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				String newNickname = nameText.getText().toString();
				String newProfession = professionSpinner.getSelectedItem().toString();
				
				Log.println(Log.INFO, "CHOICES", "Nickname : "+newNickname);
				Log.println(Log.INFO, "CHOICES", "Profession : "+newProfession);
				
				StaticData_Players.currentPlayer.setNickname(newNickname);
				StaticData_Players.currentPlayer.changeProfession(newProfession);
				
				
				
				try {
					MessageSender.send_CHANGE_NAME(nameText.getText().toString());					
					MessageSender.send_CHANGE_PROFESSION(newProfession);
				} 
				
				catch (IOException e) {
					Toast toast = Toast.makeText(myView.getContext(), "Could not set personal data", Toast.LENGTH_SHORT);
					toast.show();
				}

				
				FragmentUtils.loadLobbyFragment(parentActivity);
								
			}
		});
		
		
	}
	
	
	
	
	
	
	public LobbySettingsFragment(){
		super();		
	}
	
	public void setParentActivity(Activity parentActivity){
		this.parentActivity = parentActivity;
	}
	
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	
    	View view = inflater.inflate(R.layout.fragment_lobby_config, container, false);
    	    	
    	view.setBackgroundColor(Color.WHITE);
    	
    	view.setBackgroundResource(R.drawable.background_lobby_settings4);
    	
    	FragmentUtils.setCurrentFragment(this);
    	FragmentUtils.setCurrentContext(view.getContext());
    	
    	
    	setupPlayerNameText(view);
    	setupOkButton(view);  
    	setupSpinner(view);
    	setupEditText(view);
    	
    	return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);        
    }
}