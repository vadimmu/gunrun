package ro.vadim.peoplewithguns.game.fragments;

import ro.vadim.peoplewithguns.R;
import ro.vadim.peoplewithguns.game.utils.AdminUtils;
import ro.vadim.peoplewithguns.game.utils.FragmentUtils;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html.ImageGetter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class TutorialsFragment extends Fragment{

	private static String URL_HOWTO_ABOUT = "file:///android_asset/tutorial_pages/about.html";
	private static String URL_HOWTO_MENU = "file:///android_asset/tutorial_pages/howto_menu.html";
	private static String URL_HOWTO_LOBBY = "file:///android_asset/tutorial_pages/howto_lobby.html";
	private static String URL_HOWTO_GAME = "file:///android_asset/tutorial_pages/howto_game.html";
	
	
	private Activity parentActivity = null;
	
	
	private Button howto_aboutButton = null;
	private Button howto_menuButton = null;
	private Button howto_lobbyButton = null;
	private Button howto_gameButton = null;
	private Button backButton = null;
	private ScrollView tutorial_container = null;
	private LayoutInflater layoutInflater = null;
	
	
	private View aboutView = null;
	private View menuView = null;
	private View lobbyView = null;
	private View gameView = null;
		
	
	
	private boolean ready = false;
	
	private String previousText = "";
	
	
	
	
	private void loadView(int resource){
		if(tutorial_container.getChildCount() > 0){
			tutorial_container.removeAllViews();			
		}
		
		View newView = layoutInflater.inflate(resource, tutorial_container);		
		
		//tutorial_container.addView(newView);
	}
	
	private void loadInfo_About(){
		loadView(R.layout.tutorial_about);
	}
	
	private void loadInfo_Menu(){
		loadView(R.layout.tutorial_menu);
					
				
	}
	
	private void loadInfo_Lobby(){
		loadView(R.layout.tutorial_lobby);		
	} 
	
	private void loadInfo_Game(){
		loadView(R.layout.tutorial_game);
	}
	
	
	private void prepareViews(LayoutInflater inflater){
		aboutView = inflater.inflate(R.layout.tutorial_about, tutorial_container);
		menuView = inflater.inflate(R.layout.tutorial_menu, tutorial_container);
		lobbyView = inflater.inflate(R.layout.tutorial_lobby, tutorial_container);
		gameView = inflater.inflate(R.layout.tutorial_game, tutorial_container);		
	}
	
	private void setupTutorialContainer(View view){		
		tutorial_container = (ScrollView) view.findViewById(R.id.tutorial_contents);		
	}
	
	private void setupAboutButton(View view){
		
		howto_aboutButton = (Button) view.findViewById(R.id.howto_about);
		
		final View myView = view;
		
		howto_aboutButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				loadInfo_About();				
			}
		});
	}
		
	private void setupMenuButton(final View view){
		howto_menuButton = (Button) view.findViewById(R.id.howto_menu);
		
		final View myView = view;
		
		howto_menuButton.setOnClickListener(new OnClickListener() {
			
			
			@Override
			public void onClick(View v) {			
				loadInfo_Menu();			    
			}
		});
	}
		
	private void setupLobbyButton(View view){
		howto_lobbyButton = (Button) view.findViewById(R.id.howto_lobby);
		final View myView = view;
		
		howto_lobbyButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {			
				loadInfo_Lobby();
			}
		});
	}
	
	private void setupGameButton(View view){
		howto_gameButton = (Button) view.findViewById(R.id.howto_game);
		final View myView = view;
		
		howto_gameButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				loadInfo_Game();
			}
		});
	}
	
	private void setupBackButton(View view){
		backButton = (Button) view.findViewById(R.id.tutorials_back);
		final View myView = view;
		
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				FragmentUtils.loadMainMenuFragment(FragmentUtils.currentActivity);
			}
		});
	}
		
	private void setupButtons(View view){
		setupAboutButton(view);
		setupBackButton(view);
		setupGameButton(view);
		setupLobbyButton(view);
		setupMenuButton(view);
		setupTutorialContainer(view);
	}
	
	public void setParentActivity(Activity parentActivity){
		this.parentActivity = parentActivity;
		
	}
		
	public TutorialsFragment(){
		super();
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
	}
	
	
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {    
    		    
	    View view = inflater.inflate(R.layout.tutorials, container, false);
	   
	    layoutInflater = inflater;	    
	    
	    FragmentUtils.setCurrentFragment(this);
	    FragmentUtils.setCurrentContext(view.getContext());
	    
	    parentActivity = this.getActivity();
	    
	    setupButtons(view);
	    setupTutorialContainer(view);
	    //prepareViews(inflater);
	    
	    return view;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Log.println(Log.INFO, "context ?", String.valueOf(parentActivity));
        
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }    
}
	
	

