package ro.vadim.peoplewithguns.game.misc;

import java.util.ArrayList;
import java.util.concurrent.Executor;

import ro.vadim.peoplewithguns.game.gameelements.Weapon;
import ro.vadim.peoplewithguns.game.utils.GameFragmentMapUtils;

import com.google.android.maps.MapView;

import android.os.SystemClock;
import android.text.format.Time;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class WeaponReportingTask extends AsyncTask<Weapon, Integer, Integer> {	
  
	
	private boolean hasFinished = true;
	
	Button pressedButton = null;
	String initialWeaponName = "";
	
	
	public WeaponReportingTask(Button newPressedButton){
		
		super();
		
		hasFinished = true;
		
		pressedButton = newPressedButton;
		
		initialWeaponName = pressedButton.getText().toString();
	}
	

	@Override
 	protected Integer doInBackground(Weapon... params) {
		
		
		hasFinished = false;
		
		Weapon weapon = params[0];
 		if(weapon.getLastUsed() == null) 
 			return 0;
 		
 		
 		int secondsElapsed = (int) GameFragmentMapUtils.getTimeDelta(weapon.getLastUsed(), true);
 		
 		while(secondsElapsed <= weapon.cooldown){
 			
 			publishProgress(weapon.cooldown - secondsElapsed);
 			
 			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
 			
 			secondsElapsed = (int) GameFragmentMapUtils.getTimeDelta(weapon.getLastUsed(), true);
 		}
 		
 		return 1;
 	}
     
    
    protected synchronized void onProgressUpdate(Integer... progress) {
    	
    	if(pressedButton.isEnabled())
    		pressedButton.setEnabled(false);
    	
    	if(progress != null)
    		    		
    		pressedButton.setText(initialWeaponName+"\n"+" ("+progress[0].toString()+" )");
    	
    	else 
    		pressedButton.setText(initialWeaponName);
    }

    
    @Override
    protected void onPostExecute(Integer result) {
    	
    	
    	pressedButton.setText(initialWeaponName);
    	pressedButton.setEnabled(true);
    	super.onPostExecute(result);
    	
    	hasFinished = true;
    	
    }
    
    @Override
    protected void onCancelled() {
    	
    	
    	pressedButton.setText(initialWeaponName);    	
    	super.onCancelled();
    	
    	hasFinished = true;
    }



	public boolean hasFinished() {
		return hasFinished;		
	}    
}
