package ro.vadim.peoplewithguns.game.misc;

import java.util.Map;
import java.util.UUID;

import ro.vadim.peoplewithguns.communication.messages.MessageHandler;
import android.util.Log;

public class AbilityCountdown implements Runnable{

	CountdownAction action = null;
	UUID playerUUID = null;
	
	
	public AbilityCountdown(UUID newPlayerUUID, CountdownAction newAction){		
		action = newAction;
		playerUUID = newPlayerUUID;
	}
	
	
	
	@Override
	public void run() {
		action.doDuringCountdown();
		action.doAfterCountdown();		
	}
	
	
	
	public interface CountdownAction {

		public void doDuringCountdown();
		public void doAfterCountdown();
		
	}
	
}
