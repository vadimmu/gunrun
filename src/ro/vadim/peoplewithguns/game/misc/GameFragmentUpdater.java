package ro.vadim.peoplewithguns.game.misc;

import ro.vadim.peoplewithguns.game.fragments.GameFragment;
import ro.vadim.peoplewithguns.game.gameelements.Player;
import ro.vadim.peoplewithguns.game.gameelements.Weapon;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;
import ro.vadim.peoplewithguns.game.utils.FragmentUtils;
import ro.vadim.peoplewithguns.game.utils.GameFragmentButtonsUtils;
import ro.vadim.peoplewithguns.game.utils.MapUtils;
import android.util.Log;
import android.widget.Button;

public class GameFragmentUpdater extends AsyncTask<Void, Void, Void>{

	private boolean hasFinished = true;
	
	
	
	@Override
	protected Void doInBackground(Void... params) {
		
		hasFinished = false;
		Log.println(Log.INFO, "Updating GameFragment elements", "...");
		
		publishProgress();
		
		return null;
	}
     
    
	
	private void updateHealthTextView(){
		
		if(StaticData_Players.currentPlayer != null){
			
			Player currentPlayer = StaticData_Players.currentPlayer;
			
			if(currentPlayer.getHealth() <= 0){
				GameFragment.myHealthTextView.setText("DEAD...");				
			}
			else{
				GameFragment.myHealthTextView.setText("HEALTH : " + currentPlayer.getHealth());
			}
    	}
	}
	
	
    protected synchronized void onProgressUpdate(Void... progress) {
    	
    	
    }

    
    @Override
    protected void onPostExecute(Void result) {
    	
    	super.onPostExecute(result);
    	
    	updateHealthTextView();
    	 	
    	
    	if(GameFragment.textForToast != null){    		
    		FragmentUtils.showText(GameFragment.mapContainer.getContext(), GameFragment.textForToast);    		
    		GameFragment.textForToast = null;
    	}
    	
    	if(GameFragment.alertMessage != null){    		
    		MapUtils.buildAlertMessage(GameFragment.mapContainer.getContext(), GameFragment.alertMessage);
    		GameFragment.alertMessage = null;
    	}
    	
    	if(GameFragment.enableWeapons != null){
    		
    		if(GameFragmentButtonsUtils.buttons != null){
    			for(Button b : GameFragmentButtonsUtils.buttons){
    				b.setEnabled(GameFragment.enableWeapons);
    			}
    		}
    		
    		GameFragment.enableWeapons = null;
    	}
    	
    	hasFinished = true;
    	
    }
    
    @Override
    protected void onCancelled() {    	    	
    	super.onCancelled();
    	hasFinished = true;
    }

	public boolean hasFinished() {
		return hasFinished;		
	}    
	
	
	
	
	
	
}
