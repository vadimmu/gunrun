package ro.vadim.peoplewithguns.game.misc;

import java.net.InetAddress;
import java.util.UUID;

public class Game {
	
	private String name = null;
	
	private String ip = null;
	private Integer port = null;
	private UUID gameID = null;
	private UUID creatorID = null;
	
	private Integer countAwayTeam = null;
	private Integer countHomeTeam = null;
	
	private Boolean inProgress = null;

	
	
	public Game(
			String newName, 
			String newIp, 
			Integer newPort, 
			UUID newGameID, 
			UUID newCreatorID,
			Integer newCountAwayTeam,
			Integer newCountHomeTeam,
			Boolean newInProgress){
		
		setName(newName);
		setIp(newIp);
		setPort(newPort);
		setGameID(newGameID);
		setCreatorID(newCreatorID);
		setCountAwayTeam(newCountAwayTeam);
		setCountHomeTeam(newCountHomeTeam);
		setInProgress(newInProgress);
		
	}
	
	
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public UUID getGameID() {
		return gameID;
	}

	public void setGameID(UUID gameID) {
		this.gameID = gameID;
	}

	public UUID getCreatorID() {
		return creatorID;
	}

	public void setCreatorID(UUID creatorID) {
		this.creatorID = creatorID;
	}

	public Integer getCountAwayTeam() {
		return countAwayTeam;
	}

	public void setCountAwayTeam(Integer countAwayTeam) {
		this.countAwayTeam = countAwayTeam;
	}

	public Integer getCountHomeTeam() {
		return countHomeTeam;
	}

	public void setCountHomeTeam(Integer countHomeTeam) {
		this.countHomeTeam = countHomeTeam;
	}
	
	public Boolean getInProgress() {
		return inProgress;
	}
	
	public void setInProgress(Boolean inProgress) {
		this.inProgress = inProgress;
	}
	
}
