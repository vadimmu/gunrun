package ro.vadim.peoplewithguns.game.misc;

import java.util.UUID;

import ro.vadim.peoplewithguns.game.fragments.GameFragment;
import ro.vadim.peoplewithguns.game.gameelements.Player;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;
import ro.vadim.peoplewithguns.game.utils.FragmentUtils;
import ro.vadim.peoplewithguns.game.utils.GameFragmentButtonsUtils;
import ro.vadim.peoplewithguns.game.utils.GameFragmentUtils;
import ro.vadim.peoplewithguns.game.utils.MapUtils;
import android.widget.Button;

public class GameFragmentUpdater2 implements Runnable{

	
	
	private synchronized void updateHealthTextView(){
		
		if(StaticData_Players.currentPlayer != null){
			
			Player currentPlayer = StaticData_Players.currentPlayer;
			
			if(currentPlayer.getHealth() <= 0){
				GameFragment.myHealthTextView.setText("DEAD...");
				
				if(GameFragmentUtils.isGameFragmentOn() && (FragmentUtils.currentActivity != null)){					
					MapUtils.buildGameOverAlertMessage(GameFragment.mapContainer.getContext(), FragmentUtils.currentActivity);					
				}
			}
			
			else{
				GameFragment.myHealthTextView.setText("HEALTH : " + currentPlayer.getHealth());
			}
    	}
	}
	
	
	private synchronized void updateSelectedPlayerMarker(){
		
		if(GameFragmentUtils.isGoogleMapAvailable()){
			
			UUID selectedPlayerUUID = StaticData_Players.selectedPlayerUUID; 
			
			if(selectedPlayerUUID != null){
				GameFragmentUtils.redrawPlayer(selectedPlayerUUID);
			}
		}
	}
	
	
	@Override
	public void run() {
		
		updateHealthTextView();    	 	
    	updateSelectedPlayerMarker();
		
    	if(GameFragment.textForToast != null){
    		    		
    		
    		
    		FragmentUtils.showText(GameFragment.mapContainer.getContext(), GameFragment.textForToast);    		
    		GameFragment.textForToast = null;
    	}
    	
    	if(GameFragment.alertMessage != null){    		
    		MapUtils.buildAlertMessage(GameFragment.mapContainer.getContext(), GameFragment.alertMessage);
    		GameFragment.alertMessage = null;
    	}
    	
    	if(GameFragment.enableWeapons != null){
    		
    		if(GameFragmentButtonsUtils.buttons != null){
    			for(Button b : GameFragmentButtonsUtils.buttons){
    				b.setEnabled(GameFragment.enableWeapons);
    			}
    		}
    		
    		GameFragment.enableWeapons = null;
    	}
	}
}
