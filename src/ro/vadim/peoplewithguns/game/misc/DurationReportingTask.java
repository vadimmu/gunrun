package ro.vadim.peoplewithguns.game.misc;

import java.util.ArrayList;
import java.util.concurrent.Executor;

import ro.vadim.peoplewithguns.game.fragments.GameFragment;
import ro.vadim.peoplewithguns.game.gameelements.Weapon;
import ro.vadim.peoplewithguns.game.utils.FragmentUtils;
import ro.vadim.peoplewithguns.game.utils.GameFragmentMapUtils;

import com.google.android.maps.MapView;

import android.os.SystemClock;
import android.text.format.Time;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class DurationReportingTask extends AsyncTask<Weapon, Integer, Integer> {	
  
	
	private boolean hasFinished = true;
	
	TextView pressedButton = null;
	String initialWeaponName = "";
	
	
	public DurationReportingTask(TextView newPressedButton){
		
		super();
		
		hasFinished = true;
		
		pressedButton = newPressedButton;
		
		initialWeaponName = pressedButton.getText().toString();
	}
	

	@Override
 	protected Integer doInBackground(Weapon... params) {
		
		
		hasFinished = false;
		
		Weapon weapon = params[0];
 		 		
 		FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				GameFragment.powerupsLayout.addView(pressedButton);
				
			}
		});
 		
 		Log.println(Log.INFO, "Started weapon duration counter", "Weapon : " + weapon.name);
 		
 		double secondsElapsed = 0;
 		
 		while(secondsElapsed <= weapon.duration){
 			
 			publishProgress(weapon.duration - (int)secondsElapsed);
 			
 			try {
				Thread.sleep(200);
				secondsElapsed += 0.2;
			}
 			
 			catch (InterruptedException e) {
				Log.println(Log.ERROR, "DurationReportingTask.doInBackground failed !", e.toString());
			}
 		}
 		
 		return 1;
 	}
     
    
    protected synchronized void onProgressUpdate(Integer... progress) {
    	
    	if(pressedButton.isEnabled())
    		pressedButton.setEnabled(false);
    	
    	if(progress != null)
    		    		
    		pressedButton.setText(" " + initialWeaponName.split(" ")[0] + " ("+progress[0].toString()+" )" + " ");
    	
    	else 
    		pressedButton.setText(initialWeaponName);
    }

    
    @Override
    protected void onPostExecute(Integer result) {
    	
    	
    	GameFragment.powerupsLayout.removeView(pressedButton);
    	super.onPostExecute(result);
    	
    	hasFinished = true;
    	
    }
    
    @Override
    protected void onCancelled() {    	
    	
    	GameFragment.powerupsLayout.removeView(pressedButton);    	
    	super.onCancelled();
    	
    	hasFinished = true;
    }



	public boolean hasFinished() {
		return hasFinished;		
	}    
}
