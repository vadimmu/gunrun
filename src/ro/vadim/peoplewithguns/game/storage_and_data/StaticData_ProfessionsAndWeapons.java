package ro.vadim.peoplewithguns.game.storage_and_data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import ro.vadim.peoplewithguns.game.gameelements.Player;
import ro.vadim.peoplewithguns.game.gameelements.Profession;
import ro.vadim.peoplewithguns.game.gameelements.Weapon;
import ro.vadim.peoplewithguns.game.misc.AbilityCountdown;
import ro.vadim.peoplewithguns.game.misc.AbilityCountdown.CountdownAction;
import ro.vadim.peoplewithguns.game.utils.GameFragmentDrawingUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import android.util.Log;

public class StaticData_ProfessionsAndWeapons {

	public static final int NUMBER_OF_PROFESSIONS = 10;
	public static final int NUMBER_OF_WEAPONS = 30;
	
	
	public static HashMap<String, Profession> professions = null;
	public static HashMap<String, Weapon> weapons = null;
	
	
	ObjectMapper mapper = new ObjectMapper();
	
	public static void populateProfessions(Object jsonData){
		
		if(professions == null)
			professions = new HashMap<String, Profession>(NUMBER_OF_PROFESSIONS);
		
		Log.println(Log.INFO, "POPULATE PROFESSIONS", "...");
		
		for(Object profession : (ArrayList<Object>) jsonData){
				
			Map<String, Object> p = (Map<String, Object>) profession;
			
			ArrayList<String> weapons = new ArrayList<String> ( (ArrayList<String>)p.get("weapons") );			
			int health = Integer.valueOf(String.valueOf(p.get("health")));
			String title = String.valueOf(p.get("title"));
			String description = String.valueOf(p.get("description"));
			
			professions.put(title, new Profession(weapons, health, title, description));
		}
	}
	
	public static void populateWeapons(Object jsonData){
		
		if(weapons == null)
			weapons = new HashMap<String, Weapon>(NUMBER_OF_WEAPONS);
		
		Log.println(Log.INFO, "POPULATE WEAPONS", "...");
		
		for(Object weapon : (ArrayList<Object>) jsonData){
			
			
			Map<String, Object> w = (Map<String, Object>) weapon;
			
			
			String name = (String) w.get("name");
			int range = Integer.valueOf ( 
					String.valueOf(w.get("range")));
			int damage = Integer.valueOf (
					String.valueOf(w.get("damage")));
			int cooldown = Integer.valueOf (
					String.valueOf(w.get("cooldown")));
			int duration = Integer.valueOf (
					String.valueOf(w.get("duration")));
			int policy = Integer.valueOf (
					String.valueOf(w.get("policy")));
			String description = (String) w.get("description");
			
			weapons.put(name, new Weapon(name, range, damage, cooldown, duration, policy, description));
		}
		
	}
	
	public static void getConfig(Map<String, Object> jsonMessage){
		
		Log.println(Log.INFO, "POPULATING PROFESSIONS AND WEAPONS", "...");
		
		Log.println(Log.INFO, "PROFESSIONS CLASS TYPE", jsonMessage.get("professions").getClass().toString());
		Log.println(Log.INFO, "WEAPONS CLASS TYPE", jsonMessage.get("weapons").getClass().toString());
		
		populateProfessions(jsonMessage.get("professions"));
		populateWeapons(jsonMessage.get("weapons"));
		
		Log.println(Log.INFO, "NUMBER OF WEAPONS", String.valueOf(weapons.size()));
		Log.println(Log.INFO, "NUMBER OF PROFESSIONS", String.valueOf(professions.size()));
	}
	
	public static AbilityCountdown setInvisiblityCountdown(final UUID playerUUID){
		
		AbilityCountdown invisibilityCountdown = new AbilityCountdown(playerUUID, new CountdownAction() {
			
			@Override
			public void doDuringCountdown() {
				
				Player player = StaticData_Players.getPlayerById(playerUUID);
				if(player != null)
					player.isInvisible = true;
				
				GameFragmentDrawingUtils.redrawPlayer(playerUUID);
								
				try {
					Thread.sleep(1000 * StaticData_ProfessionsAndWeapons.weapons.get("Invisibility Cloak").duration);
				} 
				
				catch (InterruptedException e) {
					Log.println(Log.ERROR, "Ability Countdown Thread.sleep error", e.toString());
				}
			}
			
			@Override
			public void doAfterCountdown() {
				
				Player player = StaticData_Players.getPlayerById(playerUUID);
				if(player != null)
					player.isInvisible = false;
				
				GameFragmentDrawingUtils.redrawPlayer(playerUUID);
			}
		});
		
		return invisibilityCountdown;		
	}
	
	public static AbilityCountdown setShieldCountdown(final UUID playerUUID){
		
		AbilityCountdown shieldCountdown = new AbilityCountdown(playerUUID, new CountdownAction() {
			
			@Override
			public void doDuringCountdown() {
				
				StaticData_Players.getPlayerById(playerUUID).isShielded = true;
				
				try {
					Thread.sleep(1000 * StaticData_ProfessionsAndWeapons.weapons.get("Shield").duration);
				} 
				
				catch (InterruptedException e) {
					Log.println(Log.ERROR, "Ability Countdown Thread.sleep error", e.toString());
				}
			}
			
			@Override
			public void doAfterCountdown() {
				
				Player player = StaticData_Players.getPlayerById(playerUUID);
				if(player != null)				
					player.isShielded = false;
			}
		});
		
		return shieldCountdown;		
	}
	
	
	
}
