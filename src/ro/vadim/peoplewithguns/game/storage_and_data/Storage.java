package ro.vadim.peoplewithguns.game.storage_and_data;

import java.util.HashMap;
import java.util.UUID;

import ro.vadim.peoplewithguns.R;
import ro.vadim.peoplewithguns.communication.CommunicationUtils;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class Storage {

	public static void saveCurrentPlayerData(Context context){
		
		
		Log.i("Storage", "SAVING CURRENT PLAYER DATA");
		
		SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.save_file_key_lobby), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("currentPlayerName", StaticData_Players.currentPlayer.getNickname());
		editor.putString("currentPlayerProfessionTitle", StaticData_Players.currentPlayer.getProfessionTitle());
		editor.commit();
		
		Log.i("Storage", StaticData_Players.currentPlayer.getNickname() + "  (" + StaticData_Players.currentPlayer.getProfessionTitle()+")" );
		
	}
	
	
	public static HashMap<String, String> loadCurrentPlayerData(Context context){
		
		Log.i("Storage", "LOADING CURRENT PLAYER DATA");
		
		SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.save_file_key_lobby), Context.MODE_PRIVATE);
		HashMap<String, String> playerData = new HashMap<String, String>(10);
				
		playerData.put("currentPlayerName", sharedPref.getString("currentPlayerName", "Player"));		
		playerData.put("currentPlayerProfessionTitle", sharedPref.getString("currentPlayerProfessionTitle", StaticData_ProfessionsAndWeapons.professions.get("Marine").title));
		
		Log.i("Storage", playerData.get("currentPlayerName")+" ("+playerData.get("currentPlayerProfessionTitle")+")");
		
		return playerData;
	}
	
	
	public static void saveServerData(Context context){
		
		Log.i("Storage", "SAVING SERVER IP AND PORT");
		SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.save_file_key_main_menu_settings), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("serverIP", CommunicationUtils.defaultServerAddress);
		editor.putInt("serverPort", CommunicationUtils.defaultServerPort);		
		editor.commit();
	}
	
	public static void loadServerData(Context context){
		Log.i("Storage", "LOADING SERVER IP AND PORT");
		
		SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.save_file_key_main_menu_settings), Context.MODE_PRIVATE);
		
		if((sharedPref.contains("serverIP"))&&(sharedPref.contains("serverPort"))){		
			CommunicationUtils.defaultServerAddress = sharedPref.getString("serverIP", "87.106.29.46");
			CommunicationUtils.defaultServerPort = sharedPref.getInt("serverPort", 9001);
		}
	}
	
	
	public static void savePlayerIdData(Context context){
		
		if(StaticData_Players.currentPlayerUUID != null){		
			Log.i("Storage", "SAVING PLAYER ID");
			SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.save_file_key_player_id), Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPref.edit();		
			editor.putString("playerID", StaticData_Players.currentPlayerUUID.toString());				
			editor.commit();
		}
	}
	
	public static void loadPlayerIdData(Context context){
		Log.i("Storage", "LOADING PLAYER ID");
		
		SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.save_file_key_player_id), Context.MODE_PRIVATE);
		if(sharedPref.contains("playerID"))
			StaticData_Players.currentPlayerUUID = UUID.fromString(sharedPref.getString("playerID", null));
	}
	
	
	
	
	
}
