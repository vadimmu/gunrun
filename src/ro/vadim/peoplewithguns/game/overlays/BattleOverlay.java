package ro.vadim.peoplewithguns.game.overlays;


import com.vadim.experiments.game.R;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import ro.vadim.peoplewithguns.game.fragments.GameFragment;
import ro.vadim.peoplewithguns.game.gameelements.Player;
import ro.vadim.peoplewithguns.game.gameelements.Weapon;
import ro.vadim.peoplewithguns.game.staticdata.StaticData_Players;
import ro.vadim.peoplewithguns.game.utils.FragmentUtils;
import ro.vadim.peoplewithguns.game.utils.MapUtils;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.google.android.maps.Projection;


public class BattleOverlay extends Overlay{

	
	private HashMap<UUID, Point> drawnPoints = new HashMap<UUID, Point>(2*StaticData_Players.maxPlayersPerTeam);
	
	public static final int BITSET_DEFAULT = 0;
	
	public static Drawable marker_currentPlayer = null;
	public static Drawable marker_friend = null;
	public static Drawable marker_enemy = null;
	public static Drawable marker_selected = null;
			
	private Context context = null;
	private MapView mapView = null;
	
	
		
	public BattleOverlay(MapView givenMapView){
		super();
		Log.println(Log.INFO, "BattleOverlay constructor has been called !", "CALLED !");
		this.mapView = givenMapView;
		this.context = mapView.getContext();
			
	}
	
	
	/*
	 * Va retine intr-un HashMap combinatia <UUID, Point>. 
	 * Acest HashMap va fi folosit pentru a identifica punctele apropiate de touch
	 * */
	private void drawItem(Player player, UUID uuid, Canvas canvas, int playerStatus){
		
		if(player.getOverlayItem() != null){						
			OverlayItem item = player.getOverlayItem();
			String playerName = player.getNickname();
			String playerOccupation = player.getProfessionTitle();
			
			Point point = new Point();
			mapView.getProjection().toPixels(item.getPoint(), point);
			
			
			int markerID = 0;
			switch (playerStatus) {
			case Player.STATUS_SELECTED:
				markerID = R.drawable.marker_selected;
				break;
			case Player.STATUS_CURRENT_PLAYER:
				markerID = R.drawable.marker_current_player;
				break;
			case Player.STATUS_FRIEND:
				markerID = R.drawable.marker_friend;
				break;
			case Player.STATUS_ENEMY:
				markerID = R.drawable.marker_enemy;
				break;
			default:
				break;
			}
			
	        Bitmap bmp = BitmapFactory.decodeResource(mapView.getResources(), markerID);
	        	        
	        int x = point.x - bmp.getWidth() / 2;
	        int y = point.y - bmp.getHeight();
	        	        
	        //adds the point in the drawnPoints HashMap
	        Point pointToHashMap = new Point(point.x, point.y);
	        drawnPoints.put(uuid, pointToHashMap);
	        
	        if(! player.isInvisible){
	        
	        	//draw the health bar
	        	
	        	
		        //draw the name and occupation
		        Paint paint = new Paint();
		        paint.setColor(Color.BLACK);
		        paint.setTextSize(25);	        
		        canvas.drawText(playerName+"("+playerOccupation.substring(0, 3)+") [HEALTH : "+ String.valueOf(player.getHealth()) + " ]", x - bmp.getWidth()/2, y, paint);
		        
		        //draw the actual marker
		        
		        canvas.drawBitmap(bmp, x, y, null);
		        
	        }
		}
	}
	
	
	
	/* The radius is expressed in meters */
	public static void drawWeaponRange(GeoPoint playerPosition, int weaponRangeRadius, MapView destinationMapView, Canvas overlayCanvas){
		float mapRadius = MapUtils.convertRadiusToMapPixelRadius(playerPosition, weaponRangeRadius, destinationMapView);
		Point mapPoint = MapUtils.convertGeoPointToScreenPoint(playerPosition, destinationMapView);
		MapUtils.drawRangeOnMap((int)mapRadius, mapPoint, overlayCanvas);
	}
	
	private void drawCurrentPlayer(Canvas canvas){
		Player me = StaticData_Players.currentPlayer;
		int mySelectedWeaponIndex = StaticData_Players.currentPlayer.getSelectedWeaponIndex();
		
		
		if(StaticData_Players.currentPlayerUUID.equals(StaticData_Players.selectedPlayerUUID))
			drawItem(StaticData_Players.currentPlayer, StaticData_Players.currentPlayerUUID, canvas, Player.STATUS_SELECTED);
		else
			drawItem(StaticData_Players.currentPlayer, StaticData_Players.currentPlayerUUID, canvas, Player.STATUS_CURRENT_PLAYER);
		
		Weapon selectedWeapon = me.getWeapons().get(mySelectedWeaponIndex);
		BattleOverlay.drawWeaponRange(StaticData_Players.currentPlayer.getOverlayItem().getPoint(), selectedWeapon.range, mapView, canvas);				
	}	
	
	private void drawPlayersFromHashMap(HashMap<UUID, Player> hashMap, Canvas canvas, int playerStatus){
				
		Iterator iter = null;
		
		if(hashMap.size() > 0){
						
			iter = hashMap.entrySet().iterator();
			
			while(iter.hasNext()){
				
				Entry<UUID, Player> entry = (Entry<UUID, Player>) iter.next();
				Player player = entry.getValue();
				UUID playerUUID = entry.getKey();
				

				if(playerUUID.equals(StaticData_Players.selectedPlayerUUID)) 
					drawItem(player,playerUUID, canvas, Player.STATUS_SELECTED);		
				
				else
					drawItem(player,playerUUID, canvas, playerStatus);
				
			}
		}
	}
	
	private void drawFriends(Canvas canvas){
		drawPlayersFromHashMap(StaticData_Players.friends, canvas, Player.STATUS_FRIEND);		
	}
	
	private void drawEnemies(Canvas canvas){
		drawPlayersFromHashMap(StaticData_Players.enemies, canvas, Player.STATUS_ENEMY);		
	}
	
	private void drawPlayers(Canvas canvas){
		drawnPoints.clear();
		drawCurrentPlayer(canvas);
		drawFriends(canvas);
		drawEnemies(canvas);
	}
	
	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		// TODO Auto-generated method stub
		super.draw(canvas, mapView, shadow);
		if(!shadow){
			
			drawPlayers(canvas);
				
		}
	}
	
	
	
	public void selectPlayer(GeoPoint tapPoint){
		
		
		UUID closestUUID = getClosestPlayerUUIDWithinTapRadius(tapPoint, null);
		
		StaticData_Players.selectedPlayerUUID = applyCurrentPlayerSelectionPolicy(closestUUID);
		
	}
	
	
	@Override
	public boolean onTap(GeoPoint p, MapView mapView) {
		
		selectPlayer(p);		
		Player closestPlayer = StaticData_Players.getPlayerById(StaticData_Players.selectedPlayerUUID);
		
		if(closestPlayer != null){
			String weapons = "";
			for(Weapon w : closestPlayer.getWeapons()){
				weapons+=w.name+"("+ String.valueOf(w.range)+") ";
			}
			FragmentUtils.showText(mapView.getContext(), "Player : "+closestPlayer.getNickname()+" Weapons : " + weapons + " Position : "+closestPlayer.getPosition().toString());
		}
		else
			FragmentUtils.showText(mapView.getContext(), "Closest UUID : null");
		
		return super.onTap(p, mapView);
	}
	
	
	
	
	private UUID applyCurrentPlayerSelectionPolicy(UUID closestPointUUID){
		
		int selectedWeaponIndex = StaticData_Players.currentPlayer.getSelectedWeaponIndex();
		Weapon selectedWeapon = StaticData_Players.currentPlayer.getWeapons().get(selectedWeaponIndex);
		
		int playerSelectionPolicy = selectedWeapon.policy;
		
		boolean returnClosestPointUUID = true;
		
		switch (playerSelectionPolicy) {
		case Weapon.POLICY_SELF:
			
			if(!StaticData_Players.playerIsMe(closestPointUUID))
				returnClosestPointUUID = false;
				
			break;
		
		case Weapon.POLICY_FRIENDS_AND_SELF:
			if((!StaticData_Players.playerIsMe(closestPointUUID))&&(!StaticData_Players.playerIsFriend(closestPointUUID)))
				returnClosestPointUUID = false;
			break;
			
		case Weapon.POLICY_FRIENDS:
			if(!StaticData_Players.playerIsFriend(closestPointUUID))
				returnClosestPointUUID = false;
			break;
			
		case Weapon.POLICY_ENEMIES:
			
			if(!StaticData_Players.playerIsEnemy(closestPointUUID))
				returnClosestPointUUID = false;
			break;
			
		case Weapon.POLICY_FRIENDS_AND_ENEMIES:
			if((!StaticData_Players.playerIsEnemy(closestPointUUID))&&(!StaticData_Players.playerIsFriend(closestPointUUID)))
				returnClosestPointUUID = false;
			break;
		
		case Weapon.POLICY_FRIENDS_AND_ENEMIES_AND_SELF:
			if((!StaticData_Players.playerIsMe(closestPointUUID))&&(!StaticData_Players.playerIsFriend(closestPointUUID))&&(!StaticData_Players.playerIsEnemy(closestPointUUID)))
				returnClosestPointUUID = false;
			break;

		default:
			returnClosestPointUUID = true;
			break;
		}
		
		
		if(returnClosestPointUUID)
			return closestPointUUID;
		
		return null;
	}
	
	private double calculateMaximumAcceptedDistance(double touchRadius){
		DisplayMetrics dm = context.getResources().getDisplayMetrics();
		int densityDpi = dm.densityDpi;
		
		double averageDpi = (dm.xdpi + dm.ydpi)/2;
		return averageDpi * touchRadius; // expressed in pixels		
	}
	
	private UUID getClosestPlayerToTapPosition(GeoPoint geoPoint){				
		return getClosestPlayerUUIDWithinTapRadius(geoPoint, Double.MAX_VALUE);
	}
	
	private UUID getClosestPlayerUUIDWithinTapRadius(GeoPoint geoPoint, Double newTouchRadius){
		
		double touchRadius = 0.2; //expressed in inches
		
		if(newTouchRadius != null) touchRadius = newTouchRadius;		 
				
		double maximumAcceptedDistance = calculateMaximumAcceptedDistance(touchRadius);
		
		Point touchScreenPoint = MapUtils.convertGeoPointToScreenPoint(geoPoint, mapView);
		
		//get the closest point UUID and the closest distance
		UUID closestPointUUID = null;
		double closestPointDistance = Double.MAX_VALUE;
			
		
		Iterator iter = drawnPoints.entrySet().iterator();
		while(iter.hasNext()){			
			Entry<UUID, Point> entry = (Entry<UUID, Point>) iter.next();
			
			UUID uuid = entry.getKey();
			Point point = entry.getValue();
			
			double distance = MapUtils.getDistanceBetweenPoints(point, touchScreenPoint);
			
			if(distance < closestPointDistance){
				closestPointDistance = distance;
				closestPointUUID = uuid;		
			}
		}
		
		if(touchRadius == Double.MAX_VALUE) 
			return closestPointUUID;
		
		else if(closestPointDistance <= maximumAcceptedDistance){
			return closestPointUUID;
		}
		
		
		return null;
		
	}
	
	
}
