package ro.vadim.peoplewithguns.game.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout.LayoutParams;

import ro.vadim.peoplewithguns.R;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.game.fragments.GameFragment;
import ro.vadim.peoplewithguns.game.gameelements.Player;
import ro.vadim.peoplewithguns.game.gameelements.Weapon;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;

public class GameFragmentDrawingUtils {
	
	private static final BitmapDescriptor ICON_SELECTED = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);
	private static final BitmapDescriptor ICON_FRIEND = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
	private static final BitmapDescriptor ICON_ENEMY = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
	private static final BitmapDescriptor ICON_DEAD = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET);
	
	public static GameFragmentPeriodicRefreshRunnable gameFragmentScreenRefresher = null;
	public static LinkedHashMap<UUID, Marker> playerMarkers = null;
	
	
	private static AtomicInteger sNextGeneratedId = new AtomicInteger(1);
		
	public static int generateViewId() {
		
	    for (;;) {
	        final int result = sNextGeneratedId.get();
	        // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
	        int newValue = result + 1;
	        if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
	        if (sNextGeneratedId.compareAndSet(result, newValue)) {
	            return result;
	        }
	    }
	}
		
	
	
	
	
	public static boolean isGameFragmentOn(){
		if(FragmentUtils.currentFragment.getClass().equals(GameFragment.class)){
			Log.println(Log.INFO, "isGameFragmentOn : GAME FRAGMENT OK !", "...");
			return true;
		}
		
		Log.println(Log.INFO, "isGameFragmentOn : GAME FRAGMENT NOT AVAILABLE !", "...");
		return false;
	}
	
	public static boolean isGoogleMapAvailable(){
		if(!isGameFragmentOn())
			return false;
		
		if(GameFragment.googleMap == null){
			Log.println(Log.INFO, "isGoogleMapAvailable : GOOGLE MAP NOT AVAILABLE !", "...");
			return false;
		}
		
		Log.println(Log.INFO, "isGoogleMapAvailable : GOOGLE MAP OK !", "...");
		return true;
	}
		
		
	
	
	
	private static void startGameFragmentPeriodicRefreshRunnable(){
		if(gameFragmentScreenRefresher == null)
			gameFragmentScreenRefresher = new GameFragmentPeriodicRefreshRunnable();
		
		Thread refresherThread = new Thread(gameFragmentScreenRefresher);
		refresherThread.start();
	}
	
	
	
	
	
	public static LatLng getLastKnownPosition(){
		LocationManager locationManager = (LocationManager) FragmentUtils.currentActivity.getSystemService(Context.LOCATION_SERVICE);
		Location gpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		Location networkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		
		if((gpsLocation == null)&&(networkLocation == null))
			return new LatLng(0,0);		
		
		else if((gpsLocation != null)&&(networkLocation == null))
			return new LatLng(gpsLocation.getLatitude(), gpsLocation.getLongitude());
		
		else if((gpsLocation == null)&&(networkLocation != null))
			return new LatLng(networkLocation.getLatitude(), networkLocation.getLongitude());
		
		else{
			if(gpsLocation.getAccuracy() > networkLocation.getAccuracy())
				return new LatLng(gpsLocation.getLatitude(), gpsLocation.getLongitude());
			else
				return new LatLng(networkLocation.getLatitude(), networkLocation.getLongitude());
		}
	}
	
	public static void setupGoogleMap(GoogleMap googleMap){

		
		if(GameFragmentDrawingUtils.playerMarkers != null){
			GameFragmentDrawingUtils.playerMarkers.clear();
			GameFragmentDrawingUtils.playerMarkers = null;
		}
		
		googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		googleMap.setMyLocationEnabled(true);
		
		LatLng myPosition = getLastKnownPosition();
		
		googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 15.0f));
		
		googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			
			@Override
			public boolean onMarkerClick(Marker arg0) {
				
				manageOnMarkerClick(arg0);
				
				return false;
			}
		});
		
		googleMap.setOnMapClickListener(new OnMapClickListener() {
			
			@Override
			public void onMapClick(LatLng arg0) {
				//TODO deselect player here (modify marker color to original color, set selected player = null) 
				
				manageOnMapClick(arg0);
				
			}
		});
				
		googleMap.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {
			
			@Override
			public void onMyLocationChange(Location arg0) {
								
				if(!StaticData_Players.currentPlayer.getPosition().equals(arg0)){
					
					LatLng previousPosition = StaticData_Players.currentPlayer.getPosition();
					
					StaticData_Players.currentPlayer.setPosition(arg0.getLatitude(), arg0.getLongitude());					
					redrawWeaponRange();
					
					double distance = GameFragmentMapUtils.getDistanceBetweenPositions(previousPosition, StaticData_Players.currentPlayer.getPosition());
					
					if(distance > 2)
						MessageSender.send_CHANGE_POSITION(arg0.getLatitude(), arg0.getLongitude());
				}
				
			}
		});
		
		
		UiSettings mapSettings = googleMap.getUiSettings();		
		mapSettings.setMyLocationButtonEnabled(true);
		mapSettings.setZoomControlsEnabled(false);
		mapSettings.setTiltGesturesEnabled(false);
		mapSettings.setRotateGesturesEnabled(false);
		mapSettings.setCompassEnabled(false);
		
		//startRefreshRunnable();
		
	}
		
	public static UUID getUUIDbyMarker(Marker playerMarker){
		
		for(Entry<UUID, Marker> e : playerMarkers.entrySet()){
			
			if(e.getValue().equals(playerMarker)){
				return e.getKey(); 				
			}
		}
		
		return null;
	}
	
	
	
	
	
	public static synchronized void removePlayerMarker_unsafe(UUID playerID){
		if(playerMarkers != null){
			if(playerMarkers.containsKey(playerID)){
				playerMarkers.get(playerID).remove();
				playerMarkers.remove(playerID);
			}
		}		
	}	
		
	public static void removePlayerMarker(final UUID playerID){
		if(GameFragmentDrawingUtils.isGoogleMapAvailable()){
			
			Log.println(Log.INFO, "REMOVING PLAYER MARKER", playerID.toString());
			
			if(!FragmentUtils.isRunningOnUiThread()){
				FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						removePlayerMarker_unsafe(playerID);					
					}
				});
			}
			
			else{
				removePlayerMarker_unsafe(playerID);
			}
		}
	}
	
	public static synchronized Marker addPlayerMarker(GoogleMap googleMap, BitmapDescriptor markerIcon, Player player){
						
		Marker newMarker = googleMap.addMarker(new MarkerOptions()
								.position(player.getPosition())
								.title(player.getNickname()+" ("+player.getProfessionTitle().substring(0, 3)+") ")
								.snippet(String.valueOf(player.getHealth()))
								.icon(markerIcon)
								.draggable(false)											
		);
		
		return newMarker;
	}
	
	public static synchronized void redrawPlayer_unsafe(GoogleMap googleMap, UUID playerID, boolean isFriend){
				
		if(StaticData_Players.playerIsMe(playerID)){
			return;
		}
		
		Player player = StaticData_Players.getPlayerById(playerID);
		
		if(player == null){
			Log.println(Log.INFO, "Selecting player", "null");
			return;
		}
		
		if(playerMarkers == null)
			return;
		
		removePlayerMarker(playerID);
		
		double distanceToTarget = GameFragmentMapUtils.getDistanceBetweenPositions(player.getPosition(), StaticData_Players.currentPlayer.getPosition());
		
		if((player.isInvisible == true) || (distanceToTarget > StaticData_Players.VISIBILITY_RANGE)){
				
			Log.println(Log.INFO, "SETTING PLAYER INVISIBLE", player.getNickname());
			return;
		}
		
		
		if(player.isDead()){
			playerMarkers.put(playerID, addPlayerMarker(googleMap, ICON_DEAD, player));
			if(StaticData_Players.playerIsSelected(playerID))
				StaticData_Players.selectedPlayerUUID = null;
			
			return;
		}
		
		if(StaticData_Players.playerIsSelected(playerID)){				
			playerMarkers.put(playerID, addPlayerMarker(googleMap, ICON_SELECTED, player));
			playerMarkers.get(playerID).showInfoWindow();
						
			googleMap.moveCamera(CameraUpdateFactory.newLatLng(player.getPosition()));
			return;
		}
		
		else{
			
			if(isFriend)
				playerMarkers.put(playerID, addPlayerMarker(googleMap, ICON_FRIEND, player));
			else
				playerMarkers.put(playerID, addPlayerMarker(googleMap, ICON_ENEMY, player));
			
			return;
		}
	}
	
	public static void redrawPlayer(final UUID playerID){
		
		if(!isGoogleMapAvailable())
			return;
		
		final GoogleMap googleMap = GameFragment.googleMap;
		final boolean isFriend = StaticData_Players.playerIsFriend(playerID);
		
		
		if(googleMap != null){
			
			if(!FragmentUtils.isRunningOnUiThread()){
				FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						redrawPlayer_unsafe(googleMap, playerID, isFriend);
					}
				});
			}
			
			else{
				redrawPlayer_unsafe(googleMap, playerID, isFriend);
			}
		}
		
	}
	
	
	
	
	
	private static synchronized void addPlayers(GoogleMap googleMap, LinkedHashMap<UUID, Player> team, BitmapDescriptor teamIcon){
				
		for(Entry<UUID, Player> e : team.entrySet()){
			
			UUID playerID = e.getKey();			
			Player player = e.getValue();
			
			Marker newPlayerMarker = null;
			
			if(player.isDead()){
				newPlayerMarker = addPlayerMarker(googleMap, ICON_DEAD, player);
			}
			
			else if(StaticData_Players.playerIsSelected(playerID)){				
				newPlayerMarker = addPlayerMarker(googleMap, ICON_SELECTED, player);
				newPlayerMarker.showInfoWindow();
				googleMap.moveCamera(CameraUpdateFactory.newLatLng(player.getPosition()));
			}
			
			else{
				newPlayerMarker = addPlayerMarker(googleMap, teamIcon, player);
			}
			
			playerMarkers.put(playerID, newPlayerMarker);
		}
	}
	
	private static synchronized void addFriends(GoogleMap googleMap){
		addPlayers(googleMap, StaticData_Players.friends, ICON_FRIEND);
	}
		
	private static synchronized void addEnemies(GoogleMap googleMap){
		addPlayers(googleMap, StaticData_Players.friends, ICON_ENEMY);
	}
	
	
	
	
	
	public static synchronized void clearPlayerMarkers(){
		
		if(playerMarkers != null){
		
			for(Marker m : playerMarkers.values()){			
				m.remove();			
			}
			
			playerMarkers.clear();
		}
	}
	
	public static synchronized void redrawAllPlayers(){
		
		if(! isGoogleMapAvailable()){
			return;			
		}		
		
		GoogleMap googleMap = GameFragment.googleMap;
		
		if(playerMarkers == null){			
			playerMarkers = new LinkedHashMap<UUID, Marker>(StaticData_Players.maxPlayersPerTeam*2);
		}
		
		clearPlayerMarkers();
		
		addFriends(googleMap);
		addEnemies(googleMap);
		
	}
	
	public static synchronized void redrawAll(){
		
		if(playerMarkers == null){			
			playerMarkers = new LinkedHashMap<UUID, Marker>(StaticData_Players.maxPlayersPerTeam*2);
		}
		
		for(UUID playerUUID : StaticData_Players.friends.keySet()){			
			redrawPlayer(playerUUID);			
		}
		
		for(UUID playerUUID : StaticData_Players.enemies.keySet()){			
			redrawPlayer(playerUUID);			
		}
	}
	
	
	
	public static void drawWeaponRange(Weapon weapon){
		if(! GameFragmentDrawingUtils.isGoogleMapAvailable())
			return;
		
		if(GameFragment.weaponRangeCircle != null){
			removeWeaponRange();			
		}
		
		CircleOptions circleOptions = new CircleOptions();		
		circleOptions.center(StaticData_Players.currentPlayer.getPosition());
		circleOptions.radius(weapon.range);
		circleOptions.fillColor(Color.argb(64, 0, 255, 0));
		
		GameFragment.weaponRangeCircle = GameFragment.googleMap.addCircle(circleOptions);
		GameFragment.mapFragmentView.postInvalidate();
	}
	
	public static void redrawWeaponRange(){
		if(GameFragment.weaponRangeCircle != null){			
			double radius = GameFragment.weaponRangeCircle.getRadius();
			removeWeaponRange();
			
			CircleOptions circleOptions = new CircleOptions();		
			circleOptions.center(StaticData_Players.currentPlayer.getPosition());
			circleOptions.radius(radius);
			circleOptions.fillColor(Color.argb(64, 0, 255, 0));
			
			GameFragment.weaponRangeCircle = GameFragment.googleMap.addCircle(circleOptions);
			GameFragment.mapFragmentView.postInvalidate();
		}
	} 
	
	
	public static void removeWeaponRange(){
		if(! GameFragmentDrawingUtils.isGoogleMapAvailable())
			return;		
		
		GameFragment.weaponRangeCircle.remove();
		GameFragment.weaponRangeCircle = null;
	}
	
	
	
	
	
	public static synchronized UUID getClosestPlayer(LinkedHashMap<UUID, Player> team, double minDistance){
		
		UUID closestPlayerID = null;
		
		LatLng currentPlayerPosition = StaticData_Players.currentPlayer.getPosition();
		
		double nextMinDistance = Double.MAX_VALUE; 
		
		ArrayList<Entry<UUID, Player>> entrySetCopy = new ArrayList<Entry<UUID,Player>>(team.entrySet()); 
		
		for(Entry<UUID,Player> e : entrySetCopy){
			
			double distanceBetweenPlayers = GameFragmentMapUtils.getDistanceBetweenPositions(currentPlayerPosition.latitude, currentPlayerPosition.longitude, e.getValue().getPosition().latitude, e.getValue().getPosition().longitude);
			
			if((distanceBetweenPlayers > minDistance) && (distanceBetweenPlayers < nextMinDistance)){
				closestPlayerID = e.getKey();
				nextMinDistance = distanceBetweenPlayers;
			}
		}
		
		return closestPlayerID;
	}
	
	
	public static synchronized UUID getFarthestPlayer(LinkedHashMap<UUID, Player> team, double maxDistance){
		
		UUID closestPlayerID = null;
		
		LatLng currentPlayerPosition = StaticData_Players.currentPlayer.getPosition();
		
		double nextMaxDistance = -1; 
		
		ArrayList<Entry<UUID, Player>> entrySetCopy = new ArrayList<Entry<UUID,Player>>(team.entrySet());
		
		for(Entry<UUID,Player> e : entrySetCopy){
			
			double distanceBetweenPlayers = GameFragmentMapUtils.getDistanceBetweenPositions(currentPlayerPosition.latitude, currentPlayerPosition.longitude, e.getValue().getPosition().latitude, e.getValue().getPosition().longitude);
			
			if((distanceBetweenPlayers < maxDistance) && (distanceBetweenPlayers > nextMaxDistance)){
				closestPlayerID = e.getKey();
				nextMaxDistance = distanceBetweenPlayers;
			}
		}
		
		return closestPlayerID;
	}
		
	public static UUID getNextClosestPlayer(LinkedHashMap<UUID, Player> team){
		
		LatLng currentPlayerPosition = StaticData_Players.currentPlayer.getPosition();		
		
		UUID selectedPlayerUUID = StaticData_Players.selectedPlayerUUID;
		
		UUID nextPlayerUUID = null;
		
		if(selectedPlayerUUID == null){
						
			nextPlayerUUID = getClosestPlayer(team, -1);
			if(nextPlayerUUID != null)
				Log.println(Log.INFO, "Getting closest player ", StaticData_Players.getPlayerById(nextPlayerUUID).getNickname());
			else
				Log.println(Log.INFO, "Getting closest player ", "null");
			
			return nextPlayerUUID;			
		}		
		else{
			
			Player selectedPlayer = StaticData_Players.getPlayerById(selectedPlayerUUID);
			UUID closestPlayerID = null;
			
			if(selectedPlayer == null)
				return null;
			
			Log.println(Log.INFO, "Getting closest player, farther than " + selectedPlayer.getNickname(), "...");
			double minDistance = GameFragmentMapUtils.getDistanceBetweenPositions(currentPlayerPosition.latitude, currentPlayerPosition.longitude, selectedPlayer.getPosition().latitude, selectedPlayer.getPosition().longitude);			
							
			UUID nextUUID = getClosestPlayer(team, minDistance);
			if(nextUUID == null){
				Log.println(Log.INFO, "No closer player found. Selecting " + selectedPlayer.getNickname(), "...");
				return selectedPlayerUUID;
			}
			
			return nextUUID;
		}
	}
	
	public static UUID getPreviousFarthestPlayer(LinkedHashMap<UUID, Player> team){
		
		LatLng currentPlayerPosition = StaticData_Players.currentPlayer.getPosition();
		
		UUID selectedPlayerUUID = StaticData_Players.selectedPlayerUUID;
		
		UUID nextPlayerUUID = null;
		
		if(selectedPlayerUUID == null){
			
			nextPlayerUUID = getFarthestPlayer(team, Double.MAX_VALUE);
			
			if(nextPlayerUUID != null)				
				Log.println(Log.INFO, "Getting farthest player ", StaticData_Players.getPlayerById(nextPlayerUUID).getNickname());
			else
				Log.println(Log.INFO, "Getting farthest player ", "null");
			
			return nextPlayerUUID;
		}
		
		else{
			
			Player selectedPlayer = StaticData_Players.getPlayerById(selectedPlayerUUID);
			UUID closestPlayerID = null;
			
			if(selectedPlayer == null)
				return null;
			
			double maxDistance = GameFragmentMapUtils.getDistanceBetweenPositions(currentPlayerPosition.latitude, currentPlayerPosition.longitude, selectedPlayer.getPosition().latitude, selectedPlayer.getPosition().longitude);			
			Log.println(Log.INFO, "Getting farthest player, closer than " + selectedPlayer.getNickname(), "...");			
			
			UUID nextUUID = getFarthestPlayer(team, maxDistance); 
			if(nextUUID == null){
				Log.println(Log.INFO, "No farther player found. Selecting " + selectedPlayer.getNickname(), "...");
				return selectedPlayerUUID;
			}
			
			return nextUUID;
		}
		
	}
	
	
	

	
	
	public static void selectNextClosestFriend(){
		
		UUID nextClosestPlayerID = getNextClosestPlayer(StaticData_Players.friends);
		
		if(nextClosestPlayerID != null){		
		
			UUID previouslySelectedPlayerID = StaticData_Players.selectedPlayerUUID;
			
			StaticData_Players.selectedPlayerUUID = nextClosestPlayerID;
			
			if(previouslySelectedPlayerID != null)
				redrawPlayer(previouslySelectedPlayerID);
					
			redrawPlayer(nextClosestPlayerID);
		}
		
		
	}
	
	
	public static void selectNextClosestEnemy(){
		UUID nextClosestPlayerID = getNextClosestPlayer(StaticData_Players.enemies);
		
		if(nextClosestPlayerID != null){
				
			UUID previouslySelectedPlayerID = StaticData_Players.selectedPlayerUUID;
			StaticData_Players.selectedPlayerUUID = nextClosestPlayerID;		
			
			if(previouslySelectedPlayerID != null)
				redrawPlayer(previouslySelectedPlayerID);
					
			redrawPlayer(nextClosestPlayerID);
		}
	}
	
	
	public static void selectPreviousClosestFriend(){
		UUID previousClosestPlayerID = getPreviousFarthestPlayer(StaticData_Players.friends);
		
		if(previousClosestPlayerID != null){
		
			UUID previouslySelectedPlayerID = StaticData_Players.selectedPlayerUUID;
			StaticData_Players.selectedPlayerUUID = previousClosestPlayerID;		
			
			if(previouslySelectedPlayerID != null)
				redrawPlayer(previouslySelectedPlayerID);
					
			redrawPlayer(previousClosestPlayerID);
		}
	}
	
	
	public static void selectPreviousClosestEnemy(){
		UUID previousClosestPlayerID = getPreviousFarthestPlayer(StaticData_Players.enemies);
		
		if(previousClosestPlayerID != null){
		
			UUID previouslySelectedPlayerID = StaticData_Players.selectedPlayerUUID;
			
			StaticData_Players.selectedPlayerUUID = previousClosestPlayerID;		
			
			if(previouslySelectedPlayerID != null)
				redrawPlayer(previouslySelectedPlayerID);
					
			redrawPlayer(previousClosestPlayerID);
		}
	}	
	
	

	
	public static void selectCurrentPlayer(){
		UUID previouslySelectedPlayerID = StaticData_Players.selectedPlayerUUID;		
		
		if(previouslySelectedPlayerID != null){			
			StaticData_Players.selectedPlayerUUID = null;
			redrawPlayer(previouslySelectedPlayerID);		
		}
	}
	
	private static void manageOnMapClick(LatLng clickLatLng){
		selectCurrentPlayer();
	}
		
	private static void manageOnMarkerClick(Marker playerMarker){
		
		UUID playerUUID = getUUIDbyMarker(playerMarker);
		UUID previouslySelectedPlayer = StaticData_Players.selectedPlayerUUID;
		
		StaticData_Players.selectedPlayerUUID = playerUUID;
		
		if(previouslySelectedPlayer != null){
			redrawPlayer(previouslySelectedPlayer);
		}
		
		redrawPlayer(playerUUID);
	}
	

	
	
	
	
	
	
	
}
