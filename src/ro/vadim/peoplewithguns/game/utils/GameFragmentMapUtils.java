package ro.vadim.peoplewithguns.game.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.UUID;



import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;


import ro.vadim.peoplewithguns.communication.TcpGameClient;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.game.fragments.GameFragment;
import ro.vadim.peoplewithguns.game.fragments.LobbyFragment;
import ro.vadim.peoplewithguns.game.gameelements.Player;
import ro.vadim.peoplewithguns.game.misc.AbilityCountdown;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.View;


public class GameFragmentMapUtils {	
	
	public static boolean enteredGame = false;
			
	public static boolean isGpsOn(){
		
		LocationManager manager = (LocationManager) FragmentUtils.getCurrentContext().getSystemService(Context.LOCATION_SERVICE);  
		
		if(manager.isProviderEnabled(LocationManager.GPS_PROVIDER) == true)
			return true;
		
		return false;		
	}
	
	public static void setGpsOn(){
					
		if(!isGpsOn()){
			AdminUtils.buildAlertMessageNoGps(FragmentUtils.getCurrentContext());
		}
	}
	
		
	public static String getDate(long milliSeconds)
	{		
		String dateFormat = "dd/MM/yyyy hh:mm:ss.SSS";
	    SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);	  
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(milliSeconds);
	    return formatter.format(calendar.getTime());
	}
	
	public static long getTimeDelta(long initialTime, boolean inSeconds){
		long currentTime = getCurrentTimeMillis();
		long timeLeft = currentTime - initialTime;
		
		if(inSeconds == false)
			return timeLeft;	
		
		return timeLeft/1000;
	}
	
	public static long getCurrentTimeMillis(){
		Time time = new Time();
		time.setToNow();				
		return time.toMillis(false);
	}
	
	
	
	
	
	public static double getDistanceBetweenPoints(Point point1, Point point2){
		
		double XSquaredDistance = Math.pow((point1.x - point2.x), 2);
		double YSquaredDistance = Math.pow((point1.y - point2.y), 2);
		
		return Math.sqrt(XSquaredDistance + YSquaredDistance);
	}
	
	
	
	
	

	
	
    public static void setNewLocation(Location location){
    			
		if(StaticData_Players.currentPlayer != null)
			StaticData_Players.currentPlayer.setPosition(location.getLatitude(), location.getLongitude());
		
		if(TcpGameClient.isConnected == true)
			MessageSender.send_CHANGE_POSITION(location.getLatitude(), location.getLongitude());
    }
		
	public static double getDistanceBwtweenMeAndPlayer(UUID otherPlayerUUID){
		Player targetPlayer = StaticData_Players.getPlayerById(otherPlayerUUID);
		LatLng currentPlayerPosition = StaticData_Players.currentPlayer.getPosition();
		LatLng targetPlayerPosition = targetPlayer.getPosition();
		return getDistanceBetweenPositions(currentPlayerPosition, targetPlayerPosition);				
	}
			
	public static double getDistanceBetweenPositions(LatLng position1, LatLng position2){
		
		if((position1 != null)&&(position2 != null))
			return getDistanceBetweenPositions(position1.latitude, position1.longitude, position2.latitude, position2.longitude); 
		
		return -1;		
	}
	
	public static double getDistanceBetweenPositions(double latitude1, double longitude1, double latitude2, double longitude2){

		float [] results = new float[2];
				
		Location.distanceBetween(latitude1, longitude1, latitude2, longitude2, results);
				
		return (double)results[0];
	}
	
	
	
	
	
}
