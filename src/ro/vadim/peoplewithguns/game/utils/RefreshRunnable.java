package ro.vadim.peoplewithguns.game.utils;

import ro.vadim.peoplewithguns.game.fragments.GameFragment;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public class RefreshRunnable implements Runnable{
		
	@Override
	public void run() {		
		
		while(FragmentUtils.currentFragment.getClass().equals(GameFragment.class)){
			
			try {
				Thread.sleep(500);
				GameFragmentUtils.redrawAll();
			}
			
			catch (InterruptedException e) {
				Log.println(Log.ERROR, "RefreshRunnable ERROR ! ", e.toString());
			}
		}	
	}      	  
}
