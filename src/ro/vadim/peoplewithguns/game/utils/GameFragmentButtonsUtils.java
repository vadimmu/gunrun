package ro.vadim.peoplewithguns.game.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.location.Location;
import android.opengl.Visibility;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.RelativeLayout.LayoutParams;


import ro.vadim.peoplewithguns.R;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.game.fragments.GameFragment;
import ro.vadim.peoplewithguns.game.gameelements.Player;
import ro.vadim.peoplewithguns.game.gameelements.Weapon;
import ro.vadim.peoplewithguns.game.misc.AbilityCountdown;
import ro.vadim.peoplewithguns.game.misc.DurationReportingTask;
import ro.vadim.peoplewithguns.game.misc.WeaponReportingTask;
import ro.vadim.peoplewithguns.game.misc.AbilityCountdown.CountdownAction;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_ProfessionsAndWeapons;

public class GameFragmentButtonsUtils {
	
	
	private static boolean selectFriends = true;	
	public static ArrayList<Button> buttons = null;	
	public static int EMS_DEFAULT = 4;
	public static int EMS_ALTERNATIVE = 5;	
	
	
	
	
	
	public static void setupPowerupsLayout(RelativeLayout relativeLayout){
		
		Log.println(Log.INFO, "Setting up powerups layout", "...");
		
		GameFragment.powerupsLayout = new LinearLayout(relativeLayout.getContext());
		GameFragment.powerupsLayout.setOrientation(LinearLayout.HORIZONTAL);
		GameFragment.powerupsLayout.setId(GameFragmentDrawingUtils.generateViewId());
		
		GameFragment.powerupsLayout.setBackgroundColor(Color.WHITE);
		GameFragment.powerupsLayout.setMinimumWidth(GameFragment.myHealthTextView.getWidth());
		GameFragment.powerupsLayout.setMinimumHeight(GameFragment.myHealthTextView.getHeight());
		
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		
		GameFragment.powerupsLayout.setLayoutParams(layoutParams);
		relativeLayout.addView(GameFragment.powerupsLayout);
		
	}
	
	public static void addPowerupCounter(final Weapon weapon){
		Log.println(Log.INFO, "Preparing powerup counter", "...");
				
		FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				if(GameFragment.powerupsLayout == null)
					return;
				
				Log.println(Log.INFO, "Adding powerup counter", "Weapon : " + weapon.name);
				
				TextView textView = new TextView(GameFragment.powerupsLayout.getContext());
				textView.setTextColor(Color.BLACK);
				textView.setBackgroundColor(Color.WHITE);
				textView.setText(weapon.name);
				
				LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
				
				layoutParams.gravity = Gravity.CENTER_VERTICAL;
				textView.setLayoutParams(layoutParams);
							
				startWeaponDuration(textView, weapon);				
			}
		});
	}
	
	
	
	
	
	public static void setupTargetButtons(RelativeLayout relativeLayout){
		
		GameFragment.toggleButtonsLayout = new LinearLayout(relativeLayout.getContext());
		GameFragment.toggleButtonsLayout.setOrientation(LinearLayout.VERTICAL);
		GameFragment.toggleButtonsLayout.setId(GameFragmentDrawingUtils.generateViewId());
		
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		GameFragment.toggleButtonsLayout.setLayoutParams(layoutParams);
		
		GameFragment.previousTargetButton = new Button(relativeLayout.getContext());
		GameFragment.previousTargetButton.setText("Prev Target");
		GameFragment.previousTargetButton.setId(GameFragmentDrawingUtils.generateViewId());
		
		GameFragment.friendsToggleButton = new ToggleButton(relativeLayout.getContext());	
		GameFragment.friendsToggleButton.setId(GameFragmentDrawingUtils.generateViewId());
		GameFragment.friendsToggleButton.setTextOn("Friends");
		GameFragment.friendsToggleButton.setTextOff("Enemies");
					
		GameFragment.friendsToggleButton.setText("Friends");
		GameFragment.friendsToggleButton.setChecked(true);
		
		GameFragment.nextTargetButton = new Button(relativeLayout.getContext());
		GameFragment.nextTargetButton.setText("Next Target");
		GameFragment.nextTargetButton.setId(GameFragmentDrawingUtils.generateViewId());
		
		GameFragment.selectMeButton = new Button(relativeLayout.getContext());
		GameFragment.selectMeButton.setText("Myself");
		GameFragment.selectMeButton.setId(GameFragmentDrawingUtils.generateViewId());
		GameFragment.selectMeButton.setEms(EMS_DEFAULT);
		GameFragment.selectMeButton.setLines(2);
				
		GameFragment.toggleButtonsLayout.addView(GameFragment.selectMeButton);
		GameFragment.toggleButtonsLayout.addView(GameFragment.nextTargetButton);
		GameFragment.toggleButtonsLayout.addView(GameFragment.friendsToggleButton);
		GameFragment.toggleButtonsLayout.addView(GameFragment.previousTargetButton);
		
		
		relativeLayout.addView(GameFragment.toggleButtonsLayout);		
	}
		
	public static void addWeaponButtons(LinearLayout weaponsLayout){
		
		
		
		int numberOfWeapons = StaticData_Players.currentPlayer.getWeapons().size();
		
		buttons = new ArrayList<Button>(numberOfWeapons);
				
		for(Weapon w: StaticData_Players.currentPlayer.getWeapons()){
			
			DisplayMetrics displayMetrics = new DisplayMetrics();
			
			Button weaponButton = new Button(weaponsLayout.getContext());
			
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			layoutParams.width = 0;
			layoutParams.weight = 1;			
			layoutParams.gravity = Gravity.CENTER_VERTICAL;
			
			if(w.name.equals("Sniper Rifle"))
				weaponButton.setText("Sniper");
			
			else if(w.name.equals("Invisibility Cloak"))
				weaponButton.setText("Invisib");
			
			else if(w.name.equals("Painkillers"))
				weaponButton.setText("Heal");
			
			else{
				weaponButton.setText(w.name);
			}
			
			weaponButton.setId(GameFragmentDrawingUtils.generateViewId());
			weaponButton.setMaxHeight(GameFragment.nextTargetButton.getHeight());
			weaponButton.setMinWidth(displayMetrics.widthPixels / (numberOfWeapons + 2));
			
			
			weaponButton.setLines(2);			
			
			weaponButton.setEnabled(false);
			
			
			
			if(numberOfWeapons >= 5)			
				weaponButton.setEms(EMS_DEFAULT);			
			else
				weaponButton.setEms(EMS_ALTERNATIVE);
			
			
			final Weapon weaponAUX = w;
			final Button weaponButtonAUX = weaponButton;
			
			weaponButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					shoot(v, weaponAUX, StaticData_Players.selectedPlayerUUID);
				}
			});
			
			weaponButton.setOnLongClickListener(new OnLongClickListener() {
				
				@Override
				public boolean onLongClick(View v) {
					
					GameFragmentDrawingUtils.drawWeaponRange(weaponAUX);					
					return true;
				}
			});
			
			weaponButton.setLayoutParams(layoutParams);
			weaponsLayout.addView(weaponButton);
			
			buttons.add(weaponButton);
			
		}
	}
		
	public static void setupWeaponButtons(RelativeLayout relativeLayout){
		
		DisplayMetrics displayMetrics = new DisplayMetrics();
		
		GameFragment.weaponButtonsLayout = new LinearLayout(relativeLayout.getContext());
		GameFragment.weaponButtonsLayout.setOrientation(LinearLayout.HORIZONTAL);
		GameFragment.weaponButtonsLayout.setId(GameFragmentDrawingUtils.generateViewId());
		
		
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		//layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		layoutParams.addRule(RelativeLayout.LEFT_OF, GameFragment.previousTargetButton.getId());		
		layoutParams.addRule(RelativeLayout.ALIGN_TOP, GameFragment.previousTargetButton.getId());
			
		//layoutParams.width = (int) (displayMetrics.widthPixels * 0.8);
		//layoutParams.height = (int) GameFragment.previousTargetButton.getHeight();
		
		GameFragment.weaponButtonsLayout.setLayoutParams(layoutParams);
		
		addWeaponButtons(GameFragment.weaponButtonsLayout);		
				
		relativeLayout.addView(GameFragment.weaponButtonsLayout);
		
	}

	public static void setupStaticButtons(RelativeLayout relativeLayout){
		setupTargetButtons(relativeLayout);
		
		setupFriendToggleButton();
		setupPreviousTargetButton();
		setupNextTargetButton();
		setupSelectMeButton();
	}
	
	public static void setupHealthTextView(RelativeLayout relativeLayout){
		
		GameFragment.myHealthTextView = new TextView(relativeLayout.getContext());
		GameFragment.myHealthTextView.setId(GameFragmentDrawingUtils.generateViewId());
		GameFragment.myHealthTextView.setText("HEALTH : " + StaticData_Players.currentPlayer.getHealth());		
		GameFragment.myHealthTextView.setTextColor(Color.BLACK);
		GameFragment.myHealthTextView.setBackgroundColor(Color.WHITE);
		GameFragment.myHealthTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
		
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		GameFragment.myHealthTextView.setLayoutParams(layoutParams);
		
		relativeLayout.addView(GameFragment.myHealthTextView);
		
	}
	
	
	
	
	
	public static void addItemsOnMessagesSpinner(RelativeLayout relativeLayout) {  
		  ArrayList<String> list = new ArrayList<String>();
		  		  
		  list.add("Team Messages >>>");
		  list.add("Go Go Go !");
		  list.add("Cover me !");
		  list.add("Heal me !");
		  list.add("Take the flank !");
		  list.add("Group up !");
		  list.add("Defence !");
		  list.add("Offence !");
		  list.add("Split up !");  
		  
		  
		  
		  ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(relativeLayout.getContext(), android.R.layout.simple_spinner_item, list){
			  
			@Override
			public boolean isEnabled(int position) {
				if(position == 0)
					return false;
				
				return super.isEnabled(position);
			}
			 
			  
		  };
		  
		  dataAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line); 
		  GameFragment.messagesSpinner.setAdapter(dataAdapter);
		  
		  GameFragment.messagesSpinner.getAdapter().getView(0, null, null).setEnabled(false);
	}
	
	public static void setupMessagesSpinner(final RelativeLayout relativeLayout){
		
		GameFragment.messagesSpinner = new Spinner(relativeLayout.getContext());
		GameFragment.messagesSpinner.setId(GameFragmentDrawingUtils.generateViewId());
		GameFragment.messagesSpinner.setBackgroundColor(Color.argb(128, 255, 250, 250));
		
		
		addItemsOnMessagesSpinner(relativeLayout);		
		
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.BELOW, GameFragment.myHealthTextView.getId());
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		layoutParams.addRule(RelativeLayout.ALIGN_RIGHT, GameFragment.myHealthTextView.getId());
		
		
		GameFragment.messagesSpinner.setLayoutParams(layoutParams);
		GameFragment.messagesSpinner.setContentDescription("TEAM MESSAGES");
		
		
		GameFragment.messagesSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				
				if(arg2<=0)
					return;
				
				String message = arg0.getItemAtPosition(arg2).toString();
				
				try {
					MessageSender.send_MESSAGE_TEAM(message);
					FragmentUtils.showText(FragmentUtils.currentContext, " ME : \""+message+"\"");					
				} 
				catch (IOException e) {
					Log.println(Log.ERROR, "Could not send in-game message to team !", e.toString());
				}
				
				GameFragment.messagesSpinner.setSelection(0);
												
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
			}
			
			
		});
		
		GameFragment.messagesSpinner.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				
				return false;
			}
		});
		
		
		relativeLayout.addView(GameFragment.messagesSpinner);		
		
	}
	
	
	
	
	
	public static void setupPreviousTargetButton(){
		GameFragment.previousTargetButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				manageSelectPreviousTarget();				
			}	
		});
	}
		
	public static void setupNextTargetButton(){
		GameFragment.nextTargetButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				manageSelectNextTarget();				
			}
		});
	}
		
	public static void setupFriendToggleButton(){
		GameFragment.friendsToggleButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				toggleFriends();
			}
		});
	}	
	
	public static void setupSelectMeButton(){
		GameFragment.selectMeButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				GameFragmentDrawingUtils.selectCurrentPlayer();
			}
		});
		
	}
	
	
	
	
	
	public static void manageSelectNextTarget(){
		
		if(GameFragment.friendsToggleButton.isChecked())
			GameFragmentDrawingUtils.selectNextClosestFriend();
		else
			GameFragmentDrawingUtils.selectNextClosestEnemy();
	}
	
	public static void manageSelectPreviousTarget(){
		
		if(GameFragment.friendsToggleButton.isChecked())
			GameFragmentDrawingUtils.selectPreviousClosestFriend();
		else
			GameFragmentDrawingUtils.selectPreviousClosestEnemy();
	}
	
	public static void toggleFriends(){
		
		if(GameFragment.friendsToggleButton.isChecked()){
			selectFriends = true;			
		}
		else{
			selectFriends = false;
		}
	}
	
	
	
	
	
	public static void startAbilityCountdown(AbilityCountdown abilityCountdown){
		Thread countdownThread = new Thread(abilityCountdown);
		countdownThread.start();
	}
	
	
	
	
	
	public static void startWeaponCooldown(View pressedButtonView, Weapon currentWeapon){
		
		WeaponReportingTask cooldownTimer = new WeaponReportingTask((Button)pressedButtonView);									
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			
			Log.println(Log.INFO, "Build version > HONEYCOMB", "running on custom executor");
			cooldownTimer.executeOnExecutor(GameFragment.executor, currentWeapon);
		}
		else {
			Log.println(Log.INFO, "Build version < HONEYCOMB", "running on default executor");
			cooldownTimer.execute(currentWeapon);
		}		
	}
		
	public static void startWeaponDuration(View textView, final Weapon currentWeapon){
		
		final DurationReportingTask cooldownTimer = new DurationReportingTask((TextView)textView);
				
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			
			Log.println(Log.INFO, "Build version > HONEYCOMB", "running on custom executor");
			cooldownTimer.executeOnExecutor(GameFragment.executor, currentWeapon);
		}
		else {
			Log.println(Log.INFO, "Build version < HONEYCOMB", "running on default executor");
			cooldownTimer.execute(currentWeapon);
		}
			
	}
	
	
	
	
	
	
	
	private static boolean cooldownHasExpired(Weapon currentWeapon){
		
		if(GameFragmentMapUtils.getTimeDelta(currentWeapon.getLastUsed(), true) <= currentWeapon.cooldown ){
			FragmentUtils.showText(GameFragment.mapFragmentView.getContext(), "can't shoot this weapon yet !");
			return false;	
		}		
		
		return true;
	}
		
	private static boolean targetCanBeSeen(Player targetPlayer){
		if(targetPlayer.isInvisible){
			FragmentUtils.showText(GameFragment.mapFragmentView.getContext(), "you can't see'em, you can't shoot'em !");
			return false;
		}
		
		return true;
	}
	
	private static synchronized void shoot_generic_weapon(UUID targetID, Weapon currentWeapon, View pressedButtonView){
		
		Player targetPlayer = StaticData_Players.getPlayerById(targetID);
		String currentWeaponName = currentWeapon.name;
		
		if(targetPlayer == null){
			FragmentUtils.showText(FragmentUtils.currentActivity, "target = null !");
			return;
		}
		
		int damage = currentWeapon.damage;
		
		//doesn't shoot if the cooldown hasn't passed
		if(!cooldownHasExpired(currentWeapon))
			return;
		
		//can't shoot an invisible target
		if(!targetCanBeSeen(targetPlayer))
			return;
		
		
		//shield reduces damage to half
		if(targetPlayer.isShielded)
			damage /= 2;
		
				
		//mark the last time the weapon was used. with or without the countdown on the button, this restricts use.
		long shotTime = GameFragmentMapUtils.getCurrentTimeMillis();
		currentWeapon.setLastUsed(shotTime);
				
		MessageSender.send_SHOOT(targetID, currentWeaponName, damage);
		
		targetPlayer.modifyHealth(damage);
		
		GameFragmentDrawingUtils.redrawPlayer(targetID);
		
		//disable the button and show a countdown until it will be enabled again		
		startWeaponCooldown(pressedButtonView, currentWeapon);
	}
		
	private static void shoot_invisibility_cloak(View pressedButtonView, Weapon currentWeapon){
		
		if(!cooldownHasExpired(currentWeapon))
			return;
		
		
		UUID targetID = StaticData_Players.currentPlayerUUID;
		
		Player target = StaticData_Players.getPlayerById(targetID);
		target.isInvisible = true;
		startAbilityCountdown(StaticData_ProfessionsAndWeapons.setInvisiblityCountdown(targetID));
		
		long shotTime = GameFragmentMapUtils.getCurrentTimeMillis();
		currentWeapon.setLastUsed(shotTime);
		
		MessageSender.send_SHOOT(targetID, currentWeapon.name, currentWeapon.damage);		
		
		GameFragmentUpdaterUtils.showText("You have cast Invisibility Cloak on yourself !");		
		addPowerupCounter(currentWeapon);
		startWeaponCooldown(pressedButtonView, currentWeapon);		
	}
	
	private static void shoot_shield(View pressedButtonView, Weapon currentWeapon, UUID targetID){
				
		if(!cooldownHasExpired(currentWeapon))
			return;
		
		Player targetPlayer = StaticData_Players.getPlayerById(targetID);
			
		if(targetPlayer == null){			
			return;
		}		
		
		if(!targetPlayer.isShielded){
			
			targetPlayer.isShielded = true;
			startAbilityCountdown(StaticData_ProfessionsAndWeapons.setShieldCountdown(targetID));
						
			long shotTime = GameFragmentMapUtils.getCurrentTimeMillis();		
			currentWeapon.setLastUsed(shotTime);			
			MessageSender.send_SHOOT(targetID, currentWeapon.name, currentWeapon.damage);
			
			if(targetID.equals(StaticData_Players.currentPlayerUUID)){
				GameFragmentUpdaterUtils.showText("You have cast Shield on yourself !");
				addPowerupCounter(currentWeapon);
			}
			
			else
				GameFragmentUpdaterUtils.showText("You have cast Shield on " + StaticData_Players.getPlayerById(targetID).getNickname());
		}
		
		else{			
			GameFragmentUpdaterUtils.showText("This player is already shielded !");			
		}
		
		
		
		
		startWeaponCooldown(pressedButtonView, currentWeapon);
	}
		
	private static void shoot_painkillers(View pressedButtonView, Weapon currentWeapon, UUID targetID){
		
		Player target = StaticData_Players.getPlayerById(targetID);
		
		if(target == null){			
			return;
		}
			
		
		String profession = target.getProfessionTitle();
		int maxProfessionHealth = StaticData_ProfessionsAndWeapons.professions.get(profession).health;
		
		int healthDifference = maxProfessionHealth - target.getHealth();		
		int healAmount = (healthDifference >= (-1)*currentWeapon.damage) ? currentWeapon.damage : (-1) * healthDifference; 
		
		if(!cooldownHasExpired(currentWeapon))
			return;
		
		if(healAmount == 0){
			GameFragmentUpdaterUtils.showText("Nothing to heal...");
			return;
		}
		
		
		
		
		target.modifyHealth(healAmount);
		GameFragmentUpdaterUtils.updateHealthTextView();
		long shotTime = GameFragmentMapUtils.getCurrentTimeMillis();
		currentWeapon.setLastUsed(shotTime);
		
		
		
		
		MessageSender.send_SHOOT(targetID, currentWeapon.name, healAmount);
		startWeaponCooldown(pressedButtonView, currentWeapon);		
		
		if(targetID.equals(StaticData_Players.currentPlayerUUID))
			GameFragmentUpdaterUtils.showText("You have healed yourself ( "+ (-1)*healAmount+" health)");
		
		else
			GameFragmentUpdaterUtils.showText("You have healed" + StaticData_Players.getPlayerById(targetID).getNickname() + " ( "+ (-1)*healAmount+" health)");
		
		
	}
	
	
	
	
	
	
	private static boolean shootRequirementsSatisfied(Weapon weapon, UUID targetID){
		
		
		if(StaticData_Players.playerIsEnemy(targetID)){
			if(weapon.policy == Weapon.POLICY_ENEMIES)
				return true;
			
			if(weapon.policy == Weapon.POLICY_FRIENDS)
				return false;
			
			if(weapon.policy == Weapon.POLICY_FRIENDS_AND_ENEMIES)
				return true;
			
			if(weapon.policy == Weapon.POLICY_FRIENDS_AND_ENEMIES_AND_SELF)
				return true;
			
			if(weapon.policy == Weapon.POLICY_FRIENDS_AND_SELF)
				return false;
			
			if(weapon.policy == Weapon.POLICY_SELF)
				return false;			
		}
		
		if(StaticData_Players.playerIsFriend(targetID)){
			if(weapon.policy == Weapon.POLICY_ENEMIES)
				return false;
			
			if(weapon.policy == Weapon.POLICY_FRIENDS)
				return true;
			
			if(weapon.policy == Weapon.POLICY_FRIENDS_AND_ENEMIES)
				return true;
			
			if(weapon.policy == Weapon.POLICY_FRIENDS_AND_ENEMIES_AND_SELF)
				return true;
			
			if(weapon.policy == Weapon.POLICY_FRIENDS_AND_SELF)
				return true;
			
			if(weapon.policy == Weapon.POLICY_SELF)
				return false;			
		}
		
		
		if(StaticData_Players.playerIsMe(targetID)){
			if(weapon.policy == Weapon.POLICY_ENEMIES)
				return false;
			
			if(weapon.policy == Weapon.POLICY_FRIENDS)
				return false;
			
			if(weapon.policy == Weapon.POLICY_FRIENDS_AND_ENEMIES)
				return false;
			
			if(weapon.policy == Weapon.POLICY_FRIENDS_AND_ENEMIES_AND_SELF)
				return true;
			
			if(weapon.policy == Weapon.POLICY_FRIENDS_AND_SELF)
				return true;
			
			if(weapon.policy == Weapon.POLICY_SELF)
				return true;
		}
		
		return false;
		
	}
	
	private static UUID approveShotOnTarget(Weapon weapon, UUID targetID){
		
		if(weapon.name.equals("Invisibility Cloak")){			
			return StaticData_Players.currentPlayerUUID;
		}
		
		
		if((weapon.policy == Weapon.POLICY_SELF) || 
			(weapon.policy == Weapon.POLICY_FRIENDS_AND_SELF) || 
			(weapon.policy == Weapon.POLICY_FRIENDS_AND_ENEMIES_AND_SELF)){
			
			if(StaticData_Players.selectedPlayerUUID == null)
				targetID = StaticData_Players.currentPlayerUUID;
		}
		
		if(targetID == null)
			Log.i("GameFragmentButtonsUtils", "shoot (approveShot): targetID = null");
		else
			Log.i("GameFragmentButtonsUtils", "shoot (approveShot): targetID = "+targetID.toString());
		
		
		if(targetID == null){
			return null;
		}
		
		if(!shootRequirementsSatisfied(weapon, targetID))
			return null;
		
		double distance = GameFragmentMapUtils.getDistanceBwtweenMeAndPlayer(targetID);
		
		int range = weapon.range;
		
		if(distance > range){
			FragmentUtils.showText(GameFragment.mapFragmentView.getContext(), "distance "+String.valueOf(distance) + ". TOO FAR ! Get closer !");
			return null;
		}
		
		return targetID;
		
		
	}
	
	public static void shoot(View pressedButtonView, Weapon weapon, UUID targetID){
		
		
		if((pressedButtonView == null) || (weapon == null))
			return;
		
		
		if(targetID == null)
			Log.i("GameFragmentButtonsUtils", "shoot (before): targetID = null");
		else
			Log.i("GameFragmentButtonsUtils", "shoot (before): targetID = "+targetID.toString());
		
		
		
		targetID = approveShotOnTarget(weapon, targetID); 
		
		if(targetID == null)
			return;
		
		if(targetID == null)
			Log.i("GameFragmentButtonsUtils", "shoot (after): targetID = null");
		else
			Log.i("GameFragmentButtonsUtils", "shoot (after): targetID = "+targetID.toString());
		
		
		
		if(weapon.name.equals("Invisibility Cloak")){
			shoot_invisibility_cloak(pressedButtonView, weapon);
			return;
		}
		
		if(weapon.name.equals("Shield")){
			shoot_shield(pressedButtonView, weapon, targetID);
			return;
		}
		
		if(weapon.name.equals("Painkillers")){
			shoot_painkillers(pressedButtonView, weapon, targetID);
			return;
		}
		
		GameFragmentUpdaterUtils.showText("You have used " + weapon.name + " on " + StaticData_Players.getPlayerById(targetID).getNickname());
		shoot_generic_weapon(targetID, weapon, pressedButtonView);
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
