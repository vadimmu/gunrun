package ro.vadim.peoplewithguns.game.utils;

import java.util.UUID;

import ro.vadim.peoplewithguns.game.fragments.GameFragment;
import ro.vadim.peoplewithguns.game.gameelements.Player;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;
import android.app.Fragment;
import android.view.View;
import android.widget.Button;

public class GameFragmentUpdaterUtils{

	
	
	private static synchronized void updateHealthTextView_unsafe(){
		
		if(StaticData_Players.currentPlayer != null){
			
			Player currentPlayer = StaticData_Players.currentPlayer;
			
			if(currentPlayer.getHealth() <= 0){
				GameFragment.myHealthTextView.setText("DEAD...");
				
				if(GameFragmentDrawingUtils.isGameFragmentOn() && (FragmentUtils.currentActivity != null)){					
					AdminUtils.buildGameOverAlertMessage(GameFragment.mapContainer.getContext(), FragmentUtils.currentActivity);					
				}
			}
			
			else{
				GameFragment.myHealthTextView.setText("HEALTH : " + currentPlayer.getHealth());
			}
    	}
	}
	
	
	public static void updateHealthTextView(){
		

		if(!FragmentUtils.isRunningOnUiThread()){
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					updateHealthTextView_unsafe();
				}
			});
		}
		
		else{
			updateHealthTextView_unsafe();
		}
		
	}
	
	
	private static synchronized void updateSelectedPlayerMarker_unsafe(){
		
		if(GameFragmentDrawingUtils.isGoogleMapAvailable()){
			
			UUID selectedPlayerUUID = StaticData_Players.selectedPlayerUUID; 
			
			if(selectedPlayerUUID != null){
				GameFragmentDrawingUtils.redrawPlayer(selectedPlayerUUID);
			}
		}
	}
	
	public static void updateSelectedPlayerMarker(){
		
		if(!FragmentUtils.isRunningOnUiThread()){
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					updateSelectedPlayerMarker_unsafe();
				}
			});
		}
		
		else{
			updateSelectedPlayerMarker_unsafe();
		}
	}
	
	
	public static synchronized void enableWeapons_unsafe(boolean enableWeapons){
		if(GameFragmentButtonsUtils.buttons != null){
			for(Button b : GameFragmentButtonsUtils.buttons){
				b.setEnabled(enableWeapons);
			}
		}		
	}
	
	private static void enableWeapons(final boolean enableWeapons){
		if(!FragmentUtils.isRunningOnUiThread()){
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					enableWeapons_unsafe(enableWeapons);
				}
			});
		}
		
		else{
			enableWeapons_unsafe(enableWeapons);
		}		
	}
	
	
	public static void enableWeapons(){
		enableWeapons(true);		
	}
	
	public static void disableWeapons(){
		enableWeapons(false);
	}
	
	
	
	
	
	
	public static void showAlert(String message){
		AdminUtils.buildAlertMessage(GameFragment.mapContainer.getContext(), message);
	}
	
	public static void showText(String message){
		FragmentUtils.showText(GameFragment.mapContainer.getContext(), message);
	}
	
	
	
	
	private static void setButtonsHidden_unsafe(boolean setHidden){
		
		
		if(setHidden == true){
		
			for(Button b : GameFragmentButtonsUtils.buttons){
				b.setVisibility(View.INVISIBLE);			
			}
					
			GameFragment gameFragment = (GameFragment) FragmentUtils.currentFragment;
			
			gameFragment.googleMap.setMyLocationEnabled(false);
			
			gameFragment.myHealthTextView.setVisibility(View.INVISIBLE);			
			gameFragment.selectMeButton.setVisibility(View.INVISIBLE);
			gameFragment.messagesSpinner.setVisibility(View.INVISIBLE);
			
		}
		
		else{
			
			for(Button b : GameFragmentButtonsUtils.buttons){
				b.setVisibility(View.VISIBLE);
			}
						
			GameFragment gameFragment = (GameFragment) FragmentUtils.currentFragment;
			
			gameFragment.googleMap.setMyLocationEnabled(true);
			
			gameFragment.myHealthTextView.setVisibility(View.VISIBLE);
			gameFragment.selectMeButton.setVisibility(View.VISIBLE);
			gameFragment.messagesSpinner.setVisibility(View.VISIBLE);
						
		}
	}
	
	public static void setButtonsInvisible(){
		if(!FragmentUtils.isRunningOnUiThread()){
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					setButtonsHidden_unsafe(true);
				}
			});
		}
		
		else{
			setButtonsHidden_unsafe(true);
		}
		
	}
	
	public static void setButtonsVisible(){
		if(!FragmentUtils.isRunningOnUiThread()){
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					setButtonsHidden_unsafe(false);
				}
			});
		}
		
		else{
			setButtonsHidden_unsafe(false);
		}		
	}
	
}
