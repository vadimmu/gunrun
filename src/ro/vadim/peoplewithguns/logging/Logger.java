package ro.vadim.peoplewithguns.logging;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import ro.vadim.peoplewithguns.game.utils.FragmentUtils;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.net.TrafficStatsCompat;
import android.text.format.Time;
import android.util.Log;

public class Logger{
	
	private String LOGGER_TAG = "LOGGING ERROR";
	
	
	private static long MAX_FILE_SIZE_BYTES = 204800;
	private String logDir = Environment.getExternalStorageDirectory().getPath()+"/PeopleWithGuns";
	private String logFile = "PeopleWithGuns_logFile.txt";
	private String dataUsageFile = "PeopleWithGuns_dataUsage.txt";
	
	private File log = null;
	private File log_dataUsage = null;
		
	private long startRx = 0;
	private long startTx = 0;
	
	private long endRx = 0;
	private long endTx = 0;
	
	ConnectivityManager connectivityManager = null;   // WORK WITH THIS !
	NetworkInfo networkInfo = null; 				  // WORK WITH THIS !
	
	public boolean isLogFileInitialized = false;
	public boolean isDataUsageFileInitialized = false;
		
	
	public void getInitialDataVolume(){
		startRx = TrafficStats.getTotalRxBytes();
		startTx = TrafficStats.getTotalTxBytes();
		
		
		if(startRx == TrafficStats.UNSUPPORTED){
			startRx = 0;			
		}
		
		if(startTx == TrafficStats.UNSUPPORTED){
			startTx = 0;			
		}
		
		
		Log.i("GETTING DOWNLOAD AND UPLOAD DATA", "Download : "+startRx+"  Upload : "+startTx);
		
	}
	
	public void getFinalDataVolume(){
		
		long endRx_AUX = TrafficStats.getTotalRxBytes();
		long endTx_AUX = TrafficStats.getTotalTxBytes();
		
		
		
		if(endRx_AUX == TrafficStats.UNSUPPORTED)
			endRx_AUX = 0;
		
		if(endTx_AUX == TrafficStats.UNSUPPORTED)
			endTx_AUX = 0;
		
		
		
		if(endRx_AUX > endRx)
			endRx = endRx_AUX;
		
		if(endTx_AUX > endTx)
			endTx = endTx_AUX;
		
		
	}
		
	private boolean createNewFile(File file) throws IOException{
		if(!file.exists()){
			
			if(file.createNewFile()){
				Log.i("Logging file" + file.getName() + " created", "...");							 
				
			}
			
			else{
				Log.i("Logging file " + file.getName() + " NOT created", "...");
				return false;
			}
		}
		else{
			
			if(file.length() >= MAX_FILE_SIZE_BYTES){
				Log.i("LOG FILE "+ file.getName() +" TOO BIG ", "Deleting log file");
				file.delete();
				
				if(file.createNewFile()){
					Log.i("Logging file "+ file.getName() + " REcreated", "...");
				}
				
				else{
					Log.i("Logging file "+ file.getName() + " NOT REcreated", "...");
					return false;
				}
			}
		}
		
		return true;		
	}
	
	
	public void initLoggingFiles(){
		
		Log.i("INITIALIZING LOG FILES ", "...");
						
		try {			
			File dir = new File(logDir);						
			if(!dir.exists()){
				
				if(dir.mkdir()){					
					Log.i("Logging directory created", "...");
				}
				
				else{
					Log.i("Logging directory NOT created", "...");					
				}
			}			
						
			log = new File(logDir+"/"+logFile);
			log_dataUsage = new File(logDir+"/"+dataUsageFile);
			
			if(createNewFile(log))
				isLogFileInitialized = true;
				
			if(createNewFile(log_dataUsage))
				isDataUsageFileInitialized = true;
				
		}
		
		catch (IOException e) {
			Log.e(LOGGER_TAG, e.toString());			
		}
	}
		
	
	public void clearLogCat(){
		try {
			Process process = Runtime.getRuntime().exec("logcat -c");			
		} 
		
		catch (IOException e) {
			Log.e("Could not clear LogCat", e.toString());
		}		
	}
		
	
	
	
	
	public void getDataUsage(){
		
		if(!isDataUsageFileInitialized)
			return;
		
		Log.i(LOGGER_TAG, "Getting data usage !");
		
		
		getFinalDataVolume();
					
		BufferedWriter bufferedWriter = null;
		
		try {		
			
			bufferedWriter = new BufferedWriter(new FileWriter(log_dataUsage, true));
			
			Time time = new Time();
			time.setToNow();
			int year = time.year;
			int month = 1+time.month;
			int day = time.monthDay;
			int hour = time.hour;
			int minute = time.minute;
			int second = time.second;
			
			
			bufferedWriter.write("\n"+day+"."+month+"."+year+",  "+hour+" : "+minute+" : "+second);		
			bufferedWriter.write("\n"+"========================");
			bufferedWriter.write("\n"+"Data Usage(Beginning) : Download : "+startRx+" and Upload : "+startTx);
			bufferedWriter.write("\n"+"Data Usage(End) : Download : "+endRx+" and Upload : "+endTx);
			bufferedWriter.write("\n"+"Data Usage(DIFFERENCE) : Download : "+String.valueOf((double)(endRx - startRx) / 1024) +"KB and Upload : "+String.valueOf((double)(endTx - startTx) /1024)+"KB");
			bufferedWriter.write("\n"+"========================");
						
						
			bufferedWriter.close();
		}
		
		catch (IOException e) {
			Log.e(LOGGER_TAG, e.toString());
		}		
	}
	
	public void getLog(){
		
		
		if(!isLogFileInitialized)
			return;		
		
		
		Log.i(LOGGER_TAG, "Getting log !");
		
		BufferedReader bufferedReader = null;	
		BufferedWriter bufferedWriter = null;
		
		
		try {
			
			Process process = Runtime.getRuntime().exec("logcat -d *:E");				
						
			bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			bufferedWriter = new BufferedWriter(new FileWriter(log, true));
			
			Time time = new Time();
			time.setToNow();
			int year = time.year;
			int month = 1+time.month;
			int day = time.monthDay;
			int hour = time.hour;
			int minute = time.minute;
			int second = time.second;
			
			
			bufferedWriter.write("\n"+day+"."+month+"."+year+",  "+hour+" : "+minute+" : "+second);						
			bufferedWriter.write("\n"+"------------------------");
			
			String line = null;
			
			while ((line = bufferedReader.readLine()) != null) {				
				bufferedWriter.append("\n"+line);
			}
			
			
			bufferedWriter.write("\n"+"========================");
			bufferedWriter.write("\n"+"========================");
			
			for(int i=0;i<10;i++){
				bufferedWriter.write("\n");
			}
						
			bufferedReader.close();
			bufferedWriter.close();			
			
			process.destroy();			
						
		}	
		
		catch (IOException e) {
			Log.e(LOGGER_TAG, e.toString());
		}
	}
	
	
}
