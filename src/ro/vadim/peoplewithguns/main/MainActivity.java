package ro.vadim.peoplewithguns.main;







import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.MapsInitializer;

import ro.vadim.peoplewithguns.R;
import ro.vadim.peoplewithguns.communication.TcpGameClient;
import ro.vadim.peoplewithguns.communication.TcpGameManagementClient;
import ro.vadim.peoplewithguns.game.fragments.GameFragment;
import ro.vadim.peoplewithguns.game.fragments.GameSelectionFragment;
import ro.vadim.peoplewithguns.game.fragments.LoadingFragment;
import ro.vadim.peoplewithguns.game.fragments.LobbyFragment;
import ro.vadim.peoplewithguns.game.fragments.LobbySettingsFragment;
import ro.vadim.peoplewithguns.game.fragments.MainMenuFragment;
import ro.vadim.peoplewithguns.game.fragments.MainMenuSettingsFragment;
import ro.vadim.peoplewithguns.game.fragments.TutorialsFragment;
import ro.vadim.peoplewithguns.game.utils.AdminUtils;
import ro.vadim.peoplewithguns.game.utils.FragmentUtils;
import ro.vadim.peoplewithguns.game.utils.GameFragmentMapUtils;
import ro.vadim.peoplewithguns.logging.Logger;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.net.TrafficStatsCompat;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.TrafficStats;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Bundle;
import android.os.PowerManager.WakeLock;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends FragmentActivity{
		
	
	
	
	
	
	
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.main_activity);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                
        
        if (savedInstanceState != null) {
            return;
        }        
        
        AdminUtils.setDefaultUncaughtExceptionHandler();
        
        AdminUtils.setScreenLock(this);
        AdminUtils.setWifiLock(this);
        
        
        FragmentUtils.setCurrentContext(this);
        FragmentUtils.setCurrentFragment(null);
        FragmentUtils.currentActivity = this;
        
        
        AdminUtils.errorLogger.getInitialDataVolume();
        AdminUtils.errorLogger.initLoggingFiles();
        
        MainMenuFragment firstFragment = new MainMenuFragment();
        
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, firstFragment).commit();
        
    }
    
    @Override
    protected void onPause() {
    	super.onPause();    	
    }
    
    @Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    }
        
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    }
    
    @Override
    protected void onStop() {
    	super.onStop();    	
    }
    
    
    
    
    
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
		
	@Override
	public void onBackPressed() {
		//super.onBackPressed();
		
		Fragment currentFragment = FragmentUtils.currentFragment;
		
		if(currentFragment == null){
			
		}
		else if(currentFragment.getClass().equals(MainMenuFragment.class)){
			AdminUtils.exitApp();
		}
		else if(currentFragment.getClass().equals(MainMenuSettingsFragment.class)){
			FragmentUtils.loadMainMenuFragment(this);			
		}
		else if(currentFragment.getClass().equals(LobbyFragment.class)){
			TcpGameClient.stopServer();
			FragmentUtils.loadMainMenuFragment(this);
		}
		
		else if(currentFragment.getClass().equals(GameSelectionFragment.class)){
			TcpGameManagementClient.stopServer();
			FragmentUtils.loadMainMenuFragment(this);
		}
		
		else if(currentFragment.getClass().equals(LobbySettingsFragment.class)){
			FragmentUtils.loadLobbyFragment(this);
		}
		else if(currentFragment.getClass().equals(GameFragment.class)){
			//FragmentUtils.removeGameFragment(this);
			AdminUtils.buildExitGameAlertMessage(FragmentUtils.currentContext, this);
		}		
		else if(currentFragment.getClass().equals(LoadingFragment.class)){
			
			int whatsLoading = ((LoadingFragment)currentFragment).whatsLoading;
			
			if(whatsLoading == LoadingFragment.LOADING_GAME_CLIENT)
				TcpGameClient.stopServer();
			
			else if(whatsLoading == LoadingFragment.LOADING_GAME_MANAGEMENT_CLIENT)
				TcpGameManagementClient.stopServer();			
			
			FragmentUtils.loadMainMenuFragment(this);
		}
		else if(currentFragment.getClass().equals(TutorialsFragment.class)){			
			FragmentUtils.loadMainMenuFragment(this);
		}
		
	}
	
	
	
	
	 @Override
	 public boolean onKeyDown(int keyCode, KeyEvent event) {     
		 
		 boolean isGameFragmentON = FragmentUtils.currentFragment.getClass().equals(GameFragment.class); 
		 		 
		 if((keyCode == KeyEvent.KEYCODE_HOME) && (isGameFragmentON)){			 
			 return true;
	     }
		 
		 else return super.onKeyDown(keyCode, event);
	 }
	
}