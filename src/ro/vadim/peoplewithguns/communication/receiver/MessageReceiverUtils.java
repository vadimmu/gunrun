package ro.vadim.peoplewithguns.communication.receiver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import ro.vadim.peoplewithguns.communication.CommunicationUtils;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.game.fragments.GameFragment;
import ro.vadim.peoplewithguns.game.fragments.GameSelectionFragment;
import ro.vadim.peoplewithguns.game.fragments.LobbyFragment;
import ro.vadim.peoplewithguns.game.fragments.LobbySettingsFragment;
import ro.vadim.peoplewithguns.game.gameelements.Player;
import ro.vadim.peoplewithguns.game.gameelements.Profession;
import ro.vadim.peoplewithguns.game.misc.Game;
import ro.vadim.peoplewithguns.game.misc.Games;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_ProfessionsAndWeapons;
import ro.vadim.peoplewithguns.game.utils.AdminUtils;
import ro.vadim.peoplewithguns.game.utils.FragmentUtils;
import ro.vadim.peoplewithguns.game.utils.GameFragmentButtonsUtils;
import ro.vadim.peoplewithguns.game.utils.GameFragmentUpdaterUtils;
import ro.vadim.peoplewithguns.game.utils.GameFragmentDrawingUtils;
import ro.vadim.peoplewithguns.game.utils.GameFragmentMapUtils;

import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;



public class MessageReceiverUtils {
	
	
	
	private static boolean IamTheTarget(UUID targetID){
		if(targetID.equals(StaticData_Players.currentPlayerUUID))
			return true;
		
		return false;		
	}
	
	private static boolean IamTheAttacker(UUID attackerID){
		if(attackerID.equals(StaticData_Players.currentPlayerUUID))
			return true;
		
		return false;		
	}
	
	
	
	private static void manage_SHOOT_Invisibility_Cloak(UUID attackerID, UUID targetID){
		Player victim = StaticData_Players.getPlayerById(targetID);		
		Player attacker = StaticData_Players.getPlayerById(attackerID);
		
		GameFragmentButtonsUtils.startAbilityCountdown(StaticData_ProfessionsAndWeapons.setInvisiblityCountdown(targetID));
		
		if(IamTheTarget(targetID)){
			Log.println(Log.INFO, "Invisibility cast on YOU !", "adding powerup counter");
			GameFragmentButtonsUtils.addPowerupCounter(StaticData_ProfessionsAndWeapons.weapons.get("Invisibility Cloak"));
			
			if(IamTheAttacker(attackerID)){
				GameFragmentUpdaterUtils.showText("You have cast Invisibility on yourself !");				
			}
			
			else{
				GameFragmentUpdaterUtils.showText(attacker.getNickname()+" cast Invisibility on you :) ");
			}
		}
		
		else{
			if(targetID.equals(attackerID)){
				GameFragmentUpdaterUtils.showText(attacker.getNickname()+" cast Invisibility on themself ");
			}	
			
			else{				
				GameFragmentUpdaterUtils.showText(attacker.getNickname()+" cast Invisibility on "+ victim.getNickname());
			}
		}		
		
	}
		
	private static void manage_SHOOT_Shield(UUID attackerID, UUID targetID){
		
		Log.println(Log.INFO, "SHOOT - manage_SHOOT_Shield ", "...");
		
		Player victim = StaticData_Players.getPlayerById(targetID);
		Player attacker = StaticData_Players.getPlayerById(attackerID);		
		
		
		victim.isShielded = true;
		GameFragmentButtonsUtils.startAbilityCountdown(StaticData_ProfessionsAndWeapons.setShieldCountdown(targetID));
		
		if(IamTheTarget(targetID)){
			Log.println(Log.INFO, "Shield cast on YOU !", "adding powerup counter");
			GameFragmentButtonsUtils.addPowerupCounter(StaticData_ProfessionsAndWeapons.weapons.get("Shield"));
			
			if(IamTheAttacker(attackerID)){
				GameFragmentUpdaterUtils.showText("You have cast Shield on yourself !");
			}
			
			else{
				GameFragmentUpdaterUtils.showText(attacker.getNickname()+" cast Shield on you :) ");
			}
		}
		
		else{
			if(targetID.equals(attackerID)){
				GameFragmentUpdaterUtils.showText(attacker.getNickname()+" cast Shield on themself ");
			}	
			
			else{				
				GameFragmentUpdaterUtils.showText(attacker.getNickname()+" cast Shield on "+ victim.getNickname());
			}
		}
	}	
	
	private static void manage_SHOOT_Generic_Weapon(UUID attackerID, UUID targetID, String weaponName, int damage){
		
		Player victim = StaticData_Players.getPlayerById(targetID);
		Player attacker = StaticData_Players.getPlayerById(attackerID);
		
		if(damage != 0)
			StaticData_Players.getPlayerById(targetID).modifyHealth(damage);
		
		if(IamTheTarget(targetID)){
						
			if(IamTheAttacker(attackerID)){
				GameFragmentUpdaterUtils.showText("You have used "+ weaponName + " on yourself !"+" ("+String.valueOf(damage)+" damage) !");
			}
			
			else{
				
				if(StaticData_Players.currentPlayer.getHealth() <= 0){
					GameFragmentUpdaterUtils.showText("YOU ARE DEAD !");
					GameFragmentUpdaterUtils.setButtonsInvisible();	
					AdminUtils.buildGameOverAlertMessage(FragmentUtils.currentActivity, FragmentUtils.currentActivity);
				}
				
				else{				
					GameFragmentUpdaterUtils.showText(attacker.getNickname() + " has used "+ weaponName + " on you !"+" ("+String.valueOf(damage)+" damage) !");
				}
			}
		}
		
		else{
			if(targetID.equals(attackerID)){
				GameFragmentUpdaterUtils.showText(attacker.getNickname()+" has used " + weaponName + " on themself "+" ("+String.valueOf(damage)+" damage) !");
			}	
			
			else{				
				GameFragmentUpdaterUtils.showText(attacker.getNickname()+" has used " + weaponName + " on " + victim.getNickname() + " ("+String.valueOf(damage)+" damage) !");
			}
		}
		
		GameFragmentDrawingUtils.redrawPlayer(targetID);
		
		GameFragmentUpdaterUtils.updateHealthTextView();
	}
	
	
	
	
			
	public static void manage_GAME_START(Map<String, Object> jsonData){
		GameFragmentUpdaterUtils.enableWeapons();
	}	
	
	public static void manage_SHOOT(Map<String, Object> jsonData){
		Log.println(Log.INFO, "SHOOT", jsonData.toString());
		
		
		if((jsonData.containsKey("player"))&&(jsonData.containsKey("target"))&&(jsonData.containsKey("weapon"))&&(jsonData.containsKey("damage"))){
			
			String weaponName = String.valueOf(jsonData.get("weapon"));
			int damage = Integer.valueOf(String.valueOf(jsonData.get("damage")));
						
			UUID attackerUUID = UUID.fromString(String.valueOf(jsonData.get("player")));
			UUID victimUUID = UUID.fromString(String.valueOf(jsonData.get("target")));
						
			Player attacker = StaticData_Players.getPlayerById(attackerUUID);
			Player victim = StaticData_Players.getPlayerById(victimUUID);
			
			String attackerName = attacker.getNickname();
			String victimName = victim.getNickname();
			
			
			
			Log.println(Log.INFO, "SHOOT - weapon name", "["+weaponName+"]");
						
			// add here the weapon effects 
			if(weaponName.equals("Invisibility Cloak")){
				manage_SHOOT_Invisibility_Cloak(attackerUUID, victimUUID);
				return;
			}
			
			if(weaponName.equals("Shield")){
				
				Log.println(Log.INFO, "SHOOT - managing SHIELD !", "["+weaponName+"]");
				manage_SHOOT_Shield(attackerUUID, victimUUID);
				return;
			}			
			
			manage_SHOOT_Generic_Weapon(attackerUUID, victimUUID, weaponName, damage);
			
			return;			
		}
		
	}
	
	public static void manage_CHANGE_POSITION(Map<String, Object> jsonData){
		Log.println(Log.INFO, "CHANGE_POSITION RECEIVED", jsonData.toString());
		
		if((jsonData.containsKey("player"))&&(jsonData.containsKey("latitude"))&&(jsonData.containsKey("longitude"))){
						
			Log.println(Log.INFO, "CHANGE_POSITION RECEIVED", "got player and coordinates");
			
			UUID playerUUID = UUID.fromString(String.valueOf(jsonData.get("player")));
			double latitude = Double.valueOf(String.valueOf(jsonData.get("latitude")));
			double longitude = Double.valueOf(String.valueOf(jsonData.get("longitude")));
			
			Log.println(Log.INFO, "CHANGE_POSITION RECEIVED", "setting position");						
			StaticData_Players.getPlayerById(playerUUID).setPosition(latitude , longitude);
			
			if(GameFragmentDrawingUtils.isGameFragmentOn()){
				GameFragmentDrawingUtils.redrawPlayer(playerUUID);
			}
		}
	}
	
	public static void manage_MESSAGE_TEAM(Map<String, Object> jsonData){
		Log.println(Log.INFO, "MESSAGE_TEAM", jsonData.toString());
		
		if((jsonData.containsKey("player"))&&(jsonData.containsKey("message"))){
			
			UUID playerUUID = UUID.fromString(String.valueOf(jsonData.get("player")));
			String playerName = StaticData_Players.getFriendById(playerUUID).getNickname();
			String message = String.valueOf(jsonData.get("message"));						
			
			FragmentUtils.showText(FragmentUtils.currentContext, playerName + ": \""+message+"\"");
		}
	}
	
	
	
	
		
	public static void manage_CONFIGURATION(Map<String, Object> jsonData){
		
		Log.println(Log.INFO, "CONFIGURATION", "Received Config from SERVER !");
		
		for(String key : jsonData.keySet())			
			Log.println(Log.INFO, "CONFIGURATION : KEYS", "["+key+"]");
		
		if(jsonData.containsKey("professions_and_weapons")){
			
			StaticData_ProfessionsAndWeapons.getConfig(((Map<String, Object>) jsonData.get("professions_and_weapons")));
			Log.println(Log.INFO,"CONFIGURATION","config JSON has professions_and_weapons");
			MessageReceiver.configReceived = true;
		}
		
		if(jsonData.containsKey("your_id")){
			StaticData_Players.currentPlayerUUID = UUID.fromString(String.valueOf( jsonData.get("your_id")));
			Log.println(Log.INFO,"CONFIGURATION","config JSON has your_id");
		}
		
		if(jsonData.containsKey("your_team")){
			StaticData_Players.currentPlayerTeam = Integer.valueOf(String.valueOf( jsonData.get("your_team")));
			Log.println(Log.INFO,"CONFIGURATION","config JSON has your_team");
		}
		
		if(jsonData.containsKey("players")){
			Log.println(Log.INFO,"CONFIGURATION","config JSON has players");
			Map<String, Object> players = (Map<String, Object>) jsonData.get("players");
			
			if(players.containsKey("friends")){
				StaticData_Players.friends = new LinkedHashMap<UUID, Player>(StaticData_Players.maxPlayersPerTeam);
											
				Map<String, Object> friends = (Map<String, Object>) players.get("friends");
				
				for(String uuidString : friends.keySet()){
					
					UUID playerUUID = UUID.fromString(uuidString);
					if(!playerUUID.equals(StaticData_Players.currentPlayerUUID)){
						
						Map<String, Object> friend = (Map<String, Object>) friends.get(uuidString);
						
						Log.println(Log.INFO, "Latitude and Longitude : ", "lat : "+String.valueOf(friend.get("latitude")+"  long : "+String.valueOf(friend.get("longitude"))));
						
						double geoLatitude = 0;
						double geoLongitude = 0;
						
						try{
							geoLatitude = Double.valueOf(String.valueOf(friend.get("latitude")));
							geoLongitude = Double.valueOf(String.valueOf(friend.get("longitude")));
						}
						
						catch(NumberFormatException e){
							Log.println(Log.ERROR, "NumberFormatException when getting the position : ", e.toString());						
						}						
						
						Player player = new Player(
											String.valueOf( friend.get("nickname")), 
											String.valueOf( friend.get("professionTitle")),
											geoLatitude, 
											geoLongitude
										);								
						
						StaticData_Players.friends.put(playerUUID, player);
					}
				}							
			}
			
			if(players.containsKey("enemies")){
				
				StaticData_Players.enemies = new LinkedHashMap<UUID, Player>(StaticData_Players.maxPlayersPerTeam);
				
				Map<String, Object> enemies = (Map<String, Object>) players.get("enemies");
				
				for(String uuidString : enemies.keySet()){
					UUID playerUUID = UUID.fromString(uuidString);
					
					Map<String, Object> friend = (Map<String, Object>) enemies.get(uuidString);
					
					double geoLatitude = 0;
					double geoLongitude = 0;
					
					if(String.valueOf(friend.get("latitude")).equals("null")) {
						geoLatitude = 0;												
					}
					
					if(String.valueOf(friend.get("longitude")).equals("null")) {
						geoLongitude = 0;
					}
					
					else{						
						geoLatitude = Double.valueOf(String.valueOf(friend.get("latitude")));
						geoLongitude = Double.valueOf(String.valueOf(friend.get("longitude")));
					}
					
					Player player = new Player(
										String.valueOf( friend.get("nickname")), 
										String.valueOf( friend.get("professionTitle")),
										geoLatitude, 
										geoLongitude
									);
					if(!playerUUID.equals(StaticData_Players.currentPlayerUUID))
						StaticData_Players.enemies.put(playerUUID, player);
				}
			}
			
			Log.println(Log.INFO,"CONFIGURATION","checking the list of players");
			
			for(Player p : StaticData_Players.friends.values()){
				Log.println(Log.INFO,"CONFIGURATION","Player : friend : "+p.getNickname()+" "+p.getProfessionTitle()+" "+String.valueOf(p.getHealth()));
			}
			
			for(Player p : StaticData_Players.enemies.values()){
				Log.println(Log.INFO,"CONFIGURATION","Player : enemy : "+p.getNickname()+" "+p.getProfessionTitle()+" "+String.valueOf(p.getHealth()));
			}			
			
		}
		
		Log.i("PREPARING TO INITIALIZE CURRENT PLAYER...", "...");
		StaticData_Players.initCurrentPlayer();
		
		try {
			MessageSender.send_CHANGE_NAME(StaticData_Players.currentPlayer.getNickname());
			MessageSender.send_CHANGE_PROFESSION(StaticData_Players.currentPlayer.getProfessionTitle());
		} 
		
		catch (IOException e) {
			Log.e("CONFIGURATION : could not send the nickname and/or the profession title of the current player", e.toString());
		}
		
		
	}

	public static void manage_CHANGE_PROFESSION(Map<String, Object> jsonData){
		Log.println(Log.INFO, "CHANGE_PROFESSION received !", "...");						
		
		if((jsonData.containsKey("player"))&&(jsonData.containsKey("profession"))){						
			UUID playerUUID = UUID.fromString(String.valueOf( jsonData.get("player")));
			String professionName = String.valueOf( jsonData.get("profession"));
			Profession profession = StaticData_ProfessionsAndWeapons.professions.get(professionName);
						
			Player player = StaticData_Players.getPlayerById(playerUUID);
			player.setProfessionTitle(professionName);
			player.setHealth(profession.health);			
		}
		FragmentUtils.updateLobby();
	}
	
	public static void manage_CHANGE_TEAM(Map<String, Object> jsonData){
		
		Log.println(Log.INFO, "CHANGE_TEAM received !", "...");
		
		if((jsonData.containsKey("player"))&&(jsonData.containsKey("team"))){				
			UUID playerUUID = UUID.fromString(String.valueOf( jsonData.get("player")));
			
			Player player = null;
			if((player = StaticData_Players.getEnemyById(playerUUID)) != null){							
				
				Player playerAux = StaticData_Players.getEnemyById(playerUUID);
				
				StaticData_Players.removePlayer(playerUUID);
				StaticData_Players.addPlayer(playerAux, playerUUID, true);
			}
			else if((player = StaticData_Players.getFriendById(playerUUID)) != null){
				
				Player playerAux = StaticData_Players.getFriendById(playerUUID);				
				StaticData_Players.removePlayer(playerUUID);
				StaticData_Players.addPlayer(playerAux, playerUUID, false);
			}
		}
		
		FragmentUtils.updateLobby();
	}

	public static void manage_CHANGE_NAME(Map<String, Object> jsonData){
		
		Log.println(Log.INFO, "CHANGE_NAME received", "...");
		if(jsonData != null)
			Log.println(Log.INFO, "CHANGE_NAME received", jsonData.toString());
		
		if((jsonData.containsKey("player"))&&(jsonData.containsKey("nickname"))){
			
			String name = String.valueOf(jsonData.get("nickname"));
			
			Log.println(Log.INFO, "changing nickname", name);
						
			UUID playerUUID = UUID.fromString(String.valueOf( jsonData.get("player")));
			StaticData_Players.getPlayerById(playerUUID).setNickname(name);						
		}
	}
	
	public static void manage_MESSAGE_ALL(Map<String, Object> jsonData){
		Log.println(Log.INFO, "MESSAGE_ALL received !", "...");
		
		if((jsonData.containsKey("player"))&&(jsonData.containsKey("message"))){						
			
			Player player = StaticData_Players.getPlayerById(UUID.fromString(String.valueOf(jsonData.get("player"))));
			if(player != null){				
				FragmentUtils.showText(FragmentUtils.currentContext, player.getNickname() + " : "+String.valueOf(jsonData.get("message")));				
			}				
			else{
				Log.println(Log.ERROR, "PLAYER NOT FOUND", "message received : " + String.valueOf(jsonData.get("message")));
			}
		}		
	}	

	public static void manage_PLAYER_JOINED(Map<String, Object> jsonData){
		
		Log.println(Log.INFO, "PLAYER_JOINED received !", "...");					
		
		if((jsonData.containsKey("uuid"))&&(jsonData.containsKey("nickname"))&&(jsonData.containsKey("team"))&&(jsonData.containsKey("profession"))){
			UUID uuid = UUID.fromString(String.valueOf(jsonData.get("uuid")));
			String nickname = String.valueOf(jsonData.get("nickname"));
			
			Log.println(Log.INFO, "PLAYER_JOINED (PLAYER NAME)!", nickname);
			
			if(nickname == null)
				nickname = "Player";
			
			int team = Integer.valueOf(String.valueOf(jsonData.get("team")));
			String profession = String.valueOf(jsonData.get("profession"));
			
			
			if(team == StaticData_Players.currentPlayerTeam){
				StaticData_Players.friends.put(uuid, new Player(nickname, profession, 0, 0));
			}
			
			else{
				StaticData_Players.enemies.put(uuid, new Player(nickname, profession, 0, 0));
			}			
		}
		
		FragmentUtils.updateLobby();		
	}

	public static void manage_PLAYER_LEFT(Map<String, Object> jsonData){
		
		Log.println(Log.INFO, "PLAYER_LEFT received !", "...");
		
		if(jsonData.containsKey("player")){						
			UUID playerUUID = UUID.fromString(String.valueOf(jsonData.get("player")));
			
			Player player = StaticData_Players.getPlayerById(playerUUID);
			
			if(player == null)
				return;
			
			//firstly remove the marker
			if(FragmentUtils.currentFragment.getClass().equals(GameFragment.class)){
				Log.println(Log.INFO, "Game fragment is ON ", "removing player marker");				
				GameFragmentDrawingUtils.removePlayerMarker(playerUUID);
			}
			
			//and only then remove the player ... duh !
			StaticData_Players.removePlayer(playerUUID);
			
			FragmentUtils.showText(FragmentUtils.currentContext,"Player \""+player.getNickname()+"\" has left the game...");
			
		}
		
		FragmentUtils.updateLobby();
	}
		
	public static void manage_READY(Map<String, Object> jsonData){
		
		Log.println(Log.INFO, "READY received !", "...");
		
		if((jsonData.containsKey("player"))&&(jsonData.containsKey("ready"))){
			UUID playerUUID = UUID.fromString(String.valueOf(jsonData.get("player")));
			StaticData_Players.getPlayerById(playerUUID).isReady = Boolean.valueOf(String.valueOf(jsonData.get("ready")));
			
			FragmentUtils.currentActivity.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {					
					((LobbyFragment)FragmentUtils.currentFragment).setupLists(((LobbyFragment)FragmentUtils.currentFragment).getView());					
				}
			});
			
			
		}
		
	}
		
	public static void manage_HEARTBEAT(Map<String, Object> jsonData){
		Log.println(Log.INFO, "HEARTBEAT received !", "...");				
	}
	
	public static void manage_ENTER_GAME(Map<String, Object> jsonData){
		
		Log.println(Log.INFO, "ENTER_GAME", jsonData.toString());
		FragmentUtils.startGame();
		
	}
	
		
	public static void manage_COUNTDOWN(Map<String, Object> jsonData){
		Log.println(Log.INFO, "COUNTDOWN received !", jsonData.toString());
		
		if(jsonData.containsKey("secondsLeft")){
			int secondsLeft = Integer.valueOf(String.valueOf(jsonData.get("secondsLeft")));
			
			if(secondsLeft == 3)
				FragmentUtils.lobbyFragment_setReadyButtonEnabled(false);
			
			FragmentUtils.showText(FragmentUtils.currentContext, "Seconds left : "+String.valueOf(secondsLeft));
		}
	}
	
	public static void manage_HELLO(Map<String, Object> jsonData){
		Log.println(Log.INFO, "HELLO received !", jsonData.toString());
		
		//public static int HELLO = 28;					//{player: playerUUID}
		
		
		if(jsonData.containsKey("player")){
			StaticData_Players.currentPlayerUUID = UUID.fromString(String.valueOf( jsonData.get("player")));
			Log.println(Log.INFO,"HELLO","config JSON has player id !");			
			MessageReceiver.helloReceived = true;
		}
	}

	
	public static void manage_GAMES_LIST(Map<String, Object> jsonData){
		Log.println(Log.INFO, "GAMES_LIST received !", jsonData.toString());
		
		if(jsonData.containsKey("gamesList")){
			
			String gamesListString = String.valueOf(jsonData.get("gamesList"));
			
			
			try {
				ArrayList<Object> gamesList = CommunicationUtils.mapper.readValue(gamesListString, ArrayList.class);
				//ADAUGA AICI ITERATIA PESTE ELEMENTE !!!
				
				ArrayList<String> newGames = new ArrayList<String>(gamesList.size());
				
				for(Object game : gamesList){
					HashMap<String, String> gameContents = CommunicationUtils.mapper.readValue(String.valueOf(game), HashMap.class);
					
					Log.i("GAMES_LIST", "GAME CONTENTS : "+gameContents.toString());
					
					String name = gameContents.get("name");
					String ip = gameContents.get("ip");
					int port = Integer.valueOf(gameContents.get("port"));
					UUID gameID = UUID.fromString(gameContents.get("game"));
					UUID creatorID = UUID.fromString(gameContents.get("creatorID"));
					int countAwayTeam = Integer.valueOf(gameContents.get("countAwayTeam"));
					int countHomeTeam = Integer.valueOf(gameContents.get("countHomeTeam"));
					boolean inProgress = Boolean.valueOf(gameContents.get("inProgress"));
															
					Game newGame = new Game(name, ip, port, gameID, creatorID, countAwayTeam, countHomeTeam, inProgress);
					Games.addGame(newGame);
					newGames.add(name);					
				}
				
				GameSelectionFragment currentFragment = (GameSelectionFragment) FragmentUtils.getCurrentFragment();
				currentFragment.setGames(newGames);
				currentFragment.refreshGamesList();
				
				Log.i("manage_GAMES_LIST", "Checking games list: "+currentFragment.getGames().toString());
						
				Games.refreshInProgress = false;
				
			} 
			
			catch (JsonParseException e) {
				Log.i("manage_GAMES_LIST", e.toString());
			} 
			catch (JsonMappingException e) {
				Log.i("manage_GAMES_LIST", e.toString());
			} 
			
			catch (IOException e) {
				Log.i("manage_GAMES_LIST", e.toString());
			}
			
		}
	}
	

}
