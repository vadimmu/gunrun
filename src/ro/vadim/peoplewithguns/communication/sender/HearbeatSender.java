package ro.vadim.peoplewithguns.communication.sender;

import java.io.IOException;

import android.util.Log;

public class HearbeatSender implements Runnable{
	
	boolean stop = false;
	
	public void stop(){
		stop = true;		
	}
	
	@Override
	public void run() {
		while(stop == false){
			
			try {
				Thread.sleep(3000);
				MessageSender.send_HEARTBEAT();
			}
			
			catch (InterruptedException e) {			
				Log.println(Log.ERROR, "HEARTBEAT ERROR", e.toString());
				stop();
			}
			
			catch (IOException e) {
				Log.println(Log.ERROR, "HEARTBEAT ERROR", e.toString());
				stop();
			}
		}		
	}	
}
