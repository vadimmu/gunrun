package ro.vadim.peoplewithguns.communication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Map;

import ro.vadim.peoplewithguns.communication.receiver.MessageReceiver;

import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.game.fragments.GameFragment;
import ro.vadim.peoplewithguns.game.fragments.LobbyFragment;
import ro.vadim.peoplewithguns.game.fragments.MainMenuFragment;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;
import ro.vadim.peoplewithguns.game.utils.FragmentUtils;
import ro.vadim.peoplewithguns.game.utils.GameFragmentMapUtils;

import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TcpGameClient {
	
	public static Socket clientSocket = null;
		
	public static MessageReceiver incomingMessages = null;
	
	
	public static Thread incomingMessageThread = null;
	
	
	public static boolean isConnected = false;
	
	public static Map<String, Object> parseIncomingMessage(String message) throws JsonParseException, JsonMappingException, IOException{
								
		Map<String,Object>jsonObject = CommunicationUtils.mapper.readValue(message, Map.class);
		return jsonObject;
	}
	
	
	
	private static void initJson(){
		CommunicationUtils.initObjectMapper();		
	}
	
	public static void initNetwork(String ip, int port){
		
		try{			
			TcpGameClient.clientSocket = new Socket(ip, port);	
			clientSocket.setKeepAlive(true);
			initJson();
			initMessageReceiver(clientSocket);
			initMessageSender(clientSocket);
			
			MessageSender.send_HELLO(StaticData_Players.currentPlayerUUID);
			
			isConnected = true;
		}
		
		catch (IOException e) {
			Log.println(Log.ERROR, "COULD NOT INITIALIZE NETWORK", e.toString());
			stopServer();
		}
				
	}
	
	
	public static void initMessageReceiver(Socket clientSocket){
		
		Log.println(Log.INFO, "INIT MESSAGE RECEIVER", "init message receiver");
		MessageReceiver.stop = false;
		incomingMessages = new MessageReceiver(clientSocket);
		incomingMessageThread = new Thread(incomingMessages);
		incomingMessageThread.start();
	}
	
	public static void initMessageSender(Socket clientSocket){
		Log.println(Log.INFO, "INIT MESSAGE SENDER", "init message sender");
		MessageSender.init(clientSocket);
	}
	
	
	public static void startServer(String ip, int port) throws IOException{		
		initNetwork(ip, port);		
	}
	
	
	public static void stopSender(){
		try{
			
			if(clientSocket.isOutputShutdown() == false)
				clientSocket.shutdownOutput();
		}
		
		catch (IOException e) {
			Log.println(Log.ERROR, "COULD NOT STOP SENDER !", e.toString());
		}
	}
	
	public static void stopServer(){
				
		try {
			isConnected = false;
						
			
			if(clientSocket != null){
				if(!clientSocket.isClosed()){
					Log.println(Log.INFO, "SHUTTING DOWN SOCKET", "...");
					
					
					
					if(clientSocket.isInputShutdown() == false){
						MessageReceiver.stop();
						clientSocket.shutdownInput();
					}
					
					if(clientSocket.isOutputShutdown() == false)
						clientSocket.shutdownOutput();
					
					clientSocket.close();
				
					Log.println(Log.INFO, "SHUTTING DOWN MESSAGE RECEIVER", "...");
					incomingMessages.stop();
					incomingMessageThread.interrupt();
				
					Log.println(Log.INFO, "SHUTTING DOWN HEARTBEAT SENDER", "...");					
					
				}
			}
		}
		catch (IOException e) {
			Log.println(Log.ERROR, "COULD NOT STOP SERVER !", e.toString());
		}
	}
	
	
	
	
}
