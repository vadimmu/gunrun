package ro.vadim.peoplewithguns.communication;

import java.io.IOException;
import java.net.Socket;
import java.util.Map;

import ro.vadim.peoplewithguns.communication.receiver.MessageReceiver;

import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.game.storage_and_data.StaticData_Players;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class TcpGameManagementClient {

	public static Socket clientSocket = null;
	
	public static MessageReceiver incomingMessages = null;
	
	
	public static Thread incomingMessageThread = null;
	
	
	public static boolean isConnected = false;
	
	
	private static void initJson(){
		CommunicationUtils.initObjectMapper();		
	}
	
	public static void initNetwork(String ip, int port){
		
		try{			
			clientSocket = new Socket(ip, port);
			clientSocket.setKeepAlive(true);			
			initJson();
			
			initMessageReceiver(clientSocket);
			initMessageSender(clientSocket);
			
			MessageSender.send_HELLO(StaticData_Players.currentPlayerUUID);
			
			isConnected = true;
		}
		
		catch (IOException e) {
			Log.println(Log.ERROR, "COULD NOT INITIALIZE NETWORK", e.toString());
			stopServer();
		}
				
	}
	
	
	public static void initMessageReceiver(Socket clientSocket){
		
		Log.println(Log.INFO, "INIT MESSAGE RECEIVER", "init message receiver");
		MessageReceiver.stop = false;
		incomingMessages = new MessageReceiver(clientSocket);
		incomingMessageThread = new Thread(incomingMessages);
		incomingMessageThread.start();
	}
	
	public static void initMessageSender(Socket clientSocket){
		Log.println(Log.INFO, "INIT MESSAGE SENDER", "init message sender");
		MessageSender.init(clientSocket);
	}
		
	public static void startServer(String ip, int port) throws IOException{		
		initNetwork(ip, port);		
	}
	
	public static void stopSender(){
		try{
			
			if(clientSocket.isOutputShutdown() == false)
				clientSocket.shutdownOutput();
		}
		
		catch (IOException e) {
			Log.println(Log.ERROR, "COULD NOT STOP SENDER !", e.toString());
		}
	}
	
	public static void stopReceiver(){
		try{			
			if(clientSocket.isInputShutdown() == false){
				MessageReceiver.stop();
				clientSocket.shutdownInput();
			}
		}
		
		catch (IOException e) {
			Log.println(Log.ERROR, "COULD NOT STOP SENDER !", e.toString());
		}
		
	}
	
	
	
	public static void stopServer(){
				
		try {
			isConnected = false;
						
			
			if(clientSocket != null){
				if(!clientSocket.isClosed()){
					Log.println(Log.INFO, "SHUTTING DOWN SOCKET", "...");
					
					stopSender();					
					stopReceiver();
					
					clientSocket.close();
				
					Log.println(Log.INFO, "SHUTTING DOWN MESSAGE RECEIVER", "...");
					
					incomingMessages.stop();
					incomingMessageThread.interrupt();
				}
			}
		}
		catch (IOException e) {
			Log.println(Log.ERROR, "COULD NOT STOP SERVER !", e.toString());
		}
	}
	
	
	
}
