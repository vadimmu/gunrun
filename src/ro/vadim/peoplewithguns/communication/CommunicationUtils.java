package ro.vadim.peoplewithguns.communication;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CommunicationUtils {
	
	public static String defaultServerAddress = "87.106.29.46";
	public static int defaultServerPort = 9001;
	
	public static ObjectMapper mapper = null;
		
	public static void initObjectMapper(){		
		mapper = new ObjectMapper();
	}
	
	public final static boolean validateIPAddress( String  ipAddress )
	{	
	    String[] parts = ipAddress.split( "\\." );

	    if ( parts.length != 4 )
	    {
	        return false;
	    }

	    try{
	    
		    for ( String s : parts )
		    {
		        int i = Integer.parseInt( s );
	
		        if ( (i < 0) || (i > 255) )
		        {
		            return false;
		        }
		    }
	    }
	    catch(NumberFormatException e){
	    	return false;	    	
	    }

	    return true;
	}
	
	
}
