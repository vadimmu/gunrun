/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.vadim.experiments.game;

public final class R {
    public static final class anim {
        public static final int slide_in_left=0x7f040000;
        public static final int slide_in_right=0x7f040001;
        public static final int slide_out_left=0x7f040002;
        public static final int slide_out_right=0x7f040003;
    }
    public static final class array {
        public static final int weapons_defender=0x7f070001;
        public static final int weapons_heavy_trooper=0x7f070003;
        public static final int weapons_marine=0x7f070000;
        public static final int weapons_sniper=0x7f070002;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int black=0x7f050000;
        public static final int blue=0x7f050001;
        public static final int red=0x7f050002;
    }
    public static final class drawable {
        public static final int ic_launcher=0x7f020000;
        public static final int marker_current_player=0x7f020001;
        public static final int marker_enemy=0x7f020002;
        public static final int marker_friend=0x7f020003;
        public static final int marker_selected=0x7f020004;
    }
    public static final class id {
        public static final int button_about=0x7f080015;
        public static final int button_back=0x7f08000b;
        public static final int button_change_info=0x7f08000e;
        public static final int button_change_team=0x7f08000f;
        public static final int button_exit=0x7f080017;
        public static final int button_ok=0x7f080009;
        public static final int button_ready=0x7f080010;
        public static final int button_settings=0x7f080016;
        public static final int button_settings_ok=0x7f08001d;
        public static final int button_start_game=0x7f080014;
        public static final int fragment_container=0x7f080013;
        public static final int googlemaps_change_my_position=0x7f080004;
        public static final int googlemaps_check_info=0x7f080001;
        public static final int list_away_team=0x7f080012;
        public static final int list_home_team=0x7f080011;
        public static final int loading=0x7f080005;
        public static final int loading_info=0x7f080006;
        public static final int mapview=0x7f080000;
        public static final int server_address_info=0x7f080019;
        public static final int server_address_text=0x7f08001a;
        public static final int server_port_info=0x7f08001b;
        public static final int server_port_text=0x7f08001c;
        public static final int shoot_button=0x7f080002;
        public static final int spinner_professions=0x7f080008;
        public static final int spinner_weapon=0x7f080003;
        public static final int text_away_team=0x7f08000d;
        public static final int text_home_team=0x7f08000c;
        public static final int text_menu=0x7f080018;
        public static final int text_name=0x7f080007;
        public static final int text_profession_details=0x7f08000a;
    }
    public static final class layout {
        public static final int game=0x7f030000;
        public static final int loading=0x7f030001;
        public static final int lobby_config=0x7f030002;
        public static final int lobby_main=0x7f030003;
        public static final int main_activity=0x7f030004;
        public static final int menu=0x7f030005;
        public static final int settings=0x7f030006;
    }
    public static final class string {
        public static final int app_name=0x7f060000;
    }
}
